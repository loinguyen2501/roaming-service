package constant

const (
	//User errors
	UserIDNotExist            = "UserId not exist"
	UserNotExist              = "user not exist"
	UserIDRequire             = "user id empty or invalid"
	UserWithEmailNotExist     = "Unable to find user with email '%v'"
	LoginBadReq               = "Please specify valid parameters for login"
	InvalidLoginCreds         = "Invalid login credentials"
	InvalidSecurityAns        = "Incorrect security question answer"
	ReqEmailOrMobile          = "Please fill in required fields"
	ReqSecurityQues           = "please provide at least two security questions"
	ReqValidSource            = "please provide a valid source"
	ReqMobileNIsd             = "please provide a mobile no with isd code"
	FailUserMobileOverride    = "user mobile override failed"
	FailUserEmailOverride     = "User email override failed"
	UpdatedUserRetrieveFailed = "Could not retrieve updated user information"
	InvalidNamespace          = "Invalid namespace"
	ExtendSchemaFail          = "Failed to extend schema"
	SamePwdErr                = "New password cannot be same as current password"
	OldPwdMismatchErr         = "Wrong old password"
	UnknownChannelErr         = "Channel to verify is unknown"
	InvalidSource             = "please provide valid source"
	InvalidMigUserPayload     = "Error in validating migrate user api payload"
	TelcoUserNotExist         = "Telco user not exist"
	RefreshTokenUpdateErr     = "Failed to store refreshed token information"
	ValidationUnknownErr      = "Something went wrong while trying to validate"
	MissingChannelParamsErr   = "Missing channel or user id"
	InvalidPwdLengthErr       = `Password needs to be at least 8 characters long`
	FailedToDeleteUser        = `failed to delete user`
	FailedToDeleteDevices     = `failed to delete devices`
	FailedToDeleteLogins      = `failed to delete logins`
	UsersCannotBeMerged       = `Users cannot be merged`

	// Generic query errors
	InvalidEntity   = "Invalid entity"
	InvalidOperator = "Invalid operator"

	// Request errors
	DecodeRequestBodyErr = "Error while decoding request body"

	// Address errors
	AddressIDNotExist            = "AddressId not exist"
	AddressEmpty                 = "Please fill in at least one of address lines"
	UpdatedAddressRetrieveFailed = "Could not retrieve updated address information"

	//Oauth errors
	FailedCodeExchange               = "Code exchange failed"
	GetFbUserFail                    = "Get fb user failed"
	EmailNotFoundViaFb               = "Email not found for user via fb"
	GetGoogleUserFail                = "Get google user failed"
	InvalidJWTToken                  = "invalid or empty token"
	TokenExpired                     = "JWT token is expired"
	ParseTokenErr                    = "Failed to parse JWT token"
	TokenGenFail                     = "Problem in generating JWT token"
	GoogleUserReqFail                = "Error getting Google user info"
	GoogleUserReqFailCode            = "Failed to get Google user info with error code"
	GoRespBodyReadErr                = "Error reading google response body"
	GoUnMarshalUserErr               = "Error unmarshaling Google user"
	FbUserReqFail                    = "Error getting Fb user info"
	FbRespBodyReadErr                = "Error reading fb response body"
	FbUnMarshalUserErr               = "Error unmarshaling fb user"
	FbUserReqFailCode                = "Failed to get fb user info with error code"
	MissingEmailForAuth              = `missing email for email auth`
	MissingPhoneNoAndIsdCodeForAuth  = `Missing phone_no and isd_code for sms auth`
	AuthModeMissMatchWithRequestType = `auth mode mismatch with request type`
	UnknownRequestType               = `unknown request type`

	//Otp errors
	InvalidAuthMode             = "invalid auth mode"
	StoreOptFail                = "failed to store OTP in cache"
	SendOtpFail                 = "otp send failed, please try again later"
	VerifyOtpFail               = "OTP code invalid, please try again"
	PurgeVerifiedOtpFail        = "verified otp purge fail"
	FailedToHandleOtpRequestMsg = "failed to handle OTP request"

	// redis
	MarshalFailure   = "Failed to marshal model"
	UnMarshalFailure = "Failed to unmarshal model"
	SetCacheFail     = "Fail to set in cache"
	GetCacheFail     = "Fail to get model from cache"
	DeleteCacheFail  = "Fail to delete from cache"

	// device
	DeviceInvalidFieldsErr = "Missing device id or invalid device type"
)
