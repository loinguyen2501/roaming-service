package codes

/*
 * Error Codes
 *
 * first 3 digits of error code is http status, example, 400001 => 400 (Bad Request)
 * check http status is implemeted in services/constrolerUtils before create new error code
 * `0` is default error code for invalid error
 */

const (
	// Server error
	InternalErrorCode uint32 = 50001 // constant.ServerIssue

	// Request error codes
	FailedToDecodeRequestBodyCode uint32 = 40002 // constant.DecodeRequestBodyErr

	// User error codes
	UserIDNotExistCode         uint32 = 40001 // constant.UserIDNotExist
	DuplicateAccessCode        uint32 = 40004 // constant.DuplicateAccess
	UserNotExistCode           uint32 = 40007 // constant.UserNotExist
	RequiredUserIDCode         uint32 = 40008 // constant.UserIDRequire
	UserNotExistAuthCode       uint32 = 40103 // constant.UserNotExist
	UserWithEmailNotExistCode  uint32 = 40014 // constant.UserWithEmailNotExist
	ReqEmailOrMobileCode       uint32 = 40015 // constant.ReqEmailOrMobile
	ReqSecurityQuesCode        uint32 = 40016 // constant.ReqSecurityQues
	InvalidSourceCode          uint32 = 40017 // constant.InvalidSource
	ReqMobileNIsdCode          uint32 = 40018 // constant.ReqMobileNIsd
	FailUserMobileOverrideCode uint32 = 50010 // constant.FailUserMobileOverride
	InvalidNamespaceCode       uint32 = 40019 // constant.InvalidNamespace
	ExtendSchemaFailedCode     uint32 = 50011 // constant.ExtendSchemaFail
	UserNotFoundCode           uint32 = 40401 // constant.UserNotExist
	SamePwdCode                uint32 = 40020 // constant.SamePwdErr
	OldPwdMismatchCode         uint32 = 40105 // constant.OldPwdMismatchErr
	UnknownChannelCode         uint32 = 40021 // constant.UnknownChannelErr
	TelcoUserNotExistCode      uint32 = 40022 // constant.TelcoUserNotExist
	InvalidEntityCode          uint32 = 40023 // constant.InvalidEntity
	InvalidOperatorCode        uint32 = 50012 // constant.InvalidOperator
	CountQueryCode             uint32 = 50015 // constant.CountQueryErr
	ReqValidSourceCode         uint32 = 40024 // constant.ReqValidSource
	MissingChannelParamsCode   uint32 = 40025 // constant.MissingChannelParamsErr
	InvalidPwdLengthCode       uint32 = 40026 // constant.InvalidPwdLengthErr
	UsersCannotBeMergedCode    uint32 = 40041 // constant.UsersCannotBeMerged
	FailedToDeleteUserCode     uint32 = 50024 // constant.FailedToDeleteUser
	FailedToDeleteDevicesCode  uint32 = 50025 // constant.FailedToDeleteDevices
	FailedToDeleteLoginsCode   uint32 = 50026 // constant.FailedToDeleteLogins

	// Address
	AddressEmptyCode                 uint32 = 40003 // constant.AddressEmpty
	AddressIDNotExistCode            uint32 = 40005 // constant.AddressIDNotExist
	UpdatedAddressRetrieveFailedCode uint32 = 50002 // constant.UpdatedAddressRetrieveFailed

	// Token
	TokenGenerationFailedCode uint32 = 50003 // constant.TokenGenFail
	ParseTokenCode            uint32 = 40009 // constant.ParseTokenErr
	TokenExpiredCode          uint32 = 40011 // constant.TokenExpired
	RefreshTokenUpdateCode    uint32 = 50006 // constant.RefreshTokenUpdateErr

	// Login
	LoginRecordFailedCode                uint32 = 50004 // constant.LoginRecordFail
	LoginBadReqCode                      uint32 = 40006 // constant.LoginBadReq
	InvalidLoginCredsCode                uint32 = 40101 // constant.InvalidLoginCreds
	MissingEmailForAuthCode              uint32 = 40028 // constant.MissingEmailForAuth
	MissingPhoneNoAndIsdCodeForAuthCode  uint32 = 40029 // constant.MissingPhoneNoAndIsdCodeForAuth
	AuthModeMissMatchWithRequestTypeCode uint32 = 40030 // constant.AuthModeMissMatchWithRequestType
	UnknownRequestTypeCode               uint32 = 40031 // constant.UnknownRequestType

	// Validation
	ValidateFieldCode               uint32 = 40032 // constant.ValidateFieldErr
	ValidationUnknownCodeDeprecated uint32 = 40027 // constant.ValidationUnknownErr
	ValidationUnknownCode           uint32 = 50020 //constant.ValidationUnknownErr

	// Device
	DeviceInvalidFieldsCode uint32 = 40033 // constant.DeviceInvalidFieldsErr

	// OTP
	VerifyOtpFailedCode              uint32 = 40102 // constant.VerifyOtpFail
	FailedToHandleOtpRequestCode     uint32 = 40013 // constant.FailedToHandleOtpRequestMsg
	StoreOptFailedCode               uint32 = 50017 // constant.StoreOptFail
	SendOtpFailedCode                uint32 = 50018 // constant.SendOtpFail
	VerifyOtpFailedServerCode        uint32 = 50019 // constant.VerifyOtpFail
	PurgeVerifiedOtpFailedCode       uint32 = 40106 // constant.PurgeVerifiedOtpFail
	PurgeVerifiedOtpFailedServerCode uint32 = 50013
	LoginStaleDataErrorCode          uint32 = 40301 // constant.StaleDataErr

	// Security
	InvalidSecurityAnsCode uint32 = 40104 // constant.InvalidSecurityAns

	// JWT
	EmptyJWTTokenCode   uint32 = 40010 // constant.InvalidJWTToken
	InvalidJWTTokenCode uint32 = 40107 // constant.InvalidJWTToken

	// DB
	FailedToDeleteDBModel uint32 = 50005 // constant.DbDeleteFail
	MgoDbConFailedCode    uint32 = 50014 // constant.MgoDbConFail
	FailDbQueryCode       uint32 = 50016 // constant.FailDbQuery

	// oAuth
	FailedCodeExchangeCode    uint32 = 50007 // constant.FailedCodeExchange
	GetGoogleUserFailedCode   uint32 = 50008 // constant.GetGoogleUserFail
	GetFbUserFailedCode       uint32 = 50009 // constant.GetFbUserFail
	InvalidAuthModeCode       uint32 = 40012 // constant.InvalidAuthMode
	EmailNotFoundFbFailedCode uint32 = 40404 // constant.EmailNotFoundViaFb
)
