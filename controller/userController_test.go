package controller

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/libertywireless/roaming-service/constant"
	"bitbucket.org/libertywireless/roaming-service/datastore"
	"bitbucket.org/libertywireless/roaming-service/service"

	"bitbucket.org/libertywireless/roaming-service/config"
	"bitbucket.org/libertywireless/roaming-service/logging"
	"bitbucket.org/libertywireless/roaming-service/model"
	"github.com/go-chi/chi"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

const (
	//Apis url
	USER_MIGRATE_API = "/v1/user/migrate"

	//Test constant
	USER_ID = "5b5abda504409c55f1f6b91c"
)

type mockUserStore struct {
	mock.Mock
}

func (m *mockUserStore) Create(ctx context.Context, user *model.UserModel) (string, error) {
	args := m.Called(ctx, user)
	return args.String(0), args.Error(1)
}

func (m *mockUserStore) Update(ctx context.Context, userModel *model.UserModel, user *model.UserUpdateRequestModel) error {
	args := m.Called(ctx, userModel, user)
	return args.Error(0)
}

func (m *mockUserStore) Update2(ctx context.Context, user *model.UserModel) error {
	args := m.Called(ctx, user)
	return args.Error(0)
}

func (m *mockUserStore) Find(ctx context.Context, criteria interface{}) (*model.UserModel, error) {
	args := m.Called(ctx, criteria)
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).(*model.UserModel), args.Error(1)
}

func (m *mockUserStore) FindAll(ctx context.Context, criteria interface{}) ([]model.UserModel, error) {
	args := m.Called(ctx, criteria)
	return args.Get(0).([]model.UserModel), args.Error(1)
}

func (m *mockUserStore) FindWithSoftDelete(ctx context.Context, includeDeletedRecords bool, criteria datastore.M) (*model.UserModel, error) {
	args := m.Called(ctx, includeDeletedRecords, criteria)
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).(*model.UserModel), args.Error(1)
}

func (m *mockUserStore) FindAllWithSoftDelete(ctx context.Context, includeDeletedRecords bool, criteria datastore.M) ([]model.UserModel, error) {
	args := m.Called(ctx, includeDeletedRecords, criteria)
	return args.Get(0).([]model.UserModel), args.Error(1)
}

func (m *mockUserStore) SoftDelete(ctx context.Context, user *model.UserModel) error {
	args := m.Called(ctx, user)
	return args.Error(0)
}

func (m *mockUserStore) Delete(ctx context.Context, criteria interface{}) error {
	args := m.Called(ctx, criteria)
	return args.Error(0)
}

type mockUserAuditStore struct {
	mock.Mock
}

func (m *mockUserAuditStore) Create(ctx context.Context, user *model.UserAuditModel) (string, error) {
	args := m.Called(ctx, user)
	return args.String(0), args.Error(1)
}

type mockTelcoAccountStore struct {
	mock.Mock
}

func (m *mockTelcoAccountStore) Create(ctx context.Context, t *model.TelcoInfo) (string, error) {
	args := m.Called(ctx, t)
	return args.String(0), args.Error(1)
}

func (m *mockTelcoAccountStore) Find(ctx context.Context, criteria interface{}) (*model.TelcoInfo, error) {
	args := m.Called(ctx, criteria)
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).(*model.TelcoInfo), args.Error(1)
}

func (m *mockTelcoAccountStore) Update(ctx context.Context, t *model.TelcoInfo) error {
	args := m.Called(ctx, t)
	return args.Error(0)
}

var generalConfig *config.GeneralConfig

func init() {
	generalConfig = new(config.GeneralConfig)
	generalConfig.PrivateKey = config.LoadPrivateKey("../private.pem")
	generalConfig.PublicKey = config.LoadPublicKey("../public.pem")
	generalConfig.LogFilePath = "../log/user_service.log"
	generalConfig.MiscData = make(map[string]interface{})
	generalConfig.CountryCode = "SG"
	generalConfig.MiscData["email_security_question_text"] = "Please enter your registered email ID"
	generalConfig.MiscData["min_session_duration"] = 360
	generalConfig.MiscData["default_pwd"] = "random password"
	generalConfig.MiscData["log_format_json"] = true
	generalConfig.MiscData["kirk_customer_info_url"] = "https://e96e7437-ed00-4025-b631-c4d3b204cbf9.mock.pstmn.io/%v/%v"
	generalConfig.MiscData["kirk_newrelic_metric_name"] = ""
	generalConfig.MiscData["security_qn_check"] = true
	generalConfig.MiscData["otp_check"] = true
	logging.InitializeLogger(generalConfig)
}

func NewMockUserController() *UserController {
	return &UserController{
		UserStore:      &mockUserStore{},
		DeviceStore:    &mockDeviceStore{},
		TelcoInfoStore: &mockTelcoAccountStore{},
		config:         generalConfig,
		Cache:          &MockCacheService{},
		OtpStore:       &mockOTPStore{},
		LoginStore:     &mockLoginStore{},
		UserAuditStore: &mockUserAuditStore{},
	}
}

func AssertDBCalls(userController *UserController, t *testing.T) {
	userController.UserStore.(*mockUserStore).AssertExpectations(t)
	userController.DeviceStore.(*mockDeviceStore).AssertExpectations(t)
	userController.OtpStore.(*mockOTPStore).AssertExpectations(t)
	userController.Cache.(*MockCacheService).AssertExpectations(t)
	userController.LoginStore.(*mockLoginStore).AssertExpectations(t)
	userController.UserAuditStore.(*mockUserAuditStore).AssertExpectations(t)
}

func TestCreateUserCompulsoryFieldsOnly(t *testing.T) {
	payload := []byte(`{
        "email": "test@gmail.com", 
		"password": "12345678", 
		"phone_no": "654321",
		"isd_code": "65"
	}`)
	expected := map[string]interface{}{
		"id":              "5b4eec77bc04fa1999963016",
		"email":           "test@gmail.com",
		"phone_no":        "654321",
		"email_confirmed": false,
		"phone_confirmed": false,
		"telco_user":      false,
	}

	userController := NewMockUserController()
	req, _ := http.NewRequest("POST", "/users/create", bytes.NewBuffer(payload))
	recorder := httptest.NewRecorder()

	userController.UserStore.(*mockUserStore).On("Create", mock.Anything, mock.Anything).Return("5b4eec77bc04fa1999963016", nil)
	userController.CreateUser(recorder, req)
	code := recorder.Result().StatusCode

	if code != http.StatusCreated {
		t.Errorf("Expected successful create to return response code 201. Got '%v'", code)
	}

	var m map[string]interface{}
	json.Unmarshal(recorder.Body.Bytes(), &m)

	if m["id"] != expected["id"] {
		t.Errorf("Expected id to be '%v'. Got '%v'", expected["id"], m["id"])
	}

	if m["external_id"] == "" {
		t.Errorf("Expected external id to be a non empty string. Got an empty string")
	}

	if m["email"] != expected["email"] {
		t.Errorf("Expected email to be '%v'. Got '%v'", expected["email"], m["email"])
	}

	if m["password"] != "" {
		t.Errorf("Expected password to be empty string. Got '%v'", m["password"])
	}

	if m["phone_no"] != expected["phone_no"] {
		t.Errorf("Expected phone number to be '%v'. Got '%v'", expected["phone_no"], m["phone_no"])
	}

	if m["email_confirmed"] != expected["email_confirmed"] {
		t.Errorf("Expected email confirm to be '%v'. Got '%v'", expected["email_confirmed"], m["email_confirmed"])
	}

	if m["phone_confirmed"] != expected["phone_confirmed"] {
		t.Errorf("Expected phone confirm to be '%v'. Got '%v'", expected["phone_confirmed"], m["phone_confirmed"])
	}

	if m["telco_user"] != expected["telco_user"] {
		t.Errorf("Expected telco user to be '%v'. Got '%v'", expected["telco_user"], m["telco_user"])
	}

	if m["created_at"] == 0 {
		t.Errorf("Expected created at to be a non empty string. Got an empty string")
	}

	if m["updated_at"] == 0 {
		t.Errorf("Expected updated at to be a non empty string. Got an empty string")
	}

	AssertDBCalls(userController, t)
}

func TestCreateUserAllFields(t *testing.T) {
	payload := []byte(`{
        "first_name":"srinand",
        "last_name":"challur",
        "dob":"1990-12-11",
        "email":"srinand.c@circles.asia",
        "password":"asdfasdf",
        "phone_no":"87682708",
        "image_url":"http://google.com",
        "isd_code":"65",
        "address_line_1":"04-125",
        "address_line_2":"20 Queen's Close",
        "address_line_3":"140090",
        "city":"Singapore",
        "state":"Singapore",
        "country":"Singapore",
        "postal_code":"140090",
		"device": {
			"device_id": "777",
			"imei": "354530085441085",
			"mac_id": "FF:FF:FF:FF:FF:FF",
			"device_token": "gregergeretetegfv",
			"device_type": "Mobile",
			"device_os": "Android Oreo",
			"user_agent": "Chrome"
		}

	}`)
	expected := map[string]interface{}{

		"id":             "5b4eec77bc04fa1999963016",
		"first_name":     "srinand",
		"last_name":      "challur",
		"dob":            "1990-12-11",
		"email":          "srinand.c@circles.asia",
		"password":       "asdfasdf",
		"phone_no":       "87682708",
		"image_url":      "http://google.com",
		"isd_code":       "65",
		"address_line_1": "04-125",
		"address_line_2": "20 Queen's Close",
		"address_line_3": "140090",
		"city":           "Singapore",
		"state":          "Singapore",
		"country":        "Singapore",
		"postal_code":    "140090",
	}

	userController := NewMockUserController()
	req, _ := http.NewRequest("POST", "/users/create", bytes.NewBuffer(payload))
	recorder := httptest.NewRecorder()

	userController.UserStore.(*mockUserStore).On("Create", mock.Anything, mock.Anything).Return("5b4eec77bc04fa1999963016", nil)
	userController.DeviceStore.(*mockDeviceStore).On("Find", mock.Anything, mock.Anything).Return(nil, errors.New("mongo: no documents in result"))
	userController.DeviceStore.(*mockDeviceStore).On("Create", mock.Anything, mock.Anything).Return("5b4eec77bc04fa1999963017", nil)
	userController.CreateUser(recorder, req)
	code := recorder.Result().StatusCode

	if code != http.StatusCreated {
		t.Errorf("Expected successful create to return response code 201. Got '%v'", code)
	}

	var m map[string]interface{}
	json.Unmarshal(recorder.Body.Bytes(), &m)

	if m["id"] != expected["id"] {
		t.Errorf("Expected id to be '%v'. Got '%v'", expected["id"], m["id"])
	}

	if m["external_id"] == "" {
		t.Errorf("Expected external id to be a non empty string. Got an empty string")
	}

	if m["first_name"] != expected["first_name"] {
		t.Errorf("Expected first name to be '%v'. Got '%v'", expected["first_name"], m["first_name"])
	}

	if m["last_name"] != expected["last_name"] {
		t.Errorf("Expected last name to be '%v'. Got '%v'", expected["last_name"], m["last_name"])
	}

	if m["dob"] != expected["dob"] {
		t.Errorf("Expected dob to be '%v'. Got '%v'", expected["dob"], m["dob"])
	}

	if m["email"] != expected["email"] {
		t.Errorf("Expected email to be '%v'. Got '%v'", expected["email"], m["email"])
	}

	if m["password"] != "" {
		t.Errorf("Expected password to be empty string. Got '%v'", m["password"])
	}

	if m["phone_no"] != expected["phone_no"] {
		t.Errorf("Expected phone number to be '%v'. Got '%v'", expected["phone_no"], m["phone_no"])
	}

	if m["image_url"] != expected["image_url"] {
		t.Errorf("Expected image url to be '%v'. Got '%v'", expected["image_url"], m["image_url"])
	}

	if m["isd_code"] != expected["isd_code"] {
		t.Errorf("Expected isd code to be '%v'. Got '%v'", expected["isd_code"], m["isd_code"])
	}

	if m["address_line_1"] != expected["address_line_1"] {
		t.Errorf("Expected address line 1 to be '%v'. Got '%v'", expected["address_line_1"], m["address_line_1"])
	}

	if m["address_line_2"] != expected["address_line_2"] {
		t.Errorf("Expected address line 2 to be '%v'. Got '%v'", expected["address_line_2"], m["address_line_2"])
	}

	if m["address_line_3"] != expected["address_line_3"] {
		t.Errorf("Expected address line 3 to be '%v'. Got '%v'", expected["address_line_3"], m["address_line_3"])
	}

	if m["city"] != expected["city"] {
		t.Errorf("Expected city to be '%v'. Got '%v'", expected["city"], m["city"])
	}

	if m["state"] != expected["state"] {
		t.Errorf("Expected state to be '%v'. Got '%v'", expected["state"], m["state"])
	}

	if m["country"] != expected["country"] {
		t.Errorf("Expected country to be '%v'. Got '%v'", expected["country"], m["country"])
	}

	if m["postal_code"] != expected["postal_code"] {
		t.Errorf("Expected postal code to be '%v'. Got '%v'", expected["postal_code"], m["postal_code"])
	}

	if m["created_at"] == "" {
		t.Errorf("Expected created at to be a non empty string. Got an empty string")
	}

	if m["updated_at"] == "" {
		t.Errorf("Expected updated at to be a non empty string. Got an empty string")
	}

	AssertDBCalls(userController, t)
}

func TestCreateUserMissingCompulsoryFields(t *testing.T) {
	payload := []byte(`{"email": "test@gmail.com" }`)

	userController := NewMockUserController()
	req, _ := http.NewRequest("POST", "/users/create", bytes.NewBuffer(payload))

	recorder := httptest.NewRecorder()

	userController.CreateUser(recorder, req)
	code := recorder.Result().StatusCode

	if code != 400 {
		t.Errorf("Expected invalid create to return response code 400. Got '%v'", code)
	}

	AssertDBCalls(userController, t)
}

func TestCreateNoSecurityQuestionsPayload(t *testing.T) {
	payload := []byte(`{"phone_no": "87652707", "isd_code":"65", "password":"12345" }`)

	userController := NewMockUserController()
	req, _ := http.NewRequest("POST", "/users/create", bytes.NewBuffer(payload))

	recorder := httptest.NewRecorder()

	userController.CreateUser(recorder, req)
	code := recorder.Result().StatusCode

	if code != 400 {
		t.Errorf("Expected invalid create to return response code 400. Got '%v'", code)
	}

	AssertDBCalls(userController, t)
}

func TestCreateNoSecurityQuestionsPayloadOneQuestion(t *testing.T) {
	payload := []byte(`{"phone_no": "87652707", "isd_code":"65", "password":"12345", "security_questions":[{"question":"q", "answer":"a"}] }`)

	userController := NewMockUserController()
	req, _ := http.NewRequest("POST", "/users/create", bytes.NewBuffer(payload))

	recorder := httptest.NewRecorder()

	userController.CreateUser(recorder, req)
	code := recorder.Result().StatusCode

	if code != 400 {
		t.Errorf("Expected invalid create to return response code 400. Got '%v'", code)
	}

	AssertDBCalls(userController, t)
}

func TestCreateNoSecurityQuestionsPayloadTwoQuestions(t *testing.T) {
	payload := []byte(`{"phone_no": "87652707", "isd_code":"65", "password":"12345678", "security_questions":[{"question":"q","answer":"a"},{"question":"q", "answer":"a"}]}`)

	userController := NewMockUserController()
	req, _ := http.NewRequest("POST", "/users/create", bytes.NewBuffer(payload))

	recorder := httptest.NewRecorder()

	userController.UserStore.(*mockUserStore).On("Create", mock.Anything, mock.Anything).Return("5b4eec77bc04fa1999963016", nil)

	userController.CreateUser(recorder, req)
	code := recorder.Result().StatusCode

	if code != 201 {
		t.Errorf("Expected invalid create to return response code 201. Got '%v'", code)
	}

	AssertDBCalls(userController, t)
}

func TestCreateUserEmpty(t *testing.T) {
	payload := []byte(`{}`)

	userController := NewMockUserController()
	req, _ := http.NewRequest("POST", "/users/create", bytes.NewBuffer(payload))

	recorder := httptest.NewRecorder()

	userController.CreateUser(recorder, req)
	code := recorder.Result().StatusCode

	if code != 400 {
		t.Errorf("Expected invalid create to return response code 400. Got '%v'", code)
	}

	AssertDBCalls(userController, t)
}

func TestUpdateUser(t *testing.T) {
	payload := []byte(`{"external_id": "123" }`)

	req, _ := http.NewRequest("POST", "/users/update", bytes.NewBuffer(payload))
	recorder := httptest.NewRecorder()

	userController := NewMockUserController()
	userController.UserStore.(*mockUserStore).On("FindWithSoftDelete", mock.Anything, mock.Anything, mock.Anything).Return(&model.UserModel{}, nil)
	userController.UserStore.(*mockUserStore).On("Update", mock.Anything, mock.Anything, mock.Anything).Return(nil)
	userController.UpdateUser(recorder, req)
	code := recorder.Result().StatusCode

	if code != 200 {
		t.Errorf("Expected update to return response code 200. Got '%v'", code)
	}

	AssertDBCalls(userController, t)
}

func TestUpdateEmpty(t *testing.T) {
	payload := []byte(`{}`)

	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", "/users/update", bytes.NewBuffer(payload))
	recorder := httptest.NewRecorder()

	userController.UpdateUser(recorder, req)
	code := recorder.Result().StatusCode

	if code != 400 {
		t.Errorf("Expected invalid update to return response code 400. Got '%v'", code)
	}

	AssertDBCalls(userController, t)
}

func TestUpdateMissingExternalId(t *testing.T) {
	payload := []byte(`{"first_name": "John"}`)
	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", "/users/update", bytes.NewBuffer(payload))
	recorder := httptest.NewRecorder()

	userController.UpdateUser(recorder, req)
	code := recorder.Result().StatusCode

	if code != 400 {
		t.Errorf("Expected invalid update to return response code 400. Got '%v'", code)
	}

	AssertDBCalls(userController, t)
}

func TestGenJWT(t *testing.T) {
	user := new(model.UserModel)
	user.ExternalId = "SG-12345678"
	currentTime := int64(1532327134)
	ctx := context.Background()
	token, _ := service.GenJWTToken(ctx, *generalConfig, service.MakeLoginClaims(user, currentTime, currentTime+(int64(360*60)), "Mobile"))
	// fmt.Print(token)

	if token != "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJkZXZpY2VfdHlwZSI6Ik1vYmlsZSIsImV4cGlyeV90aW1lIjoxNTMyMzQ4NzM0LCJleHRlcm5hbF9pZCI6IlNHLTEyMzQ1Njc4IiwibG9naW5fdGltZSI6MTUzMjMyNzEzNH0.BCSs3w4cNk5ZNKD1zle3-Jg6FLtYPqVwWvWhxsPW7dV6A4WuS6KQCfA47ZG8VwUCKEJihd3zNRTDXZf9XPoGCnZXe3dLmQE623QBOp7LgvDxIgqrEEOaFcxteBzI5m34NV1_A7B3ad3eunFnj92BODKHMJ1sfsxqhSH5m9cvVrVX_eDMUDnaRVyFGFVNJjP_Dyk0jcSGrLKIhkS24Kp_7JLSkcoR_30OhF2apalg0DNlT1hUIJWkmlKaqWYr_RIFvQmPNQI2fz1m1Wn-_8wCYpDoZyxB5v_ESrsH-y7bJ4lCUpTie76KuYZwK7TEOtygpW53ZV4tvLeBgUw3KDzY9Q" {
		t.Errorf("Token value did not match expected")
	}

	result := service.ParseJWTString(ctx, token, *generalConfig)

	if result["external_id"] != user.ExternalId {
		t.Errorf("Public Key is not valid")
	}

}

func TestResetPasswordGoodPayload(t *testing.T) {

	payload := []byte(`{
		"auth_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHBpcnlfdGltZSI6MjIxODYzNjQ2NjgyLCJleHRlcm5hbF9pZCI6IlNHLTEyMzQ1Njc4OTEiLCJsb2dpbl90aW1lIjoxNTMyMzI3MTM0fQ.UansWmSQ6F703D4Bj2OGEx3I9tqkHcioeLOpncc70hOtIk6K-67LDcHTwJG2B18C3sXXV2mMtQeOFDpFWNGflRFqjG2f0szk3U3HoKZzxtrJcxIAtTRov-LkK4jGwrH8NObsroZDEZlkjzM08e2INwcOH1tR2HXWe5llL69zUEm3AcSk0YwN2xnTbxDZQDOwklzbE2mOzQDbLKQM1GMdb0u6fOldgj1bYB2J6TOOz7O32_REYadMf_46YbFMu-At_rn8J69EMJpOH0lICsYsM28Oq88uPYwHbsjYRWYmsfOLCxLF3a1Ip5l9lTWS9XlIDJY9TKGxEx1i5JmGqYQPbQ", 
		"current_password":"12345678", 
		"new_password":"88888888"
	}`)
	expected := &model.UserModel{
		ExternalId: "SG-1234567891",
		FirstName:  "Peter",
		Password:   "$2a$10$F9JxZRjjJg2KO8uNTq1GEeQhbfD5vxt2pIlIO0099jB5KG4WTGpiq",
		CreatedAt:  1531990000,
		UpdatedAt:  1531995953,
	}
	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", "/users/SG-1234567891/password/reset", bytes.NewBuffer(payload))
	rctx := chi.NewRouteContext()
	rctx.URLParams.Add("user_id", "SG-1234567891")
	req = req.WithContext(context.WithValue(req.Context(), chi.RouteCtxKey, rctx))
	recorder := httptest.NewRecorder()
	userController.UserStore.(*mockUserStore).On("Update2", mock.Anything, mock.Anything).Return(nil)
	userController.UserStore.(*mockUserStore).On("FindWithSoftDelete", mock.Anything, mock.Anything, mock.Anything).Return(expected, nil)
	userController.ResetPassword(recorder, req)
	code := recorder.Result().StatusCode

	if code != http.StatusOK {
		t.Errorf("Expected valid password reset to return response code 200. Got '%v'", code)
	}

	AssertDBCalls(userController, t)

}

func TestPasswordResetBadRequest1(t *testing.T) {

	payload := []byte(`{"auth_token": "xxx", "current_password": "1234"}`)
	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", "/users/SG-12345678910/password/reset", bytes.NewBuffer(payload))
	rctx := chi.NewRouteContext()
	rctx.URLParams.Add("user_id", "SG-12345678910")
	req = req.WithContext(context.WithValue(req.Context(), chi.RouteCtxKey, rctx))
	recorder := httptest.NewRecorder()

	userController.ResetPassword(recorder, req)
	code := recorder.Result().StatusCode

	if code != http.StatusBadRequest {
		t.Errorf("Expected invalid update to return response code 400. Got '%v'", code)
	}

	AssertDBCalls(userController, t)
}
func TestPasswordResetBadRequest2(t *testing.T) {

	payload := []byte(`{"auth_token": "xxx", "new_password":"5678" }`)
	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", "/users/SG-12345678910/password/reset", bytes.NewBuffer(payload))
	rctx := chi.NewRouteContext()
	rctx.URLParams.Add("user_id", "SG-12345678910")
	req = req.WithContext(context.WithValue(req.Context(), chi.RouteCtxKey, rctx))
	recorder := httptest.NewRecorder()

	userController.ResetPassword(recorder, req)
	code := recorder.Result().StatusCode

	if code != http.StatusBadRequest {
		t.Errorf("Expected invalid update to return response code 400. Got '%v'", code)
	}

	AssertDBCalls(userController, t)
}
func TestPasswordResetBadRequest3(t *testing.T) {

	payload := []byte(``)
	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", "/users/SG-12345678910/password/reset", bytes.NewBuffer(payload))
	rctx := chi.NewRouteContext()
	rctx.URLParams.Add("user_id", "SG-12345678910")
	req = req.WithContext(context.WithValue(req.Context(), chi.RouteCtxKey, rctx))
	recorder := httptest.NewRecorder()

	userController.ResetPassword(recorder, req)
	code := recorder.Result().StatusCode

	if code != http.StatusBadRequest {
		t.Errorf("Expected invalid update to return response code 400. Got '%v'", code)
	}

	var m map[string]interface{}
	json.Unmarshal(recorder.Body.Bytes(), &m)

	if m["message"] != constant.DecodeRequestBodyErr {
		t.Errorf("Expected error message: '%v'. Got '%v'", constant.DecodeRequestBodyErr, m["message"])
	}

	AssertDBCalls(userController, t)
}

func TestPasswordResetInvalidAuthToken(t *testing.T) {

	payload := []byte(`{
		"auth_token": "xxx", 
		"current_password":"1234", 
		"new_password":"5678" 
	}`)

	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", "/users/SG-1234567891/password/reset", bytes.NewBuffer(payload))
	rctx := chi.NewRouteContext()
	rctx.URLParams.Add("user_id", "SG-1234567891")
	req = req.WithContext(context.WithValue(req.Context(), chi.RouteCtxKey, rctx))
	recorder := httptest.NewRecorder()

	userController.ResetPassword(recorder, req)
	code := recorder.Result().StatusCode

	if code != http.StatusBadRequest {
		t.Errorf("Expected invalid update to return response code 400. Got '%v'", code)
	}

	AssertDBCalls(userController, t)
}

func TestPasswordResetWrongCurrentPassword(t *testing.T) {

	payload := []byte(`{
		"auth_token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHBpcnlfdGltZSI6MjIxODYzNjQ2NjgyLCJleHRlcm5hbF9pZCI6IlNHLTEyMzQ1Njc4OTEiLCJsb2dpbl90aW1lIjoxNTMyMzI3MTM0fQ.UansWmSQ6F703D4Bj2OGEx3I9tqkHcioeLOpncc70hOtIk6K-67LDcHTwJG2B18C3sXXV2mMtQeOFDpFWNGflRFqjG2f0szk3U3HoKZzxtrJcxIAtTRov-LkK4jGwrH8NObsroZDEZlkjzM08e2INwcOH1tR2HXWe5llL69zUEm3AcSk0YwN2xnTbxDZQDOwklzbE2mOzQDbLKQM1GMdb0u6fOldgj1bYB2J6TOOz7O32_REYadMf_46YbFMu-At_rn8J69EMJpOH0lICsYsM28Oq88uPYwHbsjYRWYmsfOLCxLF3a1Ip5l9lTWS9XlIDJY9TKGxEx1i5JmGqYQPbQ", 
		"new_password":"12345678", 
		"current_password":"11111111"
	}`)

	expected := &model.UserModel{
		ExternalId: "SG-1234567891",
		FirstName:  "Peter",
		Password:   "$2y$12$eSdrnp3klxDjmseIWLIyeuMGC.vy.Uv5AUq3H8W45hioh7ViB/W3O",
		CreatedAt:  1531990000,
		UpdatedAt:  1531995953,
	}
	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", "/users/SG-12345678910/password/reset", bytes.NewBuffer(payload))
	rctx := chi.NewRouteContext()
	rctx.URLParams.Add("user_id", "SG-1234567891")
	req = req.WithContext(context.WithValue(req.Context(), chi.RouteCtxKey, rctx))
	recorder := httptest.NewRecorder()

	userController.UserStore.(*mockUserStore).On("FindWithSoftDelete", mock.Anything, mock.Anything, mock.Anything).Return(expected, nil)
	userController.ResetPassword(recorder, req)
	code := recorder.Result().StatusCode

	if code != http.StatusUnauthorized {
		t.Errorf("Expected invalid update to return response code 401. Got '%v'", code)
	}

	var m map[string]interface{}
	json.Unmarshal(recorder.Body.Bytes(), &m)

	if m["message"] != constant.OldPwdMismatchErr {
		t.Errorf("Expected error message: '%v'. Got '%v'", constant.OldPwdMismatchErr, m["message"])
	}

	AssertDBCalls(userController, t)
}

func TestPasswordResetSameOldAndNewPassword(t *testing.T) {

	payload := []byte(`{
		"auth_token": "xxx", 
		"current_password":"12345678", 
		"new_password":"12345678"
	}`)

	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", "/users/SG-12345678910/password/reset", bytes.NewBuffer(payload))
	rctx := chi.NewRouteContext()
	rctx.URLParams.Add("user_id", "SG-12345678910")
	req = req.WithContext(context.WithValue(req.Context(), chi.RouteCtxKey, rctx))
	recorder := httptest.NewRecorder()

	userController.ResetPassword(recorder, req)
	code := recorder.Result().StatusCode

	if code != http.StatusBadRequest {
		t.Errorf("Expected invalid update to return response code 400. Got '%v'", code)
	}

	var m map[string]interface{}
	json.Unmarshal(recorder.Body.Bytes(), &m)

	if m["message"] != "New password cannot be same as current password" {
		t.Errorf("Expected error message: New password cannot be same as current password. Got '%v'", m["message"])
	}

	AssertDBCalls(userController, t)
}

func TestForgotPasswordGoodRequest(t *testing.T) {

	payload := []byte(`{
		"auth_id": "AUTH5b59752c47e63d7ff01f9660", 
		"new_password":"12345678", 
		"otp_code":"5b59752c47e63d7ff01f9660"
	}`)

	expected := &model.UserModel{
		ExternalId: "SG-12345678910",
		FirstName:  "Peter",
		Password:   "$2y$12$eSdrnp3klxDjmseIWLIyeuMGC.vy.Uv5AUq3H8W45hioh7ViB/W3O",
		CreatedAt:  1531990000,
		UpdatedAt:  1531995953,
	}

	cachedOtpModel := &model.OtpAuthenticationModel{
		Id:          "AUTH5b59752c47e63d7ff01f9660",
		OtpCode:     "5b59752c47e63d7ff01f9660",
		UserId:      "SG-12345678910",
		SecurityAns: "Manchester City",
		IsdCode:     "65",
		PhoneNo:     "98765432",
		AuthMode:    "Email",
		RequestType: "PASSWORD_RESET",
	}

	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", "/users/password/reset", bytes.NewBuffer(payload))
	recorder := httptest.NewRecorder()

	userController.UserStore.(*mockUserStore).On("Update2", mock.Anything, mock.Anything).Return(nil)
	userController.UserStore.(*mockUserStore).On("FindWithSoftDelete", mock.Anything, mock.Anything, mock.Anything).Return(expected, nil)
	userController.Cache.(*MockCacheService).On("GetOTP", mock.Anything, mock.Anything).Return(cachedOtpModel)
	userController.OtpStore.(*mockOTPStore).On("Create", mock.Anything, mock.Anything).Return("5b5abda504409c55f1f6b91c", nil)
	userController.Cache.(*MockCacheService).On("DeleteOTP", mock.Anything, mock.Anything).Return(true)
	userController.ForgotPassword(recorder, req)
	code := recorder.Result().StatusCode

	if code != http.StatusOK {
		t.Errorf("Expected valid password reset to return response code 200. Got '%v'", code)
	}

	AssertDBCalls(userController, t)
}

func TestForgotPasswordBadRequest1(t *testing.T) {

	payload := []byte(`{
		"new_password":"12345678", 
		"otp_code":"5b59752c47e63d7ff01f9660"
	}`)

	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", "/users/password/reset", bytes.NewBuffer(payload))
	recorder := httptest.NewRecorder()

	userController.ForgotPassword(recorder, req)
	code := recorder.Result().StatusCode

	if code != http.StatusBadRequest {
		t.Errorf("Expected invalid update to return response code 400. Got '%v'", code)
	}

	AssertDBCalls(userController, t)
}

func TestForgotPasswordBadRequest2(t *testing.T) {

	payload := []byte(`{
		"auth_id": "AUTH5b59752c47e63d7ff01f9660", 
		"otp_code":"5b59752c47e63d7ff01f9660"
	}`)

	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", "/users/password/reset", bytes.NewBuffer(payload))
	recorder := httptest.NewRecorder()

	userController.ForgotPassword(recorder, req)
	code := recorder.Result().StatusCode

	if code != http.StatusBadRequest {
		t.Errorf("Expected invalid update to return response code 400. Got '%v'", code)
	}

	AssertDBCalls(userController, t)
}

func TestForgotPasswordBadRequest3(t *testing.T) {

	payload := []byte(`{}`)

	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", "/users/password/reset", bytes.NewBuffer(payload))
	recorder := httptest.NewRecorder()

	userController.ForgotPassword(recorder, req)
	code := recorder.Result().StatusCode

	if code != http.StatusBadRequest {
		t.Errorf("Expected invalid update to return response code 400. Got '%v'", code)
	}

	AssertDBCalls(userController, t)
}

func TestCreateUserAuthRequired(t *testing.T) {

	payload := []byte(`{
        "first_name":"srinand",
        "last_name":"challur",
        "dob":"1990-12-11",
        "email":"srinand.c@circles.asia",
        "password":"asdfasdf",
        "phone_no":"87682708",
        "image_url":"http://google.com",
        "isd_code":"65",
        "address_line_1":"04-125",
        "address_line_2":"20 Queen's Close",
        "address_line_3":"140090",
        "city":"Singapore",
        "state":"Singapore",
        "country":"Singapore",
        "postal_code":"140090",
		"device": {
			"device_id": "777",
			"imei": "354530085441085",
			"mac_id": "FF:FF:FF:FF:FF:FF",
			"device_token": "gregergeretetegfv",
			"device_type": "Mobile",
			"device_os": "Android Oreo",
			"user_agent": "Chrome"
		},
		"auth_required": true
	}`)

	userController := NewMockUserController()
	req, _ := http.NewRequest("POST", "/users/create", bytes.NewBuffer(payload))
	recorder := httptest.NewRecorder()

	userController.UserStore.(*mockUserStore).On("Create", mock.Anything, mock.Anything).Return("5b4eec77bc04fa1999963016", nil)
	userController.DeviceStore.(*mockDeviceStore).On("Find", mock.Anything, mock.Anything).Return(nil, errors.New("mongo: no documents in result"))
	userController.DeviceStore.(*mockDeviceStore).On("Create", mock.Anything, mock.Anything).Return("5b4eec77bc04fa1999963017", nil)
	userController.LoginStore.(*mockLoginStore).On("Find", mock.Anything, mock.Anything).Return(nil, errors.New("mongo: no documents in result"))
	userController.LoginStore.(*mockLoginStore).On("Create", mock.Anything, mock.Anything).Return("5b4eec77bc04fa1999963016", nil)
	userController.CreateUser(recorder, req)
	code := recorder.Result().StatusCode

	if code != http.StatusCreated {
		t.Errorf("Expected successful create to return response code 201. Got '%v'", code)
	}

	var m map[string]interface{}
	json.Unmarshal(recorder.Body.Bytes(), &m)

	if m["auth_token"] == "" {
		t.Errorf("Expected non empty auth token. Got empty.")
	}

	AssertDBCalls(userController, t)
}

func TestVerifyEmail(t *testing.T) {

	payload := []byte(`{
		"auth_id": "AUTH-123456789",
		"otp_code": "622250",
		"device": {
			"device_id": "777",
			"imei": "354530085441085",
			"mac_id": "FF:FF:FF:FF:FF:FF",
			"device_token": "gregergeretetegfv",
			"device_type": "Mobile",
			"device_os": "Android Oreo",
			"user_agent": "Chrome"
		}
	}`)

	storedUser := &model.UserModel{
		ExternalId:     "SG-123456789",
		EmailConfirmed: false,
	}

	storedAuth := &model.OtpAuthenticationModel{
		AuthID:  "AUTH-123456789",
		UserId:  "SG-123456789",
		OtpCode: "622250",
	}

	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", "/users/SG-123456789/verify/email", bytes.NewBuffer(payload))
	rctx := chi.NewRouteContext()
	rctx.URLParams.Add("user_id", "SG-123456789")
	rctx.URLParams.Add("channel", "email")
	req = req.WithContext(context.WithValue(req.Context(), chi.RouteCtxKey, rctx))
	recorder := httptest.NewRecorder()

	userController.Cache.(*MockCacheService).On("GetOTP", mock.Anything, mock.Anything).Return(storedAuth)
	userController.UserStore.(*mockUserStore).On("FindWithSoftDelete", mock.Anything, mock.Anything, mock.Anything).Return(storedUser, nil)
	userController.UserStore.(*mockUserStore).On("Update2", mock.Anything, mock.Anything).Return(nil)
	userController.OtpStore.(*mockOTPStore).On("Create", mock.Anything, mock.Anything).Return("5b5abda504409c55f1f6b91c", nil)
	userController.DeviceStore.(*mockDeviceStore).On("Find", mock.Anything, mock.Anything).Return(&model.UserDevice{DeviceId: "777"}, nil)
	userController.Cache.(*MockCacheService).On("DeleteOTP", mock.Anything, mock.Anything).Return(true)

	userController.VerifyChannel(recorder, req)
	code := recorder.Result().StatusCode

	if code != http.StatusOK {
		t.Errorf("Expected invalid update to return response code 200. Got '%v'", code)
	}

	if !storedUser.EmailConfirmed {
		t.Errorf("Expected email confirmed to be true. Got false.")
	}

	AssertDBCalls(userController, t)
}

func TestVerifyMobile(t *testing.T) {

	payload := []byte(`{
		"auth_id": "AUTH-123456789",
		"otp_code": "622250",
		"device": {
			"device_id": "777",
			"imei": "354530085441085",
			"mac_id": "FF:FF:FF:FF:FF:FF",
			"device_token": "gregergeretetegfv",
			"device_type": "Mobile",
			"device_os": "Android Oreo",
			"user_agent": "Chrome"
		}
	}`)

	storedUser := &model.UserModel{
		ExternalId:     "SG-123456789",
		PhoneConfirmed: false,
	}

	storedAuth := &model.OtpAuthenticationModel{
		AuthID:  "AUTH-123456789",
		UserId:  "SG-123456789",
		OtpCode: "622250",
	}

	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", "/users/SG-123456789/verify/mobile", bytes.NewBuffer(payload))
	rctx := chi.NewRouteContext()
	rctx.URLParams.Add("user_id", "SG-123456789")
	rctx.URLParams.Add("channel", "mobile")
	req = req.WithContext(context.WithValue(req.Context(), chi.RouteCtxKey, rctx))
	recorder := httptest.NewRecorder()

	userController.Cache.(*MockCacheService).On("GetOTP", mock.Anything, mock.Anything).Return(storedAuth)
	userController.UserStore.(*mockUserStore).On("FindWithSoftDelete", mock.Anything, mock.Anything, mock.Anything).Return(storedUser, nil)
	userController.UserStore.(*mockUserStore).On("Update2", mock.Anything, mock.Anything).Return(nil)
	userController.OtpStore.(*mockOTPStore).On("Create", mock.Anything, mock.Anything).Return("5b5abda504409c55f1f6b91c", nil)
	userController.DeviceStore.(*mockDeviceStore).On("Find", mock.Anything, mock.Anything).Return(&model.UserDevice{DeviceId: "777"}, nil)
	userController.Cache.(*MockCacheService).On("DeleteOTP", mock.Anything, mock.Anything).Return(true)

	userController.VerifyChannel(recorder, req)
	code := recorder.Result().StatusCode

	if code != http.StatusOK {
		t.Errorf("Expected invalid update to return response code 200. Got '%v'", code)
	}

	if !storedUser.PhoneConfirmed {
		t.Errorf("Expected phone confirmed to be true. Got false.")
	}

	AssertDBCalls(userController, t)
}

func TestVerifyEmpty(t *testing.T) {

	payload := []byte(``)

	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", "/users/SG-123456789/verify/mobile", bytes.NewBuffer(payload))
	rctx := chi.NewRouteContext()
	rctx.URLParams.Add("user_id", "SG-123456789")
	rctx.URLParams.Add("channel", "mobile")
	req = req.WithContext(context.WithValue(req.Context(), chi.RouteCtxKey, rctx))
	recorder := httptest.NewRecorder()

	userController.VerifyChannel(recorder, req)
	code := recorder.Result().StatusCode

	if code != http.StatusBadRequest {
		t.Errorf("Expected invalid update to return response code 400. Got '%v'", code)
	}

	AssertDBCalls(userController, t)
}

func TestChangeMobile(t *testing.T) {

	payload := []byte(`{
		"auth_id": "AUTH-123456789",
		"otp_code": "622250",
		"old_phone_no": "12345678",
		"old_isd_code": "65",
		"new_phone_no": "87654321",
		"new_isd_code": "88",
		"device": {
			"device_id": "777",
			"imei": "354530085441085",
			"mac_id": "FF:FF:FF:FF:FF:FF",
			"device_token": "gregergeretetegfv",
			"device_type": "Mobile",
			"device_os": "Android Oreo",
			"user_agent": "Chrome"
		}
	}`)

	storedUser := &model.UserModel{
		ExternalId: "SG-123456789",
		ISDCode:    "65",
		PhoneNo:    "12345678",
	}

	storedAuth := &model.OtpAuthenticationModel{
		AuthID:  "AUTH-123456789",
		UserId:  "SG-123456789",
		OtpCode: "622250",
	}

	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", "/users/SG-123456789/change/mobile", bytes.NewBuffer(payload))
	rctx := chi.NewRouteContext()
	rctx.URLParams.Add("user_id", "SG-123456789")
	rctx.URLParams.Add("channel", "mobile")
	req = req.WithContext(context.WithValue(req.Context(), chi.RouteCtxKey, rctx))
	recorder := httptest.NewRecorder()

	userController.Cache.(*MockCacheService).On("GetOTP", mock.Anything, mock.Anything).Return(storedAuth)
	userController.UserStore.(*mockUserStore).On("FindWithSoftDelete", mock.Anything, mock.Anything, mock.Anything).Return(storedUser, nil)
	userController.UserStore.(*mockUserStore).On("Update2", mock.Anything, mock.Anything).Return(nil)
	userController.OtpStore.(*mockOTPStore).On("Create", mock.Anything, mock.Anything).Return("5b5abda504409c55f1f6b91c", nil)
	userController.DeviceStore.(*mockDeviceStore).On("Find", mock.Anything, mock.Anything).Return(&model.UserDevice{DeviceId: "777"}, nil)
	userController.Cache.(*MockCacheService).On("DeleteOTP", mock.Anything, mock.Anything).Return(true)

	userController.ChangeChannel(recorder, req)
	code := recorder.Result().StatusCode

	if code != http.StatusOK {
		t.Errorf("Expected valid update to return response code 200. Got '%v'", code)
	}

	if storedUser.PhoneNo != "87654321" {
		t.Errorf("Expected new phone number to be 87654321. Got '%v'", storedUser.PhoneNo)
	}

	if storedUser.ISDCode != "88" {
		t.Errorf("Expected isd code to be 88. Got '%v'", storedUser.ISDCode)
	}

	AssertDBCalls(userController, t)
}

func TestChangeEmail(t *testing.T) {

	payload := []byte(`{
		"auth_id": "AUTH-123456789",
		"otp_code": "622250",
		"old_email": "hi@hotmail.com",
		"new_email": "bye@gmail.com",
		"device": {
			"device_id": "777",
			"imei": "354530085441085",
			"mac_id": "FF:FF:FF:FF:FF:FF",
			"device_token": "gregergeretetegfv",
			"device_type": "Mobile",
			"device_os": "Android Oreo",
			"user_agent": "Chrome"
		}
	}`)

	storedUser := &model.UserModel{
		ExternalId: "SG-123456789",
		Email:      "hi@hotmail.com",
	}

	storedAuth := &model.OtpAuthenticationModel{
		AuthID:  "AUTH-123456789",
		UserId:  "SG-123456789",
		OtpCode: "622250",
	}

	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", "/users/SG-123456789/change/email", bytes.NewBuffer(payload))
	rctx := chi.NewRouteContext()
	rctx.URLParams.Add("user_id", "SG-123456789")
	rctx.URLParams.Add("channel", "email")
	req = req.WithContext(context.WithValue(req.Context(), chi.RouteCtxKey, rctx))
	recorder := httptest.NewRecorder()

	userController.Cache.(*MockCacheService).On("GetOTP", mock.Anything, mock.Anything).Return(storedAuth)
	userController.UserStore.(*mockUserStore).On("FindWithSoftDelete", mock.Anything, mock.Anything, mock.Anything).Return(storedUser, nil)
	userController.UserStore.(*mockUserStore).On("Update2", mock.Anything, mock.Anything).Return(nil)
	userController.OtpStore.(*mockOTPStore).On("Create", mock.Anything, mock.Anything).Return("5b5abda504409c55f1f6b91c", nil)
	userController.DeviceStore.(*mockDeviceStore).On("Find", mock.Anything, mock.Anything).Return(&model.UserDevice{DeviceId: "777"}, nil)
	userController.Cache.(*MockCacheService).On("DeleteOTP", mock.Anything, mock.Anything).Return(true)

	userController.ChangeChannel(recorder, req)
	code := recorder.Result().StatusCode

	if code != http.StatusOK {
		t.Errorf("Expected valid update to return response code 200. Got '%v'", code)
	}

	if storedUser.Email != "bye@gmail.com" {
		t.Errorf("Expected new gmail to be bye@gmail.com. Got '%v'", storedUser.Email)
	}

	AssertDBCalls(userController, t)
}

func TestChangeEmpty(t *testing.T) {

	payload := []byte(``)

	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", "/users/SG-123456789/change/email", bytes.NewBuffer(payload))
	rctx := chi.NewRouteContext()
	rctx.URLParams.Add("user_id", "SG-123456789")
	rctx.URLParams.Add("channel", "email")
	req = req.WithContext(context.WithValue(req.Context(), chi.RouteCtxKey, rctx))
	recorder := httptest.NewRecorder()

	userController.ChangeChannel(recorder, req)
	code := recorder.Result().StatusCode

	if code != http.StatusBadRequest {
		t.Errorf("Expected invalid update to return response code 400. Got '%v'", code)
	}

	AssertDBCalls(userController, t)
}

func TestChangeMobileWithOverride(t *testing.T) {

	payload := []byte(`{
		"auth_id": "AUTH-123456789",
		"otp_code": "622250",
		"old_phone_no": "12345678",
		"old_isd_code": "65",
		"new_phone_no": "87654321",
		"new_isd_code": "88",
		"device": {
			"device_id": "777",
			"imei": "354530085441085",
			"mac_id": "FF:FF:FF:FF:FF:FF",
			"device_token": "gregergeretetegfv",
			"device_type": "Mobile",
			"device_os": "Android Oreo",
			"user_agent": "Chrome"
		},
		"override": true
	}`)

	storedUser := &model.UserModel{
		ExternalId: "SG-123456789",
		ISDCode:    "65",
		PhoneNo:    "12345678",
	}

	overrideUsers := []model.UserModel{
		model.UserModel{
			ExternalId:     "SG-000000000",
			ISDCode:        "65",
			PhoneNo:        "12345678",
			PhoneConfirmed: true,
		},
		model.UserModel{
			ExternalId:     "SG-111111111",
			ISDCode:        "65",
			PhoneNo:        "12345678",
			PhoneConfirmed: true,
		},
	}

	storedAuth := &model.OtpAuthenticationModel{
		AuthID:  "AUTH-123456789",
		UserId:  "SG-123456789",
		OtpCode: "622250",
	}

	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", "/users/SG-123456789/change/mobile", bytes.NewBuffer(payload))
	rctx := chi.NewRouteContext()
	rctx.URLParams.Add("user_id", "SG-123456789")
	rctx.URLParams.Add("channel", "mobile")
	req = req.WithContext(context.WithValue(req.Context(), chi.RouteCtxKey, rctx))
	recorder := httptest.NewRecorder()

	userController.Cache.(*MockCacheService).On("GetOTP", mock.Anything, mock.Anything).Return(storedAuth)
	userController.UserStore.(*mockUserStore).On("FindWithSoftDelete", mock.Anything, mock.Anything, mock.Anything).Return(storedUser, nil)
	userController.UserStore.(*mockUserStore).On("Update2", mock.Anything, mock.Anything).Return(nil)
	userController.UserStore.(*mockUserStore).On("FindAllWithSoftDelete", mock.Anything, mock.Anything, mock.Anything).Return(overrideUsers, nil)
	userController.OtpStore.(*mockOTPStore).On("Create", mock.Anything, mock.Anything).Return("5b5abda504409c55f1f6b91c", nil)
	userController.DeviceStore.(*mockDeviceStore).On("Find", mock.Anything, mock.Anything).Return(&model.UserDevice{DeviceId: "777"}, nil)
	userController.UserAuditStore.(*mockUserAuditStore).On("Create", mock.Anything, mock.Anything).Return("5b5abda504409c55f1f6b91c", nil)
	userController.Cache.(*MockCacheService).On("DeleteOTP", mock.Anything, mock.Anything).Return(true)

	userController.ChangeChannel(recorder, req)
	code := recorder.Result().StatusCode

	if code != http.StatusOK {
		t.Errorf("Expected valid update to return response code 200. Got '%v'", code)
	}

	if storedUser.PhoneNo != "87654321" {
		t.Errorf("Expected new phone number to be 87654321. Got '%v'", storedUser.PhoneNo)
	}

	if storedUser.ISDCode != "88" {
		t.Errorf("Expected isd code to be 88. Got '%v'", storedUser.ISDCode)
	}

	for _, overUser := range overrideUsers {
		if overUser.PhoneNo != "" {
			t.Errorf("Expected overrode user's new phone number to be blank. Got '%v'", overUser.PhoneNo)
		}

		if overUser.ISDCode != "" {
			t.Errorf("Expected overrode user's isd code to be blank. Got '%v'", overUser.ISDCode)
		}

		if overUser.PhoneConfirmed != false {
			t.Errorf("Expected overrode user's phone confirmed to be false. Got '%v'", overUser.PhoneConfirmed)
		}
	}

	AssertDBCalls(userController, t)
}

func TestCreateUserWithOTP(t *testing.T) {

	payload := []byte(`{
        "first_name":"srinand",
        "last_name":"challur",
        "dob":"1990-12-11",
        "password":"asdfasdf",
        "phone_no":"87682708",
        "image_url":"http://google.com",
        "isd_code":"65",
        "address_line_1":"04-125",
        "address_line_2":"20 Queen's Close",
        "address_line_3":"140090",
        "city":"Singapore",
        "state":"Singapore",
        "country":"Singapore",
        "postal_code":"140090",
		"device": {
			"device_id": "777",
			"imei": "354530085441085",
			"mac_id": "FF:FF:FF:FF:FF:FF",
			"device_token": "gregergeretetegfv",
			"device_type": "Mobile",
			"device_os": "Android Oreo",
			"user_agent": "Chrome"
		},
		"auth_id": "AUTH-123456789",
		"otp_code": "622250",
		"security_questions": [{"question": "hi", "answer": "bye"}, {"question": "hi", "answer": "bye"}]
	}`)

	storedAuth := &model.OtpAuthenticationModel{
		AuthID:  "AUTH-123456789",
		UserId:  "SG-123456789",
		OtpCode: "622250",
	}

	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", "/users/register/otp", bytes.NewBuffer(payload))
	recorder := httptest.NewRecorder()

	userController.Cache.(*MockCacheService).On("GetOTP", mock.Anything, mock.Anything).Return(storedAuth)
	userController.UserStore.(*mockUserStore).On("Create", mock.Anything, mock.Anything).Return("5b5abda504409c55f1f6b91c", nil)
	userController.OtpStore.(*mockOTPStore).On("Create", mock.Anything, mock.Anything).Return("5b5abda504409c55f1f6b91c", nil)
	userController.DeviceStore.(*mockDeviceStore).On("Find", mock.Anything, mock.Anything).Return(&model.UserDevice{DeviceId: "777"}, nil)
	userController.LoginStore.(*mockLoginStore).On("Find", mock.Anything, mock.Anything).Return(nil, errors.New("mongo: no documents in result"))
	userController.LoginStore.(*mockLoginStore).On("Create", mock.Anything, mock.Anything).Return("5b5abda504409c55f1f6b91c", nil)
	userController.Cache.(*MockCacheService).On("DeleteOTP", mock.Anything, mock.Anything).Return(true)

	userController.RegisterWithOTP(recorder, req)
	code := recorder.Result().StatusCode

	if code != http.StatusCreated {
		t.Errorf("Expected valid update to return response code 201. Got '%v'", code)
	}

	var m map[string]interface{}
	json.Unmarshal(recorder.Body.Bytes(), &m)

	if m["auth_token"] == "" {
		t.Errorf("Expected non empty auth token. Got empty.")
	}

	AssertDBCalls(userController, t)
}

func TestCreateUserWithOTPOverride(t *testing.T) {

	payload := []byte(`{
        "first_name":"srinand",
        "last_name":"challur",
        "dob":"1990-12-11",
        "password":"asdfasdf",
        "phone_no":"87682708",
        "image_url":"http://google.com",
        "isd_code":"65",
        "address_line_1":"04-125",
        "address_line_2":"20 Queen's Close",
        "address_line_3":"140090",
        "city":"Singapore",
        "state":"Singapore",
        "country":"Singapore",
        "postal_code":"140090",
		"device": {
			"device_id": "777",
			"imei": "354530085441085",
			"mac_id": "FF:FF:FF:FF:FF:FF",
			"device_token": "gregergeretetegfv",
			"device_type": "Mobile",
			"device_os": "Android Oreo",
			"user_agent": "Chrome"
		},
		"auth_id": "AUTH-123456789",
		"otp_code": "622250",
		"override": true,
		"security_questions": [{"question": "hi", "answer": "bye"}, {"question": "hi", "answer": "bye"}]
	}`)

	overrideUsers := []model.UserModel{
		model.UserModel{
			ExternalId:     "SG-000000000",
			ISDCode:        "65",
			PhoneNo:        "12345678",
			PhoneConfirmed: true,
		},
		model.UserModel{
			ExternalId:     "SG-111111111",
			ISDCode:        "65",
			PhoneNo:        "12345678",
			PhoneConfirmed: true,
		},
	}

	storedAuth := &model.OtpAuthenticationModel{
		AuthID:  "AUTH-123456789",
		UserId:  "SG-123456789",
		OtpCode: "622250",
	}

	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", "/users/register/otp", bytes.NewBuffer(payload))
	recorder := httptest.NewRecorder()

	userController.Cache.(*MockCacheService).On("GetOTP", mock.Anything, mock.Anything).Return(storedAuth)
	userController.UserStore.(*mockUserStore).On("Update2", mock.Anything, mock.Anything).Return(nil)
	userController.UserStore.(*mockUserStore).On("Create", mock.Anything, mock.Anything).Return("5b5abda504409c55f1f6b91c", nil)
	userController.UserStore.(*mockUserStore).On("FindAllWithSoftDelete", mock.Anything, mock.Anything, mock.Anything).Return(overrideUsers, nil)
	userController.OtpStore.(*mockOTPStore).On("Create", mock.Anything, mock.Anything).Return("5b5abda504409c55f1f6b91c", nil)
	userController.DeviceStore.(*mockDeviceStore).On("Find", mock.Anything, mock.Anything).Return(&model.UserDevice{DeviceId: "777"}, nil)
	userController.UserAuditStore.(*mockUserAuditStore).On("Create", mock.Anything, mock.Anything).Return("5b5abda504409c55f1f6b91c", nil)
	userController.LoginStore.(*mockLoginStore).On("Find", mock.Anything, mock.Anything).Return(nil, errors.New("mongo: no documents in result"))
	userController.LoginStore.(*mockLoginStore).On("Create", mock.Anything, mock.Anything).Return("5b5abda504409c55f1f6b91c", nil)
	userController.Cache.(*MockCacheService).On("DeleteOTP", mock.Anything, mock.Anything).Return(true)

	userController.RegisterWithOTP(recorder, req)
	code := recorder.Result().StatusCode

	if code != http.StatusCreated {
		t.Errorf("Expected valid update to return response code 201. Got '%v'", code)
	}

	for _, overUser := range overrideUsers {
		if overUser.PhoneNo != "" {
			t.Errorf("Expected overrode user's new phone number to be empty. Got '%v'", overUser.PhoneNo)
		}

		if overUser.ISDCode != "" {
			t.Errorf("Expected overrode user's isd code to be empty. Got '%v'", overUser.ISDCode)
		}

		if overUser.PhoneConfirmed != false {
			t.Errorf("Expected overrode user's phone confirmed to be false. Got '%v'", overUser.PhoneConfirmed)
		}
	}

	var m map[string]interface{}
	json.Unmarshal(recorder.Body.Bytes(), &m)

	if m["auth_token"] == "" {
		t.Errorf("Expected non empty auth token. Got empty.")
	}

	AssertDBCalls(userController, t)
}

func TestCreateUserWithOTPEmpty(t *testing.T) {

	payload := []byte(``)

	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", "/users/register/otp", bytes.NewBuffer(payload))
	recorder := httptest.NewRecorder()

	userController.RegisterWithOTP(recorder, req)
	code := recorder.Result().StatusCode

	if code != http.StatusBadRequest {
		t.Errorf("Expected valid update to return response code 400. Got '%v'", code)
	}

	AssertDBCalls(userController, t)
}

func TestMigrateUserWithBadPayload(t *testing.T) {

	payload := []byte(``)

	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", USER_MIGRATE_API, bytes.NewBuffer(payload))
	recorder := httptest.NewRecorder()

	userController.MigrateUser(recorder, req)
	code := recorder.Result().StatusCode

	assert.Equal(t, http.StatusBadRequest, code)
}

func TestMigrateUserWithEmptyEmail(t *testing.T) {

	payload := []byte(`{
	"user" : {
		"first_name" : "amit",
		"last_name" : "kumar",
		"dob" : "1990-12-11",
		"image_url" : "",
		"password" : "asasasasa",
		"phone_no" : "12345679",
		"address_line_1" : "home",
		"address_line_2" : "office",
		"address_line_3" : "",
		"city" : "singapore",
		"state" : "singapore",
		"country" : "singapore",
		"postal_code" : "159557",
		"telco_user" : true,
		"security_questions" : []
	}
}`)

	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", USER_MIGRATE_API, bytes.NewBuffer(payload))
	recorder := httptest.NewRecorder()

	userController.MigrateUser(recorder, req)
	code := recorder.Result().StatusCode

	assert.Equal(t, http.StatusBadRequest, code)
}

func TestMigrateUserWithBadStructurePayload(t *testing.T) {

	payload := []byte(`{
	"user" : {
		"first_name" : "Shelly",
		"last_name" : "Cooper",
		"dob" : "1990-12-11",
		"image_url" : "",
		"email" : "test2@circles.asia",
		"password" : "asasasasa",
		"phone_no" : "12345678",
		"isd_code" : "65",
		"address_line_1" : "home",
		"address_line_2" : "office",
		"address_line_3" : "",
		"city" : "singapore",
		"state" : "singapore",
		"country" : "singapore",
		"postal_code" : "159557",
		"telco_user" : true,
		"security_questions" : []
	},
	"user_device" : {
		"user_id" : "65-12321211",
		"device_id" : "device1232",
		"imei" : "asasas",
		"mac_id" : "1212sdw",
		"device_token" : "Qwqwqw",
		"device_type" : "web",
		"device_os" : "mac",
		"user_agent" : "postman",
		"status" : "active"
	},
	"telco_info" : {
		"customer_name" : "kumar Jon",
		"resigtered_email" : "test1@circes.asia",
		"billing_email" : "test2@circes.asia",
		"isd_code": "65",
		"phone_no" : "123456778",
		"creation_date" : "2018-07-10",
		"activation_date" : "2018-07-10",
		"nric" : "G3478043P",
		"dob" : "1990-07-06",
		"circles_info" : {
			"customor_account" : "LW2017215",
			"billing_account" : "LW2017216",
			"service_instance_account" : "LW2017218",
			"service_account" : "LW2017217",
			"account_sub_status" : "ACS01",
			"referral_code" : "34REGOFF",
			"order_ref_no" : "000001531105932692",
			"base_plan" : "CirclesOne",
			"portin_status" : "Normal",
			"common_status_id" : "CST01",
			"number_status" : "Active",
			"join_date" : "2018-07-10"
		}
	}
}`)
	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", USER_MIGRATE_API, bytes.NewBuffer(payload))
	recorder := httptest.NewRecorder()

	userController.MigrateUser(recorder, req)
	code := recorder.Result().StatusCode

	assert.Equal(t, http.StatusBadRequest, code)
}

func TestMigrateUserWithNewUserAndTelcoAccount(t *testing.T) {

	payload := []byte(`{
	"user" : {
		"first_name" : "Shelly",
		"last_name" : "Cooper",
		"dob" : "1990-12-11",
		"image_url" : "",
		"email" : "test2@circles.asia",
		"password" : "asasasasa",
		"phone_no" : "12345678",
		"isd_code" : "65",
		"address_line_1" : "home",
		"address_line_2" : "office",
		"address_line_3" : "",
		"city" : "singapore",
		"state" : "singapore",
		"country" : "singapore",
		"postal_code" : "159557",
		"telco_user" : true,
		"security_questions" : []
	},
	"user_devices" : [{
		"user_id" : "65-12321211",
		"device_id" : "device1232",
		"imei" : "asasas",
		"mac_id" : "1212sdw",
		"device_token" : "Qwqwqw",
		"device_type" : "Mobile",
		"device_os" : "mac",
		"user_agent" : "postman",
		"status" : "active"
	}],
	"telco_info" : [{
		"customer_name" : "kumar Jon",
		"resigtered_email" : "test1@circes.asia",
		"billing_email" : "test2@circes.asia",
		"isd_code": "65",
		"phone_no" : "123456778",
		"creation_date" : "2018-07-10",
		"activation_date" : "2018-07-10",
		"identifier_info" : {
			"nric" : "G3478043P",
			"dob" : "1990-07-06"
		},
		"circles_info" : {
			"customor_account" : "LW2017215",
			"billing_account" : "LW2017216",
			"service_instance_account" : "LW2017218",
			"service_account" : "LW2017217",
			"account_sub_status" : "ACS01",
			"referral_code" : "34REGOFF",
			"order_ref_no" : "000001531105932692",
			"base_plan" : "CirclesOne",
			"portin_status" : "Normal",
			"common_status_id" : "CST01",
			"number_status" : "Active",
			"join_date" : "2018-07-10"
		}
	}]
}`)
	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", USER_MIGRATE_API, bytes.NewBuffer(payload))
	recorder := httptest.NewRecorder()

	userController.UserStore.(*mockUserStore).On("FindWithSoftDelete", mock.Anything, mock.Anything, mock.Anything).Return(nil, nil)
	userController.UserStore.(*mockUserStore).On("Create", mock.Anything, mock.Anything).Return(USER_ID, nil)
	userController.UserStore.(*mockUserStore).On("Update2", mock.Anything, mock.Anything).Return(nil)
	userController.DeviceStore.(*mockDeviceStore).On("Find", mock.Anything, mock.Anything).Return(nil,
		errors.New("mongo: no documents in result"))
	userController.DeviceStore.(*mockDeviceStore).On("Create", mock.Anything, mock.Anything).Return(USER_ID, nil)
	userController.TelcoInfoStore.(*mockTelcoAccountStore).On("Find", mock.Anything, mock.Anything).Return(nil, nil)
	userController.TelcoInfoStore.(*mockTelcoAccountStore).On("Create", mock.Anything, mock.Anything).Return(USER_ID, nil)
	//userController.LoginStore.(*mockLoginStore).On("Find", mock.Anything).Return(nil, errors.New("mongo: no documents in result"))
	//userController.LoginStore.(*mockLoginStore).On("Create", mock.Anything).Return("5b9f7014c430439139f34987", nil)

	userController.MigrateUser(recorder, req)
	code := recorder.Result().StatusCode

	var m map[string]interface{}
	json.Unmarshal(recorder.Body.Bytes(), &m)

	assert.Equal(t, http.StatusOK, code)
	assert.Nil(t, m["auth_token"])
	AssertDBCalls(userController, t)
}

func TestMigrateUserWithOldUserAndNewTelcoAccount(t *testing.T) {

	payload := []byte(`{
	"user" : {
		"first_name" : "Shelly",
		"last_name" : "Cooper",
		"dob" : "1990-12-11",
		"image_url" : "",
		"email" : "test2@circles.asia",
		"password" : "asasasasa",
		"phone_no" : "12345678",
		"isd_code" : "65",
		"address_line_1" : "home",
		"address_line_2" : "office",
		"address_line_3" : "",
		"city" : "singapore",
		"state" : "singapore",
		"country" : "singapore",
		"postal_code" : "159557",
		"telco_user" : true,
		"security_questions" : []
	},
	"user_devices" : [{
		"user_id" : "65-12321211",
		"device_id" : "device1232",
		"imei" : "asasas",
		"mac_id" : "1212sdw",
		"device_token" : "Qwqwqw",
		"device_type" : "Mobile",
		"device_os" : "mac",
		"user_agent" : "postman",
		"status" : "active"
	}],
	"telco_info" : [{
		"customer_name" : "kumar Jon",
		"resigtered_email" : "test1@circes.asia",
		"billing_email" : "test2@circes.asia",
		"isd_code": "65",
		"phone_no" : "123456778",
		"creation_date" : "2018-07-10",
		"activation_date" : "2018-07-10",
		"identifier_info" : {
			"nric" : "G3478043P",
			"dob" : "1990-07-06"
		},
		"circles_info" : {
			"customor_account" : "LW2017215",
			"billing_account" : "LW2017216",
			"service_instance_account" : "LW2017218",
			"service_account" : "LW2017217",
			"account_sub_status" : "ACS01",
			"referral_code" : "34REGOFF",
			"order_ref_no" : "000001531105932692",
			"base_plan" : "CirclesOne",
			"portin_status" : "Normal",
			"common_status_id" : "CST01",
			"number_status" : "Active",
			"join_date" : "2018-07-10"
		}
	}]
}`)
	storedUser := &model.UserModel{
		Id:         USER_ID,
		ExternalId: "SG-123456789",
		Email:      "hi@hotmail.com",
		FirstName:  "Jon",
		LastName:   "Dey",
	}

	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", USER_MIGRATE_API, bytes.NewBuffer(payload))
	recorder := httptest.NewRecorder()

	userController.UserStore.(*mockUserStore).On("FindWithSoftDelete", mock.Anything, mock.Anything, mock.Anything).Return(storedUser, nil)
	userController.UserStore.(*mockUserStore).On("Update2", mock.Anything, mock.Anything).Return(nil)
	userController.DeviceStore.(*mockDeviceStore).On("Find", mock.Anything, mock.Anything).Return(nil,
		errors.New("mongo: no documents in result"))
	userController.DeviceStore.(*mockDeviceStore).On("Create", mock.Anything, mock.Anything).Return(USER_ID, nil)
	userController.TelcoInfoStore.(*mockTelcoAccountStore).On("Find", mock.Anything, mock.Anything).Return(nil, nil)
	userController.TelcoInfoStore.(*mockTelcoAccountStore).On("Create", mock.Anything, mock.Anything).Return(USER_ID, nil)
	//userController.LoginStore.(*mockLoginStore).On("Find", mock.Anything).Return(&model.UserLogin{}, nil)
	//userController.LoginStore.(*mockLoginStore).On("Update", mock.Anything).Return(nil)

	userController.MigrateUser(recorder, req)
	code := recorder.Result().StatusCode

	var m map[string]interface{}
	json.Unmarshal(recorder.Body.Bytes(), &m)

	assert.Equal(t, http.StatusOK, code)
	assert.Nil(t, m["auth_token"])
	AssertDBCalls(userController, t)
}

func TestUnlinkTelcoInfo(t *testing.T) {

	payload := []byte(`{
		"isd_code": "65",
		"phone_no": "87654321",
		"user_id": "SG-123456789"
	}`)
	storedUser := &model.UserModel{
		ExternalId:      "SG-123456789",
		ISDCode:         "65",
		PhoneNo:         "87654321",
		CirclesAccounts: map[string]interface{}{"65::87654321": nil, "65::12345678": nil},
		PhoneConfirmed:  true,
	}
	storedTelcoInfo := &model.TelcoInfo{
		ISDCode: "65",
		PhoneNo: "87654321",
		Status:  "Active",
	}

	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", "/users/unlink", bytes.NewBuffer(payload))
	recorder := httptest.NewRecorder()

	userController.UserStore.(*mockUserStore).On("FindWithSoftDelete", mock.Anything, mock.Anything, mock.Anything).Return(storedUser, nil)
	userController.UserStore.(*mockUserStore).On("Update2", mock.Anything, mock.Anything).Return(nil)
	userController.TelcoInfoStore.(*mockTelcoAccountStore).On("Find", mock.Anything, mock.Anything).Return(storedTelcoInfo, nil)
	userController.TelcoInfoStore.(*mockTelcoAccountStore).On("Update", mock.Anything, mock.Anything).Return(nil)

	userController.UnlinkTelcoUser(recorder, req)
	code := recorder.Result().StatusCode

	var m map[string]interface{}
	json.Unmarshal(recorder.Body.Bytes(), &m)

	assert.Equal(t, http.StatusOK, code)
	assert.Equal(t, storedTelcoInfo.Status, "Inactive")
	assert.False(t, storedUser.PhoneConfirmed)
	assert.Equal(t, storedUser.CirclesAccounts, map[string]interface{}{"65::12345678": nil})
	AssertDBCalls(userController, t)
}

func TestUnlinkTelcoInfoEmptyPayload(t *testing.T) {

	payload := []byte(``)

	userController := NewMockUserController()

	req, _ := http.NewRequest("POST", "/users/unlink", bytes.NewBuffer(payload))
	recorder := httptest.NewRecorder()

	userController.UnlinkTelcoUser(recorder, req)
	code := recorder.Result().StatusCode

	assert.Equal(t, http.StatusBadRequest, code)
	AssertDBCalls(userController, t)
}

func TestMergeUser(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		src := []byte(`{
			"user_id": "SG-BPB37K3T968G",
			"email": "hello@gmail.com",
			"otp_code": "1234",
			"auth_id": "AUTH-1234"
		}`)

		primaryUser := &model.UserModel{
			ExternalId:     "SG-BPB37K3T968G",
			ISDCode:        "65",
			PhoneNo:        "87654321",
			EmailConfirmed: false,
			PhoneConfirmed: true,
		}

		secondaryUser := &model.UserModel{
			ExternalId:     "SG-BP123456777",
			Email:          "hello@gmail.com",
			Password:       "djfklsdjflkjsfdlkjeergnejgriojergio",
			EmailConfirmed: true,
			PhoneConfirmed: false,
			Source:         "FACEBOOK",
			CirclesAccounts: map[string]interface{}{
				"65::88888888": map[string]interface{}{
					"Test": 123,
				},
				"65::77777777": map[string]interface{}{
					"Test": "test",
				},
			},
			SecurityQuestions: []model.SecurityQuestion{
				model.SecurityQuestion{
					Question: "What is your favourite football team?",
					Answer:   "Manchester City",
				},
				model.SecurityQuestion{
					Question: "What is your favourite telco?",
					Answer:   "Circles",
				},
			},
		}

		expected := &model.UserModel{
			ExternalId:     "SG-BPB37K3T968G",
			ISDCode:        "65",
			PhoneNo:        "87654321",
			Email:          "hello@gmail.com",
			Password:       "djfklsdjflkjsfdlkjeergnejgriojergio",
			EmailConfirmed: true,
			PhoneConfirmed: true,
			Source:         "FACEBOOK",
			CirclesAccounts: map[string]interface{}{
				"65::88888888": map[string]interface{}{
					"Test": 123,
				},
				"65::77777777": map[string]interface{}{
					"Test": "test",
				},
			},
			SecurityQuestions: []model.SecurityQuestion{
				model.SecurityQuestion{
					Question: "What is your favourite football team?",
					Answer:   "Manchester City",
				},
				model.SecurityQuestion{
					Question: "What is your favourite telco?",
					Answer:   "Circles",
				},
			},
			LinkedUserID: "SG-BP123456777",
		}

		var mergeRequest model.UserMergeRequestModel
		json.Unmarshal(src, &mergeRequest)

		userController := NewMockUserController()
		userController.UserStore.(*mockUserStore).On("FindWithSoftDelete", context.Background(), false, datastore.M{"email": mergeRequest.Email}).Return(secondaryUser, nil)
		userController.UserStore.(*mockUserStore).On("FindWithSoftDelete", context.Background(), false, datastore.M{"external_id": mergeRequest.UserID}).Return(primaryUser, nil)
		userController.UserStore.(*mockUserStore).On("Update2", context.Background(), secondaryUser).Return(nil)
		userController.UserStore.(*mockUserStore).On("Update2", context.Background(), primaryUser).Return(nil)
		userController.UserStore.(*mockUserStore).On("SoftDelete", context.Background(), secondaryUser).Return(nil)

		userController.Cache.(*MockCacheService).On("GetOTP", context.Background(), "user_service_otpauth_AUTH-1234").Return(&model.OtpAuthenticationModel{AuthID: "AUTH-1234", OtpCode: "1234"})
		userController.Cache.(*MockCacheService).On("DeleteOTP", context.Background(), "user_service_otpauth_AUTH-1234").Return(true)

		userController.UserAuditStore.(*mockUserAuditStore).On("Create", context.Background(), mock.Anything).Return("dfsfsdfsfdsf", nil)

		userController.OtpStore.(*mockOTPStore).On("Create", context.Background(), mock.Anything).Return("fsdfsdfdsfew", nil)

		mergedUser, _ := userController.mergeUser(context.Background(), &mergeRequest, true)
		mergedUser.UpdatedAt = 0

		if !assert.ObjectsAreEqualValues(expected, mergedUser) {
			t.Errorf("User field values are not merged correctly")
		}
		AssertDBCalls(userController, t)
	})

	t.Run("invalid merge", func(t *testing.T) {
		var invalid = []struct {
			description string
			src         []byte
			primary     model.UserModel
			secondary   model.UserModel
		}{
			{
				"merge self",
				[]byte(`{
					"user_id": "SG-BPB37K3T968G",
					"email": "hello@gmail.com",
					"otp_code": "1234",
					"auth_id": "AUTH-1234"
				}`),
				model.UserModel{
					ExternalId:     "SG-BPB37K3T968G",
					Email:          "hello@gmail.com",
					ISDCode:        "65",
					PhoneNo:        "87654321",
					EmailConfirmed: false,
					PhoneConfirmed: true,
				},
				model.UserModel{
					ExternalId:     "SG-BPB37K3T968G",
					Email:          "hello@gmail.com",
					ISDCode:        "65",
					PhoneNo:        "87654321",
					EmailConfirmed: false,
					PhoneConfirmed: true,
				},
			},

			{
				"merge primary with email",
				[]byte(`{
					"user_id": "SG-BPB37K3T968G",
					"email": "hello@gmail.com",
					"otp_code": "1234",
					"auth_id": "AUTH-1234"
				}`),
				model.UserModel{
					ExternalId:     "SG-BPB37K3T968G",
					Email:          "bye@gmail.com",
					ISDCode:        "65",
					PhoneNo:        "87654321",
					EmailConfirmed: false,
					PhoneConfirmed: true,
				},
				model.UserModel{
					ExternalId:     "SG-BPB37123456",
					Email:          "hello@gmail.com",
					EmailConfirmed: false,
					PhoneConfirmed: true,
				},
			},

			{
				"merge primary with link",
				[]byte(`{
					"user_id": "SG-BPB37K3T968G",
					"email": "hello@gmail.com",
					"otp_code": "1234",
					"auth_id": "AUTH-1234"
				}`),
				model.UserModel{
					ExternalId:     "SG-BPB37K3T968G",
					ISDCode:        "65",
					PhoneNo:        "87654321",
					EmailConfirmed: false,
					PhoneConfirmed: true,
					LinkedUserID:   "SG-123456789",
				},
				model.UserModel{
					ExternalId:     "SG-BPB37123456",
					Email:          "hello@gmail.com",
					EmailConfirmed: false,
					PhoneConfirmed: true,
				},
			},

			{
				"merge secondary with phone number",
				[]byte(`{
					"user_id": "SG-BPB37K3T968G",
					"email": "hello@gmail.com",
					"otp_code": "1234",
					"auth_id": "AUTH-1234"
				}`),
				model.UserModel{
					ExternalId:     "SG-BPB37K3T968G",
					ISDCode:        "65",
					PhoneNo:        "87654321",
					EmailConfirmed: false,
					PhoneConfirmed: true,
				},
				model.UserModel{
					ExternalId:     "SG-BPB37123456",
					Email:          "hello@gmail.com",
					ISDCode:        "65",
					PhoneNo:        "98765432",
					EmailConfirmed: false,
					PhoneConfirmed: true,
				},
			},
		}

		for _, tt := range invalid {
			t.Logf("Test description %v", tt.description)
			var mergeRequest model.UserMergeRequestModel
			json.Unmarshal(tt.src, &mergeRequest)

			userController := NewMockUserController()

			userController.UserStore.(*mockUserStore).On("FindWithSoftDelete", context.Background(), false, datastore.M{"email": mergeRequest.Email}).Return(&tt.secondary, nil)
			userController.UserStore.(*mockUserStore).On("FindWithSoftDelete", context.Background(), false, datastore.M{"external_id": mergeRequest.UserID}).Return(&tt.primary, nil)

			_, err := userController.mergeUser(context.Background(), &mergeRequest, true)
			assert.Error(t, err)
			assert.Equal(t, constant.UsersCannotBeMerged, err.Error())
			AssertDBCalls(userController, t)
		}
	})
}
