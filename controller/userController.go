package controller

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/libertywireless/roaming-service/config"
	"bitbucket.org/libertywireless/roaming-service/constant"
	"bitbucket.org/libertywireless/roaming-service/constant/codes"
	"bitbucket.org/libertywireless/roaming-service/datastore"
	"bitbucket.org/libertywireless/roaming-service/internal/errors"
	"bitbucket.org/libertywireless/roaming-service/internal/mongopaging"
	"bitbucket.org/libertywireless/roaming-service/logging"
	"bitbucket.org/libertywireless/roaming-service/model"
	"bitbucket.org/libertywireless/roaming-service/service"
	"github.com/go-chi/chi"
	"github.com/mongodb/mongo-go-driver/bson/objectid"
	"gopkg.in/mgo.v2/bson"
)

type UserController struct {
	UserStore      datastore.UserStore
	config         *config.GeneralConfig
}

// CreateUser godoc
// @Summary register new user
// @Description register new user with email or phone number. if auth_required param is true, this api will return auth_token instead of model.UserModel.
// @Tags user
// @Accept  json
// @Produce  json
// @Param payload body model.UserRegisterRequestModel true "email or phone_no should not be blank."
// @Success 201 {object} model.UserModel
// @Failure 400 {object} model.ErrorResponse
// @Failure 404 {object} model.ErrorResponse
// @Failure 500 {object} model.ErrorResponse
// @Router /users/register [post]
func (a *UserController) CreateUser(w http.ResponseWriter, r *http.Request) {

	log := logging.GetLogger(r.Context())

	var userReg model.UserRegisterRequestModel
	err := json.NewDecoder(r.Body).Decode(&userReg)
	defer r.Body.Close()
	if err != nil {
		err = errors.New(codes.FailedToDecodeRequestBodyCode, constant.DecodeRequestBodyErr)
		service.WriteError(r, w, err)
		return
	}
	userReg.Email = strings.ToLower(userReg.Email)

	userReg.Source = userReg.GetLoginSource()
	if userReg.ExternalId == "" {
		userReg.ExternalId = service.GenExternalID(a.config.CountryCode)
	}

	if userReg.Email == "" && (!service.ValidateSecurityQuestions(userReg.SecurityQuestions)) && service.IsSecurityQuestionCheck(a.config) {
		err := errors.New(codes.ReqSecurityQuesCode, constant.ReqSecurityQues)
		log.Logger.WithError(err).Error("Create user api fail")
		service.WriteError(r, w, err)
		return
	}

	err = userReg.Validate(r.Context())
	if err != nil {
		log.Logger.WithError(err).Error("Create user api fail")
		service.WriteError(r, w, err)
		return
	}

	log.Logger.WithField(constant.Identifier, userReg.ExternalId).Info("externalId in create user api")

	user := service.MakeUserModel(userReg)
	user.Password = service.HashAndSalt(user.Password)

	id, err := a.UserStore.Create(r.Context(), user)

	user.Id = id

	if err != nil {
		if err.Error() == constant.DuplicateAccess {
			log.Logger.WithError(err).Error("Create user api fail")
			err = errors.New(codes.DuplicateAccessCode, constant.DuplicateAccess)
			service.WriteError(r, w, err)
			return
		}
		err = errors.New(codes.InternalErrorCode, constant.ServerIssue)
		log.Logger.WithError(err).Error("Create user api fail")
		service.WriteError(r, w, err)
		return
	}

	userResp := *user
	userResp.Password = ""
	service.WriteJSONWithStatus(w, http.StatusCreated, userResp)
	
	log.Logger.WithField(constant.Identifier, userReg.ExternalId).Info("Create user api success")
}

// UpdateUser godoc
// @Summary update user information
// @Description Update existing user information
// @Tags user
// @Accept  json
// @Produce  json
// @Param payload body model.UserUpdateRequestModel true "Request Payload"
// @Success 200
// @Failure 400 {object} model.ErrorResponse
// @Failure 404 {object} model.ErrorResponse
// @Failure 500 {object} model.ErrorResponse
// @Router /users/update [post]
func (a *UserController) UpdateUser(w http.ResponseWriter, r *http.Request) {

	log := logging.GetLogger(r.Context())

	var userUpdate model.UserUpdateRequestModel
	err := model.DecodeAndValidate(r, &userUpdate)
	if err != nil {
		log.Logger.WithError(err).Error("update user fail")
		service.WriteError(r, w, err)
		return
	}
	userUpdate.Email = strings.ToLower(userUpdate.Email)

	log.Logger.WithField(constant.Payload, fmt.Sprintf("%+v\n", userUpdate)).Debug("update user api")
	log.Logger.WithField(constant.Identifier, userUpdate.ExternalId).Info("update user with externalId")

	user, err := a.UserStore.FindWithSoftDelete(r.Context(), false, datastore.M{"external_id": userUpdate.ExternalId})
	if err != nil {
		log.Logger.WithError(err).Error("update user api fail")
		if err.Error() == constant.DuplicateAccess {
			err = errors.New(codes.DuplicateAccessCode, constant.DuplicateAccess)
			service.WriteError(r, w, err)
			return
		}
		err = errors.New(codes.InternalErrorCode, constant.ServerIssue)
		service.WriteError(r, w, err)
		return
	}

	phone := userUpdate.PhoneNo
	isd := userUpdate.ISDCode

	userUpdate.EmailConfirmed = user.EmailConfirmed
	userUpdate.PhoneConfirmed = user.PhoneConfirmed
	userUpdate.PhoneNo = user.PhoneNo
	userUpdate.ISDCode = user.ISDCode

	currentTime := time.Now().Unix()
	userUpdate.UpdatedAt = currentTime
	if userUpdate.Email != "" && phone != "" && isd != "" {
		userUpdate.EmailConfirmed = false
		userUpdate.PhoneConfirmed = false
	} else if phone != "" && isd != "" {
		userUpdate.PhoneNo = phone
		userUpdate.ISDCode = isd
		userUpdate.PhoneConfirmed = false
	} else if userUpdate.Email != "" {
		userUpdate.EmailConfirmed = false
	}

	err = a.UserStore.Update(r.Context(), user, &userUpdate)
	if err != nil {
		switch err.Error() {
		case constant.DuplicateAccess:
			err = errors.New(codes.DuplicateAccessCode, constant.DuplicateAccess)
			break
		case constant.StaleDataErr:
			err = errors.New(codes.LoginStaleDataErrorCode, constant.StaleDataErr)
			break
		default:
			err = errors.New(codes.InternalErrorCode, constant.ServerIssue)
		}
		service.WriteError(r, w, err)
		return
	}

	log.Logger.WithField(constant.Identifier, userUpdate.ExternalId).Info("update user api success")
	service.WriteWithStatus(w, http.StatusOK)
}


// GetUser godoc
// @Summary retrieve user profile
// @Description get user by User ID
// @Tags user
// @Accept  json
// @Produce  json
// @Param user_id path string true "User ID"
// @Param devices query bool false "get user dervies by devices"
// @Param deleted query bool false "get deleted user"
// @Success 200 {object} model.UserModel
// @Failure 400 {object} model.ErrorResponse
// @Failure 404 {object} model.ErrorResponse
// @Failure 500 {object} model.ErrorResponse
// @Router /users/{user_id}/profile [get]
func (a *UserController) GetUser(w http.ResponseWriter, r *http.Request) {

	log := logging.GetLogger(r.Context())

	userId := chi.URLParam(r, "user_id")

	log.Logger.WithField("userId", userId).Info("GetUser api call")

	rawId, _ := objectid.FromHex(userId)
	// isGetDevices, _ := strconv.ParseBool(r.URL.Query().Get("devices"))
	isDeleted, _ := strconv.ParseBool(r.URL.Query().Get("deleted"))

	query := datastore.M{"$or": []datastore.M{{"_id": rawId}, {"external_id": userId}, {"email": userId}, {"linked_user_id": userId}}}
	if strings.Contains(userId, "::") {
		numbers := strings.Split(userId, "::")
		query = datastore.M{"$or": []datastore.M{{"_id": rawId}, {"external_id": userId}, {"email": userId}, {"isd_code": numbers[0], "phone_no": numbers[1]}, {"linked_user_id": userId}}}
	}

	user, err := a.UserStore.FindWithSoftDelete(r.Context(), isDeleted, query)
	if err != nil {
		if err.Error() == "mongo: no documents in result" {
			log.Logger.WithError(err).Error(constant.UserNotExist)
			err = errors.New(codes.UserNotFoundCode, constant.UserNotExist)
			service.WriteError(r, w, err)
			return
		}
		log.Logger.WithError(err).Error(constant.ServerIssue)
		err = errors.New(codes.InternalErrorCode, constant.ServerIssue)
		service.WriteError(r, w, err)
		return
	}

	user.Password = ""
	user.SecurityQuestions = nil

	service.WriteJSON(w, user)
}

// ResetPassword godoc
// @Summary reset password
// @Description This api will validate the auth_token and current password with database. Then, if it is success, this api will replace old password with new password to the system.
// @Tags user
// @Accept  json
// @Produce  json
// @Param user_id path string true "User ID"
// @Param payload body model.ResetPasswordRequestModel true "Request Payload"
// @Success 200
// @Failure 400 {object} model.ErrorResponse
// @Failure 404 {object} model.ErrorResponse
// @Failure 500 {object} model.ErrorResponse
// @Router /users/{user_id}/password/reset [post]
func (a *UserController) ResetPassword(w http.ResponseWriter, r *http.Request) {

	log := logging.GetLogger(r.Context())

	var resetRequest model.ResetPasswordRequestModel
	err := model.DecodeAndValidate(r, &resetRequest)
	if err != nil {
		log.Logger.WithError(err).Error("reset password fail")
		service.WriteError(r, w, err)
		return
	}

	userId := chi.URLParam(r, "user_id")

	result := service.ParseJWTString(r.Context(), resetRequest.AuthToken, *a.config)
	if result == nil || result["external_id"].(string) != userId || int64(result["expiry_time"].(float64)) <= time.Now().Unix() {
		err := errors.New(codes.InvalidJWTTokenCode, constant.InvalidJWTToken)
		log.Logger.WithError(err).Error("reset password api fail")
		service.WriteError(r, w, err)
		return
	}

	log.Logger.WithField(constant.Identifier, userId).Info("reset password with userId")

	storedUser, err := a.UserStore.FindWithSoftDelete(r.Context(), false, datastore.M{"external_id": userId})

	if !service.ValidatePassword(storedUser.Password, resetRequest.CurrentPassword) {
		err := errors.New(codes.OldPwdMismatchCode, constant.OldPwdMismatchErr)
		service.WriteError(r, w, err)
		return
	}

	storedUser.Password = service.HashAndSalt(resetRequest.NewPassword)
	storedUser.UpdatedAt = time.Now().Unix()
	a.UserStore.Update2(r.Context(), storedUser)

	log.Logger.WithField(constant.Identifier, userId).Info("reset password api success")

	service.WriteWithStatus(w, http.StatusOK)
}

// HandleGenericDataQuery godoc
// @Summary generic query to retreive entities from roaming-service
// @Description This api will return entity data which are stored in roaming-service.
// @Tags generic query
// @Accept  json
// @Produce  json
// @Param entity path string true "Entity Name"
// @Param payload body model.GenericQuery true "Request payload"
// @Success 200 {array}  model.UserModel
// @Failure 400 {object} model.ErrorResponse
// @Failure 404 {object} model.ErrorResponse
// @Failure 500 {object} model.ErrorResponse
// @Router /{entity}/data [post]
func (a *UserController) HandleGenericDataQuery(w http.ResponseWriter, r *http.Request) {

	log := logging.GetLogger(r.Context())

	entity := chi.URLParam(r, "entity") // this depicts which entity is being queried user, userlogin or guest

	var queryReq model.GenericQuery
	queryReq.Entity = entity
	err := model.DecodeAndValidate(r, &queryReq)
	if err != nil {
		log.Logger.WithError(err).Error("generic data query api fail")
		service.WriteError(r, w, err)
		return
	}

	log.Logger.WithField(constant.Identifier, entity).Info("generic data query with entity")

	log.Logger.WithField(constant.Payload, fmt.Sprintf("%+v\n", queryReq)).Info("generic data query api")

	queryReq.Entity = model.GetGenericEntity(queryReq.Entity)
	if queryReq.PageSize == 0 || queryReq.PageSize > 10 {
		queryReq.PageSize = 10
	}

	var andQ []map[string]interface{}
	for _, v := range queryReq.Query {
		o := model.GetMgOptr(v.O)
		if !model.IsValidOptValue(o, v.V) {
			err = errors.New(codes.InvalidOperatorCode, constant.InvalidOperator)
			service.WriteError(r, w, err)
			return
		}
		k := model.GetCollectionKeyForEntity(queryReq.Entity, v.K)
		q := bson.M{k: bson.M{o: v.V}}
		andQ = append(andQ, q)
	}

	db := datastore.GetMgoStore(*a.config)
	if db == nil {
		err := errors.New(codes.MgoDbConFailedCode, constant.MgoDbConFail)
		log.Logger.WithError(err).Error("generic data query api fail")
		service.WriteError(r, w, err)
		return
	}

	mongoPagingQuery := mongopaging.New(db, model.GetDbCollection(queryReq.Entity)).Find(bson.M{"$and": andQ}).Limit(queryReq.PageSize)

	if queryReq.OrderBy != "" {
		mongoPagingQuery = mongoPagingQuery.Sort(queryReq.OrderBy)
	}

	if queryReq.NextCur != "" {
		mongoPagingQuery = mongoPagingQuery.Cursor(queryReq.NextCur)
	}

	result := model.GetCollectionRespForEntity(queryReq.Entity)
	totalNoOfRecords, nextCursor, err := mongoPagingQuery.Result(result)
	if err != nil {
		log.Logger.WithError(err).Error("generic data query api fail")
		err = errors.New(codes.FailDbQueryCode, constant.FailDbQuery)
		service.WriteError(r, w, err)
		return
	}

	service.WriteJSONWithStatus(w, http.StatusOK, &model.GenericQueryResp{Result: result, TotalCount: totalNoOfRecords, NextCur: nextCursor})
}

// HardDeleteUser godoc
// @Summary hard delete user
// @Description delete user from database.
// @Tags user
// @Accept  json
// @Produce  json
// @Param payload body model.DeleteUserRequest true "Request Payload"
// @Success 202
// @Failure 400 {object} model.ErrorResponse
// @Failure 404 {object} model.ErrorResponse
// @Failure 500 {object} model.ErrorResponse
// @Router /users/hard-delete [delete]
func (c *UserController) HardDeleteUser(w http.ResponseWriter, r *http.Request) {

	log := logging.GetLogger(r.Context())

	var deleteUserRequest model.DeleteUserRequest
	err := model.DecodeAndValidate(r, &deleteUserRequest)
	if err != nil {
		service.WriteError(r, w, err)
		return
	}

	err = c.deleteUser(r.Context(), &deleteUserRequest)
	if err != nil {
		log.Logger.WithError(err).Error("delete user fail")
		service.WriteError(r, w, err)
		return
	}

	service.WriteWithStatus(w, http.StatusNoContent)
}

func (s *UserController) deleteUser(ctx context.Context, deleteUserRequest *model.DeleteUserRequest) error {
	log := logging.GetLogger(ctx)

	err := deleteUserRequest.Validate(ctx)
	if err != nil {
		return err
	}

	_, err = s.UserStore.FindWithSoftDelete(ctx, false, datastore.M{"external_id": deleteUserRequest.DeletedBy})
	if err != nil {
		if datastore.IsNotFoundError(err) {
			return errors.New(codes.UserNotExistAuthCode, constant.UserNotExist)
		}
		return err
	}

	query := datastore.M{"$or": []datastore.M{{"email": deleteUserRequest.Email}, {"phone_no": deleteUserRequest.PhoneNo}, {"isd_code": deleteUserRequest.ISDCode}}}
	user, err := s.UserStore.FindWithSoftDelete(ctx, false, query)
	if err != nil {
		if datastore.IsNotFoundError(err) {
			return errors.New(codes.UserNotExistCode, constant.UserNotExist)
		}
		return err
	}

	// delete user
	deleteUserQuery := datastore.M{"external_id": user.ExternalId}
	err = s.UserStore.Delete(ctx, deleteUserQuery)
	if err != nil {
		return errors.New(codes.FailedToDeleteUserCode, constant.FailedToDeleteUser)
	}

	if err != nil {
		log.Logger.WithError(err).Error("failed to insert delete user audit log")
	}

	return nil
}

