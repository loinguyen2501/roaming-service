package controller

import (
	"context"
	"net/http"

	"bitbucket.org/libertywireless/roaming-service/config"
	"bitbucket.org/libertywireless/roaming-service/constant"
	"bitbucket.org/libertywireless/roaming-service/datastore"
	"bitbucket.org/libertywireless/roaming-service/logging"
	"bitbucket.org/libertywireless/roaming-service/router"
	"bitbucket.org/libertywireless/roaming-service/service"
	"github.com/go-chi/chi"
	"github.com/newrelic/go-agent"
	"github.com/swaggo/http-swagger"
)

var newrelicApp newrelic.Application

func Handler(config *config.GeneralConfig) *chi.Mux {
	r := router.GetRouter(logging.Logger)

	//  DAO
	var store = datastore.NewDatastore(*config)
	datastore.InitStore(*config)

	// Stores
	var userStore = datastore.NewUserMongoStore(store) 

	// Controllers
	var userController = &UserController{userStore, config}

	nrConfig := newrelic.NewConfig(config.NewRelic["app_name"].(string), config.NewRelic["license_key"].(string))
	var err error
	newrelicApp, err = newrelic.NewApplication(nrConfig)
	log := logging.GetLogger(context.Background())
	if err == nil {
		log.Logger.WithField(constant.Identifier, nrConfig.AppName).Info(constant.InitNewRelic)
	} else {
		log.Logger.WithError(err).Error(constant.InitNewRelicFail)
	}
	service.Init(newrelicApp)

	// Routes

	//user APIS
	r.Post(GetNewRelicHandler("/users/register", userController.CreateUser))
	r.Post(GetNewRelicHandler("/users/update", userController.UpdateUser))
	r.Get(GetNewRelicHandler("/users/{user_id}/profile", userController.GetUser))
	r.Post(GetNewRelicHandler("/users/{user_id}/password/reset", userController.ResetPassword))

	r.With(removeContextTypeJSON).Get("/swagger/*", httpSwagger.WrapHandler)

	return r
}

func GetNewRelicHandler(pattern string, handler func(http.ResponseWriter, *http.Request)) (string, func(http.ResponseWriter, *http.Request)) {
	if newrelicApp != nil {
		return newrelic.WrapHandleFunc(newrelicApp, pattern, handler)
	} 
	
	return pattern, handler
}

func removeContextTypeJSON(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		w.Header().Del("Content-Type")
		next.ServeHTTP(w, r)
	}
	return http.HandlerFunc(fn)
}
