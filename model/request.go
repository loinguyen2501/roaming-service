package model

import (
	"context"
	"strings"

	"bitbucket.org/libertywireless/roaming-service/logging"

	"bitbucket.org/libertywireless/roaming-service/constant"
	"bitbucket.org/libertywireless/roaming-service/constant/codes"
	"bitbucket.org/libertywireless/roaming-service/internal/errors"
	"gopkg.in/validator.v2"
)

type GuestRegisterRequestModel struct {
	GuestId       string     `json:"guest_id,omitempty" validate:"regexp=^[A-Z]+\\-\\S+$"`
	ExternalId    string     `json:"external_id,omitempty" validate:"regexp=^[A-Z]+\\-\\S+$"`
	FirstName     string     `json:"first_name,omitempty"`
	LastName      string     `json:"last_name,omitempty"`
	ImageUrl      string     `json:"image_url,omitempty"`
	Dob           string     `json:"dob,omitempty" validate:"regexp=(^$|^([0-9][0-9]{3})\\-(0[1-9]|1[0-2])\\-(0[1-9]|[12][0-9]|3[01])$)"`
	Email         string     `json:"email,omitempty" validate:"regexp=(^$|^([A-Za-z0-9_\\-.+])+@([A-Za-z0-9_\\-.])+\\.([A-Za-z]{2\\,})$)"`
	PhoneNo       string     `json:"phone_no,omitempty" validate:"regexp=(^$|^\\d+$)"`
	ISDCode       string     `json:"isd_code,omitempty" validate:"regexp=(^$|^\\d+$)"`
	Source        string     `json:"source"`
	AddressLine1  string     `json:"address_line_1,omitempty"`
	AddressLine2  string     `json:"address_line_2,omitempty"`
	AddressLine3  string     `json:"address_line_3,omitempty"`
	City          string     `json:"city,omitempty"`
	State         string     `json:"state,omitempty"`
	Country       string     `json:"country,omitempty"`
	PostalCode    string     `json:"postal_code,omitempty"`
	Device        UserDevice `json:"device"`
	AuthRequired  bool       `json:"auth_required"`
	LoginDuration int        `json:"login_duration"`
}

func (a *GuestRegisterRequestModel) Validate(ctx context.Context) error {
	log := logging.GetLogger(ctx)
	if a.Email == "" && (a.PhoneNo == "" || a.ISDCode == "") {
		return errors.New(codes.ReqEmailOrMobileCode, constant.ReqEmailOrMobile)
	}

	if a.Source != "" && !a.IsValidSource() {
		err := errors.New(codes.InvalidSourceCode, constant.InvalidSource)
		log.Logger.WithError(err).Error("Create user api fail")
		return err
	}

	if a.AuthRequired && !a.Device.Validate() {
		return errors.New(codes.DeviceInvalidFieldsCode, constant.DeviceInvalidFieldsErr)
	}

	log.Logger.WithField(constant.Identifier, a.Email).Info("email in create user api")
	log.Logger.WithField(constant.Identifier, a.PhoneNo).Info("phone in create user api")

	err := validator.Validate(a)
	if err != nil {
		errs, ok := err.(validator.ErrorMap)
		if ok {
			if _, ok := errs["Password"]; ok {
				return errors.New(codes.InvalidPwdLengthCode, constant.InvalidPwdLengthErr)
			}

			for f := range errs {
				return errors.New(codes.DeviceInvalidFieldsCode, constant.ValidateFieldErr+"-"+f)
			}
		} else {
			return errors.New(codes.ValidationUnknownCode, constant.ValidationUnknownErr)
		}
	}

	return nil
}

func (a *GuestRegisterRequestModel) GetLoginSource() string {
	return loginSources[strings.ToLower(a.Source)]
}

func (a *GuestRegisterRequestModel) IsValidSource() bool {
	if _, ok := loginSources[strings.ToLower(a.Source)]; !ok {
		return false
	}
	return true
}

type UserRegisterRequestModel struct {
	ExternalId        string             `json:"external_id,omitempty" validate:"regexp=^[A-Z]+\\-\\S+$"`
	FirstName         string             `json:"first_name,omitempty"`
	LastName          string             `json:"last_name,omitempty"`
	Dob               string             `json:"dob,omitempty" validate:"regexp=(^$|^([0-9][0-9]{3})\\-(0[1-9]|1[0-2])\\-(0[1-9]|[12][0-9]|3[01])$)"`
	ImageUrl          string             `json:"image_url,omitempty"`
	Email             string             `json:"email,omitempty" validate:"regexp=(^$|^([A-Za-z0-9_\\-.+])+@([A-Za-z0-9_\\-.])+\\.([A-Za-z]{2\\,})$)"`
	Password          string             `json:"password" validate:"min=8"`
	PhoneNo           string             `json:"phone_no,omitempty" validate:"regexp=(^$|^\\d+$)"`
	ISDCode           string             `json:"isd_code,omitempty" validate:"regexp=(^$|^\\d+$)"`
	Source            string             `json:"source"`
	AddressLine1      string             `json:"address_line_1,omitempty"`
	AddressLine2      string             `json:"address_line_2,omitempty"`
	AddressLine3      string             `json:"address_line_3,omitempty"`
	City              string             `json:"city,omitempty"`
	State             string             `json:"state,omitempty"`
	Country           string             `json:"country,omitempty"`
	PostalCode        string             `json:"postal_code,omitempty"`
	SecurityQuestions []SecurityQuestion `json:"security_questions,omitempty"`
	Device            UserDevice         `json:"device"`
	AuthRequired      bool               `json:"auth_required"`
	LoginDuration     int                `json:"login_duration"`
}

func (a *UserRegisterRequestModel) Validate(ctx context.Context) error {
	log := logging.GetLogger(ctx)
	if a.Email == "" && (a.PhoneNo == "" || a.ISDCode == "") {
		return errors.New(codes.ReqEmailOrMobileCode, constant.ReqEmailOrMobile)
	}

	if a.Source != "" && !a.IsValidSource() {
		err := errors.New(codes.InvalidSourceCode, constant.InvalidSource)
		log.Logger.WithError(err).Error("Create user api fail")
		return err
	}

	if a.AuthRequired && !a.Device.Validate() {
		return errors.New(codes.DeviceInvalidFieldsCode, constant.DeviceInvalidFieldsErr)
	}

	log.Logger.WithField(constant.Identifier, a.Email).Info("email in create user api")
	log.Logger.WithField(constant.Identifier, a.PhoneNo).Info("phone in create user api")

	err := validator.Validate(a)
	if err != nil {
		errs, ok := err.(validator.ErrorMap)
		if ok {
			if _, ok := errs["Password"]; ok {
				return errors.New(codes.InvalidPwdLengthCode, constant.InvalidPwdLengthErr)
			}

			for f := range errs {
				return errors.New(codes.DeviceInvalidFieldsCode, constant.ValidateFieldErr+"-"+f)
			}
		} else {
			return errors.New(codes.ValidationUnknownCode, constant.ValidationUnknownErr)
		}
	}

	return nil
}

func (a *UserRegisterRequestModel) IsValidSource() bool {
	if _, ok := loginSources[strings.ToLower(a.Source)]; !ok {
		return false
	}
	return true
}

func (a *UserRegisterRequestModel) GetLoginSource() string {
	return loginSources[strings.ToLower(a.Source)]
}

type UserOTPRegisterRequestModel struct {
	UserRegisterRequestModel
	Override bool   `json:"override"`
	AuthId   string `json:"auth_id" validate:"nonzero"`
	OtpCode  string `json:"otp_code" validate:"nonzero"`
}

func (a *UserOTPRegisterRequestModel) Validate(ctx context.Context) error {

	if a.PhoneNo == "" || a.ISDCode == "" {
		return errors.New(codes.ReqMobileNIsdCode, constant.ReqMobileNIsd)
	}

	if !a.Device.Validate() {
		return errors.New(codes.DeviceInvalidFieldsCode, constant.DeviceInvalidFieldsErr)
	}

	return ValidateFields(a)
}

type UserLoginRequestModel struct {
	Email         string     `json:"email,omitempty"`
	UserId        string     `json:"user_id,omitempty"`
	Password      string     `json:"password" validate:"nonzero"`
	PhoneNo       string     `json:"phone_no,omitempty"`
	ISDCode       string     `json:"isd_code,omitempty"`
	Device        UserDevice `json:"device,omitempty"`
	LoginMode     string     `json:"login_mode,omitempty"`
	LoginDuration int        `json:"login_duration,omitempty"` // duration in minutes
}

func (a *UserLoginRequestModel) Validate(ctx context.Context) error {

	if a.Email == "" && (a.ISDCode == "" || a.PhoneNo == "") && a.UserId == "" {
		return errors.New(codes.LoginBadReqCode, constant.LoginBadReq)
	}

	if !a.Device.Validate() {
		return errors.New(codes.DeviceInvalidFieldsCode, constant.DeviceInvalidFieldsErr)
	}

	return ValidateFields(a)
}

type UserUpdateRequestModel struct {
	ExternalId        string             `json:"external_id,omitempty" bson:"external_id" validate:"nonzero"`
	FirstName         string             `json:"first_name,omitempty" bson:"first_name,omitempty"`
	LastName          string             `json:"last_name,omitempty" bson:"last_name,omitempty"`
	Dob               string             `json:"dob,omitempty" bson:"dob,omitempty"`
	ImageUrl          string             `json:"image_url,omitempty" bson:"image_url,omitempty"`
	Email             string             `json:"email,omitempty," validate:"regexp=(^$|^([A-Za-z0-9_\\-.+])+@([A-Za-z0-9_\\-.])+\\.([A-Za-z]{2\\,})$)" bson:"email,omitempty"`
	PhoneNo           string             `json:"phone_no,omitempty" validate:"regexp=(^$|^\\d+$)" bson:"phone_no,omitempty"`
	ISDCode           string             `json:"isd_code,omitempty" validate:"regexp=(^$|^\\d+$)" bson:"isd_code,omitempty"`
	EmailConfirmed    bool               `bson:"email_confirmed"`
	PhoneConfirmed    bool               `bson:"phone_confirmed"`
	AddressLine1      string             `json:"address_line_1,omitempty" bson:"address_line_1,omitempty"`
	AddressLine2      string             `json:"address_line_2,omitempty" bson:"address_line_2,omitempty"`
	AddressLine3      string             `json:"address_line_3,omitempty" bson:"address_line_3,omitempty"`
	City              string             `json:"city,omitempty" bson:"city,omitempty"`
	State             string             `json:"state,omitempty" bson:"state,omitempty"`
	Country           string             `json:"country,omitempty" bson:"country,omitempty"`
	SecurityQuestions []SecurityQuestion `json:"security_questions,omitempty" bson:"security_questions,omitempty"`
	PostalCode        string             `json:"postal_code,omitempty" bson:"postal_code,omitempty"`
	UpdatedAt         int64              `json:"updated_at,omitempty" bson:"updated_at"`
}

func (a *UserUpdateRequestModel) Validate(ctx context.Context) error {
	return ValidateFields(a)
}

type VerifyChannelRequestModel struct {
	AuthId  string     `json:"auth_id" validate:"nonzero"`
	OtpCode string     `json:"otp_code" validate:"nonzero"`
	Device  UserDevice `json:"device"`
}

func (a *VerifyChannelRequestModel) Validate(ctx context.Context) error {
	return ValidateFields(a)
}

type ChangeChannelRequestModel struct {
	AuthId     string     `json:"auth_id" validate:"nonzero"`
	OtpCode    string     `json:"otp_code" validate:"nonzero"`
	OldEmail   string     `json:"old_email"`
	NewEmail   string     `json:"new_email"`
	OldPhoneNo string     `json:"old_phone_no"`
	NewPhoneNo string     `json:"new_phone_no"`
	OldIsdCode string     `json:"old_isd_code"`
	NewIsdCode string     `json:"new_isd_code"`
	Override   bool       `json:"override"`
	Device     UserDevice `json:"device"`
}

func (a *ChangeChannelRequestModel) Validate(ctx context.Context) error {
	return ValidateFields(a)
}

type UnlinkRequestModel struct {
	PhoneNo string `json:"phone_no" validate:"regexp=^\\d+$"`
	IsdCode string `json:"isd_code" validate:"regexp=^\\d+$"`
	UserId  string `json:"user_id" validate:"regexp=^[A-Z]+\\-\\S+$"`
}

func (r *UnlinkRequestModel) Validate(ctx context.Context) error {
	return ValidateFields(r)
}

type MigrateUserReq struct {
	UserRegisterRequestModel
	EmailConfirmed bool `json:"email_confirmed"`
	PhoneConfirmed bool `json:"phone_confirmed"`
}

type ResetPasswordRequestModel struct {
	AuthToken       string `json:"auth_token" validate:"nonzero"`
	CurrentPassword string `json:"current_password" validate:"min=8"`
	NewPassword     string `json:"new_password" validate:"min=8"`
}

func (a *ResetPasswordRequestModel) Validate(ctx context.Context) error {
	if a.CurrentPassword == a.NewPassword {
		return errors.New(codes.SamePwdCode, constant.SamePwdErr)
	}
	return ValidateFields(a)
}

type ForgotPasswordRequestModel struct {
	UserId      string `json:"user_id"`
	Email       string `json:"email"`
	IsdCode     string `json:"isd_code"`
	PhoneNo     string `json:"phone_no"`
	NewPassword string `json:"new_password" validate:"min=8"`
	AuthId      string `json:"auth_id" validate:"nonzero"`
	OtpCode     string `json:"otp_code" validate:"nonzero"`
}

func (a *ForgotPasswordRequestModel) Validate(ctx context.Context) error {
	return ValidateFields(a)
}

type RefreshTokenRequestModel struct {
	AuthToken         string `json:"auth_token" validate:"nonzero"`
	DeviceId          string `json:"device_id" validate:"nonzero"`
	LoginMode         string `json:"login_mode"`
	ExtensionDuration int    `json:"extension_duration" validate:"min=1"`
}

func (a *RefreshTokenRequestModel) Validate(ctx context.Context) error {
	return ValidateFields(a)
}

type InvalidateTokenRequestModel struct {
	AuthToken string `json:"auth_token" validate:"nonzero"`
	DeviceId  string `json:"device_id" validate:"nonzero"`
}

func (a *InvalidateTokenRequestModel) Validate(ctx context.Context) error {
	return ValidateFields(a)
}

type ValidateTokenRequest struct {
	AuthToken string `json:"auth_token" validate:"nonzero"`
	DeviceID  string `json:"device_id" validate:"nonzero"`
}

func (a *ValidateTokenRequest) Validate(ctx context.Context) error {
	return ValidateFields(a)
}

type GuestCreateRequestModel struct {
	DeviceId      string `json:"device_id"`
	DeviceType    string `json:"device_type"`
	LoginDuration int    `json:"login_duration"`
}

func (a *GuestCreateRequestModel) Validate(ctx context.Context) error {
	device := &UserDevice{DeviceId: a.DeviceId, DeviceType: a.DeviceType}
	if !device.Validate() {
		return errors.New(codes.DeviceInvalidFieldsCode, constant.DeviceInvalidFieldsErr)
	}
	return nil
}

type DeleteUserRequest struct {
	Email     string `json:"email,omitempty"  validate:"regexp=(^$|^([A-Za-z0-9_\\-.+])+@([A-Za-z0-9_\\-.])+\\.([A-Za-z]{2\\,})$)"`
	PhoneNo   string `json:"phone_no,omitempty"`
	ISDCode   string `json:"isd_code,omitempty"`
	DeletedBy string `json:"deleted_by" validate:"nonzero"`
	Comment   string `json:"comment" validate:"nonzero"`
}

func (a *DeleteUserRequest) Validate(ctx context.Context) error {
	if a.Email == "" && (a.PhoneNo == "" || a.ISDCode == "") {
		return errors.New(codes.ReqEmailOrMobileCode, constant.ReqEmailOrMobile)
	}

	return ValidateFields(a)
}

type UserMergeRequestModel struct {
	AuthID  string `json:"auth_id" validate:"nonzero"`
	Email   string `json:"email,omitempty" validate:"regexp=(^$|^([A-Za-z0-9_\\-.+])+@([A-Za-z0-9_\\-.])+\\.([A-Za-z]{2\\,})$)"`
	UserID  string `json:"user_id" validate:"regexp=^[A-Z]+\\-\\S+$"`
	OtpCode string `json:"otp_code" validate:"nonzero"`
}

func (a *UserMergeRequestModel) Validate(ctx context.Context) error {
	return ValidateFields(a)
}
