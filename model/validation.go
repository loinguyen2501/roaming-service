package model

import (
	"context"
	"encoding/json"
	"net/http"

	"bitbucket.org/libertywireless/roaming-service/constant"
	"bitbucket.org/libertywireless/roaming-service/constant/codes"
	"bitbucket.org/libertywireless/roaming-service/internal/errors"
	"gopkg.in/validator.v2"
)

type RequestValidator interface {
	Validate(ctx context.Context) error
}

func DecodeAndValidate(r *http.Request, v RequestValidator) error {
	if err := json.NewDecoder(r.Body).Decode(v); err != nil {
		return errors.New(codes.FailedToDecodeRequestBodyCode, constant.DecodeRequestBodyErr)
	}
	defer r.Body.Close()
	return v.Validate(r.Context())
}

// validateFields checks if the required fields in a model is filled.

func ValidateFields(model interface{}) error {
	err := validator.Validate(model)
	if err != nil {
		errs, ok := err.(validator.ErrorMap)
		if ok {
			for f, _ := range errs {
				return errors.New(codes.ValidateFieldCode, constant.ValidateFieldErr+"-"+f)
			}
		} else {
			return errors.New(codes.ValidationUnknownCode, constant.ValidationUnknownErr)
		}
	}

	return nil
}
