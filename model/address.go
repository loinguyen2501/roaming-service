package model

import (
	"context"

	"bitbucket.org/libertywireless/roaming-service/constant"
	"bitbucket.org/libertywireless/roaming-service/constant/codes"
	"bitbucket.org/libertywireless/roaming-service/internal/errors"
	"github.com/mongodb/mongo-go-driver/bson/objectid"
)

type UserAddress struct {
	RawId             *objectid.ObjectID `json:"raw_id,omitempty" bson:"_id,omitempty"`
	Id                string             `json:"id,omitempty" bson:"id,omitempty"`
	UserId            string             `json:"user_id" bson:"user_id"`
	AddressExternalId string             `json:"address_external_id" bson:"address_external_id"`
	AddressLine1      string             `json:"address_line_1,omitempty" bson:"address_line_1,omitempty"`
	AddressLine2      string             `json:"address_line_2,omitempty" bson:"address_line_2,omitempty"`
	AddressLine3      string             `json:"address_line_3,omitempty" bson:"address_line_3,omitempty"`
	City              string             `json:"city" bson:"city"`
	State             string             `json:"state" bson:"state"`
	Country           string             `json:"country" bson:"country"`
	PostalCode        string             `json:"postal_code" bson:"postal_code"`
	CreatedAt         int64              `json:"created_at" bson:"created_at"`
	UpdatedAt         int64              `json:"updated_at" bson:"updated_at"`
}

type AddressRegisterRequestModel struct {
	UserId            string `json:"user_id" validate:"nonzero"`
	AddressExternalId string `json:"address_external_id" validate:"nonzero"`
	AddressLine1      string `json:"address_line_1,omitempty"`
	AddressLine2      string `json:"address_line_2,omitempty"`
	AddressLine3      string `json:"address_line_3,omitempty"`
	City              string `json:"city" validate:"nonzero"`
	State             string `json:"state" validate:"nonzero"`
	Country           string `json:"country" validate:"nonzero"`
	PostalCode        string `json:"postal_code" validate:"nonzero"`
}

func (a *AddressRegisterRequestModel) Validate(ctx context.Context) error {
	if a.AddressLine1 == "" && a.AddressLine2 == "" && a.AddressLine3 == "" {
		return errors.New(codes.AddressEmptyCode, constant.AddressEmpty)
	}
	return ValidateFields(a)
}

type AddressUpdateRequestModel struct {
	UserId            string `json:"user_id" bson:"user_id" validate:"nonzero"`
	AddressExternalId string `json:"address_external_id" bson:"address_external_id" validate:"nonzero"`
	AddressLine1      string `json:"address_line_1,omitempty" bson:"address_line_1,omitempty"`
	AddressLine2      string `json:"address_line_2,omitempty" bson:"address_line_2,omitempty"`
	AddressLine3      string `json:"address_line_3,omitempty" bson:"address_line_3,omitempty"`
	City              string `json:"city,omitempty" bson:"city,omitempty"`
	State             string `json:"state,omitempty" bson:"state,omitempty"`
	Country           string `json:"country,omitempty" bson:"country,omitempty"`
	PostalCode        string `json:"postal_code,omitempty" bson:"postal_code,omitempty"`
	UpdatedAt         int64  `json:"updated_at" bson:"updated_at" validate:"nonzero"`
}

func (a *AddressUpdateRequestModel) Validate(ctx context.Context) error {
	return ValidateFields(a)
}
