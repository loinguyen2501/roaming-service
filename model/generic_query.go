package model

import (
	"context"
	"reflect"
	"strings"

	"bitbucket.org/libertywireless/roaming-service/constant"
	"bitbucket.org/libertywireless/roaming-service/constant/codes"
	"bitbucket.org/libertywireless/roaming-service/internal/errors"
)

var genericEntity = map[string]string{
	"user": "USER",
	//"device":     "DEVICE",
	"user_login": "USER_LOGIN",
	"guest_user": "GUEST_USER",
}

var collectionMap = map[string]string{
	"user":       "users",
	"device":     "user_devices",
	"user_login": "user_logins",
	"guest_user": "guest_users",
}

var userModelKeys = map[string]string{
	"id":         "external_id",
	"email":      "email",
	"source":     "source",
	"created_at": "created_at",
	"updated_at": "updated_at",
}

var userLoginKeys = map[string]string{
	"id":         "user_id",
	"created_at": "created_at",
	"updated_at": "updated_at",
}

var guestModelKeys = map[string]string{
	"id":         "external_id",
	"device_id":  "device_id",
	"created_at": "created_at",
	"updated_at": "updated_at",
}

var mongoOptMap = map[string]string{
	"eq":  "$eq",
	"neq": "$ne",
	">":   "$gt",
	">=":  "$gte",
	"<":   "$lt",
	"<=":  "$lte",
	"in":  "$in",
}

type GenericQuery struct {
	Entity   string  `json:"entity" validate:"nonzero"`
	Query    []Query `json:"query" validate:"nonzero"`
	NextCur  string  `json:"next_cur"`
	PageSize int     `json:"page_size"`
	OrderBy  string  `json:"order_by"`
}

type Query struct {
	K string      `json:"k"`
	V interface{} `json:"v"`
	O string      `json:"o"`
}

type GenericQueryResp struct {
	Result     interface{} `json:"result"`
	NextCur    string      `json:"next_cur"`
	TotalCount int         `json:"total_count,omitempty"`
}

type UserModelResp struct {
	ExternalId     string `json:"id" bson:"external_id"`
	FirstName      string `json:"first_name" bson:"first_name"`
	LastName       string `json:"last_name" bson:"last_name"`
	Dob            string `json:"dob" bson:"dob"`
	ImageUrl       string `json:"image_url" bson:"image_url"`
	Email          string `json:"email" bson:"email"`
	PhoneNo        string `json:"phone_no" bson:"phone_no"`
	ISDCode        string `json:"isd_code" bson:"isd_code"`
	AddressLine1   string `json:"address_line_1" bson:"address_line_1"`
	AddressLine2   string `json:"address_line_2" bson:"address_line_2"`
	AddressLine3   string `json:"address_line_3" bson:"address_line_3"`
	EmailConfirmed bool   `json:"email_confirmed" bson:"email_confirmed"`
	PhoneConfirmed bool   `json:"phone_confirmed" bson:"phone_confirmed"`
	TelcoUser      bool   `json:"telco_user" bson:"telco_user"`
	Source         string `json:"source" bson:"source"`
	City           string `json:"city" bson:"city"`
	State          string `json:"state" bson:"state"`
	Country        string `json:"country" bson:"country"`
	PostalCode     string `json:"postal_code" bson:"postal_code"`
	CreatedAt      int64  `json:"created_at" bson:"created_at"`
}

type UserLoginResp struct {
	UserId        string `json:"id" bson:"user_id"`
	LoginMode     string `json:"login_mode" bson:"login_mode"`
	DeviceId      string `json:"device_id" bson:"device_id"`
	LoginProvider string `json:"login_provider,omitempty" bson:"login_provider"`
	CreatedAt     int64  `json:"created_at,omitempty" bson:"created_at"`
}

type GuestModelResp struct {
	ExternalID string `json:"external_id,omitempty" bson:"external_id"`
	DeviceID   string `json:"device_id,omitempty" bson:"device_id"`
	DeviceType string `json:"device_type,omitempty" bson:"device_type"`
	CreatedAt  int64  `json:"created_at,omitempty" bson:"created_at"`
	UpdatedAt  int64  `json:"updated_at,omitempty" bson:"updated_at"`
}

func (a *GenericQuery) IsValidEntity() bool {
	if _, ok := genericEntity[strings.ToLower(a.Entity)]; !ok {
		return false
	}
	return true
}

func (a *GenericQuery) Validate(ctx context.Context) error {
	if !a.IsValidEntity() {
		return errors.New(codes.InvalidEntityCode, constant.InvalidEntity)
	}
	return nil
}

func GetCollectionKeyForEntity(entity string, key string) string {
	switch entity {
	case "USER_LOGIN":
		return getUserLoginKey(key)
	case "GUEST_USER":
		return getGuestModelKey(key)
	}
	return getUserModelKey(key)
}

func GetCollectionRespForEntity(entity string) interface{} {
	responseMap := make(map[string]interface{})

	var users []*UserModelResp
	var userLogins []*UserLoginResp
	var guests []*GuestModelResp

	responseMap["USER"] = &users
	responseMap["USER_LOGIN"] = &userLogins
	responseMap["GUEST_USER"] = &guests
	return responseMap[entity]
}

func IsValidOptValue(op string, val interface{}) bool {
	if op == "$in" {
		return reflect.ValueOf(val).Kind() == reflect.Slice
	}
	return true
}

func getUserModelKey(key string) string {
	return userModelKeys[strings.ToLower(key)]
}

func GetMgOptr(key string) string {
	return mongoOptMap[strings.ToLower(key)]
}

func GetDbCollection(key string) string {
	return collectionMap[strings.ToLower(key)]
}

func getUserLoginKey(key string) string {
	return userLoginKeys[strings.ToLower(key)]
}

func getGuestModelKey(key string) string {
	return guestModelKeys[strings.ToLower(key)]
}

func GetGenericEntity(entity string) string {
	return genericEntity[strings.ToLower(entity)]
}
