package model

import (
	"encoding/json"
	"fmt"

	"bitbucket.org/libertywireless/roaming-service/constant"
	"bitbucket.org/libertywireless/roaming-service/constant/codes"
	"bitbucket.org/libertywireless/roaming-service/internal/errors"
	"github.com/mongodb/mongo-go-driver/bson/objectid"
)

const (
	Register = "Register"
	SignIn   = "SignIn"
)

var loginSources = map[string]string{
	"public": "PUBLIC",
	"invite": "INVITE",
	"google": "GOOGLE",
	"fb":     "FACEBOOK",
}

type UserModel struct {
	RawId             *objectid.ObjectID     `json:"raw_id,omitempty" bson:"_id,omitempty"`
	Id                string                 `json:"id,omitempty" bson:"id,omitempty"`
	ExternalId        string                 `json:"external_id,omitempty" bson:"external_id"`
	FirstName         string                 `json:"first_name,omitempty" bson:"first_name,omitempty"`
	LastName          string                 `json:"last_name,omitempty" bson:"last_name,omitempty"`
	Dob               string                 `json:"dob,omitempty" bson:"dob,omitempty"`
	ImageUrl          string                 `json:"image_url,omitempty" bson:"image_url,omitempty"`
	Email             string                 `json:"email" bson:"email,omitempty"`
	Password          string                 `json:"password" bson:"password"`
	PhoneNo           string                 `json:"phone_no,omitempty" bson:"phone_no,omitempty"`
	ISDCode           string                 `json:"isd_code,omitempty" bson:"isd_code,omitempty"`
	AddressLine1      string                 `json:"address_line_1,omitempty" bson:"address_line_1,omitempty"`
	AddressLine2      string                 `json:"address_line_2,omitempty" bson:"address_line_2,omitempty"`
	AddressLine3      string                 `json:"address_line_3,omitempty" bson:"address_line_3,omitempty"`
	EmailConfirmed    bool                   `json:"email_confirmed" bson:"email_confirmed"`
	PhoneConfirmed    bool                   `json:"phone_confirmed" bson:"phone_confirmed"`
	TelcoUser         bool                   `json:"telco_user" bson:"telco_user"`
	Source            string                 `json:"source,omitempty" bson:"source,omitempty"`
	LinkedUserID      string                 `json:"linked_user_id,omitempty" bson:"linked_user_id,omitempty"`
	CirclesAccounts   map[string]interface{} `json:"circles_accounts,omitempty" bson:"circles_accounts"`
	City              string                 `json:"city,omitempty" bson:"city,omitempty"`
	State             string                 `json:"state,omitempty" bson:"state,omitempty"`
	Country           string                 `json:"country,omitempty" bson:"country,omitempty"`
	PostalCode        string                 `json:"postal_code,omitempty" bson:"postal_code,omitempty"`
	SecurityQuestions []SecurityQuestion     `json:"security_questions,omitempty" bson:"security_questions,omitempty"`
	CreatedAt         int64                  `json:"created_at,omitempty" bson:"created_at"`
	UpdatedAt         int64                  `json:"updated_at,omitempty" bson:"updated_at"`
	DeletedAt         int64                  `json:"deleted_at,omitempty" bson:"deleted_at,omitempty"`
}

func (u *UserModel) UpdateUserModel(userRequest *MigrateUserReq) {
	u.PhoneNo = userRequest.PhoneNo
	u.Email = userRequest.Email
	u.ISDCode = userRequest.ISDCode
	u.EmailConfirmed = userRequest.EmailConfirmed
	u.PhoneConfirmed = userRequest.PhoneConfirmed
	u.Password = userRequest.Password
	u.ISDCode = userRequest.ISDCode
	u.Source = userRequest.Source
	u.AddressLine1 = userRequest.AddressLine1
	u.AddressLine2 = userRequest.AddressLine2
	u.AddressLine3 = userRequest.AddressLine3
	u.City = userRequest.City
	u.Country = userRequest.Country
	u.Dob = userRequest.Dob
	u.FirstName = userRequest.FirstName
	u.LastName = userRequest.LastName
	u.ImageUrl = userRequest.ImageUrl
	u.State = userRequest.State
	u.PostalCode = userRequest.PostalCode
	u.SecurityQuestions = userRequest.SecurityQuestions
}

func (a *UserModel) ValidatePassword(pwd string) bool {
	return a.Password == pwd
}

func (a *UserModel) isMergeable(otherUser *UserModel) bool {
	if a.ExternalId == otherUser.ExternalId {
		return false
	}

	// Conditions on primary account
	// - Must not have account already linked to it
	// - Must not have an email

	// Conditions on secondary account
	// - Must not have account already linked to it
	// - Must not have any phone number

	isValidPrimary := a.LinkedUserID == "" && a.Email == ""
	isValidSecondary := otherUser.LinkedUserID == "" && otherUser.ISDCode == "" && otherUser.PhoneNo == ""

	return isValidPrimary && isValidSecondary
}

func (a *UserModel) Merge(otherUser *UserModel) error {
	if !a.isMergeable(otherUser) {
		return errors.New(codes.UsersCannotBeMergedCode, constant.UsersCannotBeMerged)
	}

	a.Email = otherUser.Email
	a.EmailConfirmed = otherUser.EmailConfirmed
	a.Password = otherUser.Password
	a.LinkedUserID = otherUser.ExternalId

	mergedUser, err := copyFieldIfNotEmpty(*a, *otherUser)
	*a = *mergedUser

	return err
}

func copyFieldIfNotEmpty(src, dst UserModel) (*UserModel, error) {
	by, err := json.Marshal(&src)
	if err != nil {
		return nil, err
	}

	json.Unmarshal(by, &dst)
	if err != nil {
		return nil, err
	}

	return &dst, nil
}

type UserLogin struct {
	Id               string `json:"id,omitempty" bson:"_id,omitempty"`
	UserId           string `json:"user_id" bson:"user_id" validate:"nonzero"`
	LoginMode        string `json:"login_mode" bson:"login_mode"`
	DeviceId         string `json:"device_id" bson:"device_id"`
	DeviceType       string `json:"device_type" bson:"device_type"`
	AuthToken        string `json:"auth_token" bson:"auth_token"`
	ValidationString string `json:"validation_string" bson:"validation_string"` // LOGIN_TIME.EXPIRY_TIME
	LoginProvider    string `json:"login_provider,omitempty" bson:"login_provider" validate:"nonzero"`
	CreatedAt        int64  `json:"created_at,omitempty" bson:"created_at" validate:"nonzero"`
	UpdatedAt        int64  `json:"updated_at,omitempty" bson:"updated_at" validate:"nonzero"`
}

func (l *UserLogin) ValidateClaims(authToken string, claims map[string]interface{}) bool {
	if l.AuthToken == "" {
		return l.ValidationString == getTokenValidationString(claims)
	}
	return l.AuthToken == authToken
}

func getTokenValidationString(claims map[string]interface{}) string {
	loginTime := claims["login_time"]
	expiryTime := claims["expiry_time"]

	if v, ok := loginTime.(float64); ok {
		loginTime = int64(v)
	}

	if v, ok := expiryTime.(float64); ok {
		expiryTime = int64(v)
	}

	return fmt.Sprintf("%.v.%.v", loginTime, expiryTime)
}

type SecurityQuestion struct {
	Question string `json:"question" bson:"question"`
	Answer   string `json:"answer" bson:"answer"`
}

type UserLoginResponseModel struct {
	AuthToken string `json:"auth_token"`
	Type      string `json:"type,omitempty"`
}

type MigrateUserModel struct {
	User MigrateUserReq `json:"user"`
	// UserDevices   []UserDevice   `json:"user_devices"`
	TelcoInfo     []TelcoInfo `json:"telco_info"`
	LoginDuration int         `json:"login_duration"`
}

type AuditLogType string

type UserAuditModel struct {
	Id            string         `json:"id,omitempty" bson:"id,omitempty"`
	UserId        string         `json:"user_id" bson:"user_id"`
	AuditType     AuditLogType   `json:"audit_type" bson:"audit_type"`
	AuditLog      *UserAuditLog  `json:"audit_log,omitempty" bson:"audit_log,omitempty"`
	GuestLog      *GuestAuditLog `json:"guest_log,omitempty" bson:"guest_log,omitempty"`
	DeleteUserLog *DeleteUserLog `json:"delete_log,omitempty" bson:"delete_log,omitempty"`
	CreatedAt     int64          `json:"created_at" bson:"created_at"`
}

type DeleteUserLog struct {
	DeletedBy string `json:"deleted_by"`
	Comment   string `json:"comment"`
}

type UserAuditLog struct {
	OldEmail   string `json:"old_email,omitempty" bson:"old_email,omitempty"`
	NewEmail   string `json:"new_email,omitempty" bson:"new_email,omitempty"`
	OldPhoneNo string `json:"old_phone_no,omitempty" bson:"old_phone_no,omitempty"`
	NewPhoneNo string `json:"new_phone_no,omitempty" bson:"new_phone_no,omitempty"`
	OldIsdCode string `json:"old_isd_code,omitempty" bson:"old_isd_code,omitempty"`
	NewIsdCode string `json:"new_isd_code,omitempty" bson:"new_isd_code,omitempty"`
}

type GuestAuditLog struct {
	GuestID string `json:"guest_id" bson:"guest_id"`
}

type GoogleUser struct {
	Email         string `json:"email"`
	VerifiedEmail bool   `json:"verified_email"`
	Name          string `json:"name"`
	GivenName     string `json:"given_name"`
	FamilyName    string `json:"family_name"`
	Picture       string `json:"picture"`
}

type FbUser struct {
	Email     string                 `json:"email"`
	FirstName string                 `json:"first_name"`
	LastName  string                 `json:"last_name"`
	Picture   map[string]interface{} `json:"picture"`
}

type CustomUser struct {
	Email         string `json:"email"`
	FirstName     string `json:"first_name"`
	LastName      string `json:"last_name"`
	Picture       string `json:"picture"`
	Source        string `json:"source"`
	EmailVerified bool   `json:"email_verified"`
}

type UserWithDevices struct {
	UserModel
	Devices []string `json:"devices_id,omitempty"`
}

type ExtendedModel struct {
	UserId    string                 `json:"user_id,omitempty" bson:"user_id,omitempty"`
	Data      map[string]interface{} `json:"data,omitempty" bson:"data,omitempty"`
	CreatedAt int64                  `json:"created_at,omitempty" bson:"created_at"`
	UpdatedAt int64                  `json:"updated_at,omitempty" bson:"updated_at"`
}
