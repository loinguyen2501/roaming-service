package model

// Login mode constant
const (
	Circles = "CIRCLES"
	Social  = "SOCIAL"
	Guest   = "GUEST"
)

type ErrorResponse struct {
	Object  string `json:"object" example:"error"`
	Code    uint32 `json:"code" example:"400"`
	Message string `json:"message" example:"status bad request"`
}

type SuccessResponse struct {
	Object  string `json:"object"`
	Message string `json:"message"`
}
