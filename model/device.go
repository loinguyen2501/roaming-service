package model

var DeviceTypes = map[string]bool{
	"Mobile": true,
	"Web":    true,
}

type UserDevice struct {
	Id          string `json:"id,omitempty" bson:"_id,omitempty"`
	UserId      string `json:"user_id" bson:"user_id"`
	DeviceId    string `json:"device_id" bson:"device_id"`
	Imei        string `json:"imei,omitempty" bson:"imei,omitempty"`
	MacId       string `json:"mac_id,omitempty" bson:"mac_id,omitempty"`
	DeviceToken string `json:"device_token,omitempty" bson:"device_token,omitempty"`
	DeviceType  string `json:"device_type,omitempty" bson:"device_type,omitempty"`
	DeviceOS    string `json:"device_os,omitempty" bson:"device_os,omitempty"`
	UserAgent   string `json:"user_agent,omitempty" bson:"user_agent,omitempty"`
	Status      string `json:"status,omitempty" bson:"status,omitempty"`
	CreatedAt   int64  `json:"created_at" bson:"created_at"`
	UpdatedAt   int64  `json:"updated_at" bson:"updated_at"`
}

func (a *UserDevice) Validate() bool {
	return a.DeviceId != "" && DeviceTypes[a.DeviceType]
}
