package model

import (
	"github.com/mongodb/mongo-go-driver/bson/objectid"
)

const (
	// for telco user id
	DELIMITER = "::"
	Active    = "Active"
	Inactive  = "Inactive"
)

var circlesInfo = map[string]string{
	"customerAccountNumber":           "customer_account",
	"customerAccountIdType":           "customer_account_id_type",
	"customerPorted":                  "customer_ported",
	"customerPortedStatus":            "customer_ported_status",
	"referralCode":                    "referral_code",
	"status":                          "status",
	"orderReferenceNumber":            "order_reference_number",
	"billingAddress":                  "billing_address",
	"billingCycleName":                "billing_cycle_name",
	"billingAccountNumber":            "billing_account_number",
	"serviceAccountNumber":            "service_account_number",
	"serviceAccountEmail":             "service_account_email",
	"serviceInstanceNumber":           "service_instance_number",
	"serviceInstanceInventoryPrimary": "service_instance_inventory_primary",
	"serviceInstanceCustomerStatus":   "service_instance_customer_status",
	"serviceInstanceBasePlanName":     "service_instance_base_plan_name",
}

type TelcoInfo struct {
	RawId           *objectid.ObjectID     `json:"raw_id,omitempty" bson:"_id,omitempty"`
	Id              string                 `json:"id,omitempty" bson:"id,omitempty"`
	UserId          string                 `json:"user_id" bson:"user_id"`
	CustomerName    string                 `json:"customer_name" bson:"customer_name"`
	ResigteredEmail string                 `json:"resigtered_email" bson:"resigtered_email"`
	BillingEmail    string                 `json:"billing_email" bson:"billing_email"`
	ISDCode         string                 `json:"isd_code" bson:"isd_code"`
	PhoneNo         string                 `json:"phone_no" bson:"phone_no"`
	CreationDate    string                 `json:"creation_date" bson:"creation_date"`
	ActivationDate  string                 `json:"activation_date" bson:"activation_date"`
	IdentifierInfo  map[string]string      `json:"identifier_info" bson:"identifier_info"`
	CirclesInfo     map[string]interface{} `json:"circles_info" bson:"circles_info"`
	Status          string                 `json:"status" bson:"status"`
	CreatedAt       int64                  `json:"created_at" bson:"created_at"`
	UpdatedAt       int64                  `json:"updated_at" bson:"updated_at"`
}

// get custom userId for user model made up of isd and phone number
func (t *TelcoInfo) GetTelcoUserId() string {
	return string(t.ISDCode + DELIMITER + t.PhoneNo)
}

// update source telco info as per destination model

func (s *TelcoInfo) UpdateTelcoInfo(d *TelcoInfo) {
	s.ResigteredEmail = d.ResigteredEmail
	s.BillingEmail = d.BillingEmail
	s.CustomerName = d.CustomerName
	s.ISDCode = d.ISDCode
	s.PhoneNo = d.PhoneNo
	s.Status = d.Status
	s.CreationDate = d.CreationDate
	s.ActivationDate = d.ActivationDate
	s.IdentifierInfo = d.IdentifierInfo
	s.CirclesInfo = d.CirclesInfo
}

func (s *TelcoInfo) UpdateTelcoStatus(status string) {
	s.Status = status
}

func GetCirclesInfoDataKeys() (keys []string) {
	for k := range circlesInfo {
		keys = append(keys, k)
	}
	return keys
}

func GetCirclesInfoKey(key string) string {
	return circlesInfo[key]
}
