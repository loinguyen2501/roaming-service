package datastore

import (
	"context"
	"time"

	"bitbucket.org/libertywireless/roaming-service/config"
	"bitbucket.org/libertywireless/roaming-service/constant"
	"bitbucket.org/libertywireless/roaming-service/logging"
	"github.com/globalsign/mgo"
)

type MgoStore struct {
	session *mgo.Session
}

var mgoStore = &MgoStore{}

func InitStore(config config.GeneralConfig) {
	log := logging.GetLogger(context.Background())
	log.Logger.Info(constant.InMgoStore)

	dialInfo, err := mgo.ParseURL(config.DatabaseHost)
	if err != nil {
		log.Logger.WithError(err).Error(constant.MgoUrlParseErr)
		return
	}

	dialInfo.Database = config.DatabaseName
	dialInfo.PoolLimit = config.MgoPoolSize
	dialInfo.Timeout = time.Duration(config.MgoTimeOut) * time.Second

	ses, err := mgo.DialWithInfo(dialInfo)
	if err != nil {
		log.Logger.WithError(err).Error(constant.MgoSesInitFail)
	}
	mgoStore.session = ses

	log.Logger.Info(constant.OutMgoStore)
}

func GetMgoStore(config config.GeneralConfig) *mgo.Database {
	if mgoStore.session == nil {
		return nil
	}
	return mgoStore.session.DB(config.DatabaseName)
}
