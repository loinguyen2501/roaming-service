package datastore

import (
	"bitbucket.org/libertywireless/roaming-service/config"
	"bitbucket.org/libertywireless/roaming-service/constant"
	"bitbucket.org/libertywireless/roaming-service/logging"
	"context"
	"github.com/mongodb/mongo-go-driver/bson"
	"github.com/mongodb/mongo-go-driver/mongo"
	"strings"
	"sync"
)

type MongoDatastore struct {
	DB      *mongo.Database
	Session *mongo.Client
}

func NewDatastore(config config.GeneralConfig) *MongoDatastore {
	var mongoDataStore *MongoDatastore
	db, session := connect(config)
	if db != nil && session != nil {
		mongoDataStore = new(MongoDatastore)
		mongoDataStore.DB = db
		mongoDataStore.Session = session
		return mongoDataStore
	}

	log := logging.GetLogger(context.Background())
	log.Logger.Fatalf("Failed to connect to database: %v", config.DatabaseName)

	return nil
}

func connect(generalConfig config.GeneralConfig) (a *mongo.Database, b *mongo.Client) {
	var connectOnce sync.Once
	var db *mongo.Database
	var session *mongo.Client
	connectOnce.Do(func() {
		db, session = connectToMongo(generalConfig)
	})

	return db, session
}

func connectToMongo(generalConfig config.GeneralConfig) (a *mongo.Database, b *mongo.Client) {
	var err error
	log := logging.GetLogger(context.Background())

	session, err := mongo.NewClient(generalConfig.DatabaseHost)
	if err != nil {
		log.Logger.Fatal(err)
	}
	err = session.Connect(context.TODO())
	if err != nil {
		log.Logger.Fatal(err)
	}

	var DB = session.Database(generalConfig.DatabaseName)
	log.Logger.Info("Successfully connected to database: %v", generalConfig.DatabaseName)

	return DB, session
}


func ensureIndex(collection *mongo.Collection, indexMap map[string]*bson.Document) error {

	log := logging.GetLogger(context.Background())
	indexView := collection.Indexes()

	for k, index := range indexMap {
		if isCompositeKey(k) {
			doc := bson.NewDocument()
			allKeys := strings.Split(k, "-")
			for i := 0; i < len(allKeys); i++ {
				doc.Append(bson.EC.Interface(allKeys[i], int32(1)))
				log.Logger.WithField(constant.Identifier, allKeys[i]).Info("index check for key")
			}
			indexModel := mongo.IndexModel{Keys: doc, Options: index}
			_, err := indexView.CreateOne(context.Background(), indexModel)

			if err != nil {
				log.Logger.WithError(err).Fatal("fail to create")
			}
		} else {
			indexModel := mongo.IndexModel{Keys: bson.NewDocument(bson.EC.Interface(k, int32(1))), Options: index}
			_, err := indexView.CreateOne(context.Background(), indexModel)

			log.Logger.WithField(constant.Identifier, k).Info("index check for key")
			if err != nil {
				log.Logger.WithError(err).Fatal("fail to create")
			}
		}
	}

	return nil
}

func isCompositeKey(key string) bool {
	return len(strings.Split(key, "-")) > 1
}
