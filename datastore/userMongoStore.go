package datastore

import (
	"context"
	"errors"
	"time"

	"bitbucket.org/libertywireless/roaming-service/constant"
	"bitbucket.org/libertywireless/roaming-service/logging"
	"bitbucket.org/libertywireless/roaming-service/model"
	"github.com/mongodb/mongo-go-driver/bson/objectid"
	"github.com/mongodb/mongo-go-driver/mongo"
)

type UserMongoStore struct {
	Datastore      *MongoDatastore
	userCollection *mongo.Collection
}

func NewUserMongoStore(mongdatastore *MongoDatastore) *UserMongoStore {
	log := logging.GetLogger(context.Background())
	log.Logger.Info(constant.InUserStore)

	userMongoStore := new(UserMongoStore)
	userMongoStore.Datastore = mongdatastore
	userMongoStore.userCollection = userMongoStore.Datastore.DB.Collection(userCollectionName)
	ensureIndex(userMongoStore.userCollection, userIndexKeys)

	log.Logger.Info(constant.OutUserStore)
	return userMongoStore
}

func (a *UserMongoStore) Create(ctx context.Context, userModel *model.UserModel) (string, error) {
	res, err := a.userCollection.InsertOne(ctx, userModel)

	if err != nil {
		log := logging.GetLogger(ctx)
		log.Logger.WithError(err).Error("failed to create model in UserMongoStore")
		if err, ok := err.(mongo.WriteErrors); ok {
			if (err)[0].Code == 11000 {
				return "", errors.New(constant.DuplicateAccess)
			}
		}
		return "", errors.New(constant.ServerIssue)
	}

	id := res.InsertedID.(objectid.ObjectID).Hex()
	return id, nil
}

func (a *UserMongoStore) Find(ctx context.Context, criteria interface{}) (*model.UserModel, error) {
	var user model.UserModel
	err := a.userCollection.FindOne(ctx, criteria).Decode(&user)
	if err != nil {
		log := logging.GetLogger(ctx)
		log.Logger.WithError(err).Error("failed to find model in UserMongoStore")
		return nil, err
	}
	user.Id = user.RawId.Hex()
	user.RawId = nil

	return &user, err
}

func (a *UserMongoStore) FindWithSoftDelete(ctx context.Context, isSoftDeletedRecordsInclude bool, criteria M) (*model.UserModel, error) {
	// soft delete filter
	query := AppendSoftDeleteQuery(isSoftDeletedRecordsInclude, criteria)
	var user model.UserModel
	err := a.userCollection.FindOne(ctx, query).Decode(&user)
	if err != nil {
		return nil, err
	}
	user.Id = user.RawId.Hex()
	user.RawId = nil

	return &user, err
}

func (a *UserMongoStore) FindAllWithSoftDelete(ctx context.Context, isSoftDeletedRecordsInclude bool, criteria M) ([]model.UserModel, error) {

	// soft delete filter
	query := AppendSoftDeleteQuery(isSoftDeletedRecordsInclude, criteria)
	var allUsers []model.UserModel
	log := logging.GetLogger(ctx)

	cur, err := a.userCollection.Find(ctx, query)
	if err != nil {
		log.Logger.WithError(err).Error("failed to find all model in UserMongoStore")
		return nil, err
	}
	defer cur.Close(context.Background())

	for cur.Next(context.Background()) {
		var elem model.UserModel
		err := cur.Decode(&elem)
		if err != nil {
			return nil, errors.New(constant.FailDecode)
		}
		elem.Id = elem.RawId.Hex()
		elem.RawId = nil
		allUsers = append(allUsers, elem)
	}

	if err := cur.Err(); err != nil {
		return nil, errors.New(constant.IterateOverErr)
	}

	return allUsers, err
}

func (a *UserMongoStore) Update(ctx context.Context, persistedUser *model.UserModel, user *model.UserUpdateRequestModel) error {

	res, err := a.userCollection.UpdateOne(ctx, M{
		"external_id": user.ExternalId,
		"updated_at":  persistedUser.UpdatedAt,
	}, M{"$set": user})

	if err != nil {
		log := logging.GetLogger(ctx)
		log.Logger.WithError(err).Error("failed to update model in UserMongoStore")
		if err, ok := err.(mongo.WriteErrors); ok {
			if (err)[0].Code == 11000 {
				return errors.New(constant.DuplicateAccess)
			}
		}
		return errors.New(constant.ServerIssue)
	}

	if res.ModifiedCount < 1 {
		return errors.New(constant.StaleDataErr)
	}

	return nil
}

func (a *UserMongoStore) Update2(ctx context.Context, user *model.UserModel) error {

	persistedUser, err := a.FindWithSoftDelete(ctx, false, M{"external_id": user.ExternalId})
	if err != nil {
		return err
	}

	user.Id = ""
	res, err := a.userCollection.ReplaceOne(ctx, M{
		"external_id": user.ExternalId,
		"updated_at":  persistedUser.UpdatedAt,
	}, user)

	if err != nil {
		log := logging.GetLogger(ctx)
		log.Logger.WithError(err).Error("failed to replace model in UserMongoStore")
		if err, ok := err.(mongo.WriteErrors); ok {
			if (err)[0].Code == 11000 {
				return errors.New(constant.DuplicateAccess)
			}
		}
		return errors.New(constant.ServerIssue)
	}

	if res.ModifiedCount < 1 {
		return errors.New(constant.StaleDataErr)
	}

	return nil
}

func (a *UserMongoStore) FindAll(ctx context.Context, criteria interface{}) ([]model.UserModel, error) {

	var allUsers []model.UserModel
	log := logging.GetLogger(ctx)

	cur, err := a.userCollection.Find(ctx, criteria)
	if err != nil {
		log.Logger.WithError(err).Error("failed to find all model in UserMongoStore")
		return nil, err
	}
	defer cur.Close(context.Background())

	for cur.Next(context.Background()) {
		var elem model.UserModel
		err := cur.Decode(&elem)
		if err != nil {
			log.Logger.WithError(err).Error("failed to decode when trying to find all model in UserMongoStore")
			return nil, errors.New(constant.FailDecode)
		}
		elem.Id = elem.RawId.Hex()
		elem.RawId = nil
		allUsers = append(allUsers, elem)
	}

	if err := cur.Err(); err != nil {
		log.Logger.WithError(err).Error("failed to iterate when trying to find all model in UserMongoStore")
		return nil, errors.New(constant.IterateOverErr)
	}

	return allUsers, err
}

func (a *UserMongoStore) Delete(ctx context.Context, criteria interface{}) error {
	_, err := a.userCollection.DeleteMany(ctx, criteria)
	return err
}

func (a *UserMongoStore) SoftDelete(ctx context.Context, user *model.UserModel) error {
	_, err := a.userCollection.UpdateOne(ctx, M{
		"external_id": user.ExternalId,
		"updated_at":  user.UpdatedAt,
	}, M{"$set": M{"deleted_at": time.Now().Unix()}})
	return err
}
