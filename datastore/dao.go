package datastore

import (
	"context"

	"bitbucket.org/libertywireless/roaming-service/model"
	"github.com/mongodb/mongo-go-driver/bson"
	"github.com/mongodb/mongo-go-driver/mongo"
)

type M map[string]interface{}

func UniqueIndex() *bson.Document {
	indexBuilder := mongo.NewIndexOptionsBuilder()
	indexBuilder.Unique(true)
	indexBuilder.Background(true)
	return indexBuilder.Build()
}

func SparseIndex() *bson.Document {
	indexBuilder := mongo.NewIndexOptionsBuilder()
	indexBuilder.Sparse(true)
	indexBuilder.Background(true)
	return indexBuilder.Build()
}

func SparseUniqueIndex() *bson.Document {
	indexBuilder := mongo.NewIndexOptionsBuilder()
	indexBuilder.Sparse(true)
	indexBuilder.Unique(true)
	indexBuilder.Background(true)
	return indexBuilder.Build()
}

var userCollectionName = "users"
var userIndexKeys = map[string]*bson.Document{
	"external_id":       UniqueIndex(),
	"email_confirmed":   SparseIndex(),
	"phone_confirmed":   SparseIndex(),
	"telco_user":        SparseIndex(),
	"created_at":        SparseIndex(),
	"updated_at":        SparseIndex(),
	"source":            SparseIndex(),
	"email":             SparseUniqueIndex(),
	"isd_code-phone_no": SparseUniqueIndex(),
}

type UserStore interface {
	Create(context.Context, *model.UserModel) (string, error)
	Update(context.Context, *model.UserModel, *model.UserUpdateRequestModel) error
	Update2(context.Context, *model.UserModel) error
	Find(context.Context, interface{}) (*model.UserModel, error)
	FindAll(context.Context, interface{}) ([]model.UserModel, error)
	FindWithSoftDelete(ctx context.Context, includeDeletedRecords bool, criteria M) (*model.UserModel, error)
	FindAllWithSoftDelete(ctx context.Context, includeDeletedRecords bool, criteria M) ([]model.UserModel, error)
	SoftDelete(ctx context.Context, user *model.UserModel) error
	Delete(ctx context.Context, criteria interface{}) error
}

var userLoginCollectionName = "user_logins"
var loginIndexKeys = map[string]*bson.Document{
	"created_at":        SparseIndex(),
	"updated_at":        SparseIndex(),
	"user_id-device_id": SparseUniqueIndex(),
}

type LoginStore interface {
	Create(context.Context, *model.UserLogin) (string, error)
	Update(context.Context, *model.UserLogin) error
	Find(context.Context, interface{}) (*model.UserLogin, error)
	Delete(context.Context, interface{}) error
}

var deviceCollectionName = "user_devices"
var deviceIndexKeys = map[string]*bson.Document{
	"device_id":  SparseIndex(),
	"user_id":    SparseIndex(),
	"created_at": SparseIndex(),
	"updated_at": SparseIndex(),
}

type DeviceStore interface {
	Create(context.Context, *model.UserDevice) (string, error)
	Update(context.Context, *model.UserDevice) error
	Find(context.Context, interface{}) (*model.UserDevice, error)
	FindAll(context.Context, interface{}) ([]model.UserDevice, error)
	Delete(context.Context, interface{}) error
}

func AppendSoftDeleteQuery(isDeleted bool, criteria M) interface{} {
	return M{"$and": []M{criteria, {"deleted_at": M{"$exists": isDeleted}}}}
}
