package logging

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"time"

	"bitbucket.org/libertywireless/roaming-service/config"
	"github.com/fsnotify/fsnotify"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/sirupsen/logrus"
)

type StructuredLogger struct {
	Logger *logrus.Logger
}

func NewStructuredLogger(logger *logrus.Logger) func(next http.Handler) http.Handler {
	return middleware.RequestLogger(&StructuredLogger{logger})
}

var Logger = logrus.New()
var logfile *os.File

func (l *StructuredLogger) NewLogEntry(r *http.Request) middleware.LogEntry {
	entry := &StructuredLoggerEntry{Logger: logrus.NewEntry(l.Logger)}
	logFields := logrus.Fields{}

	logFields["@timestamp"] = time.Now().Format(time.RFC3339Nano)

	if reqID := middleware.GetReqID(r.Context()); reqID != "" {
		logFields["req_id"] = reqID
	}

	scheme := "http"
	if r.TLS != nil {
		scheme = "https"
	}
	logFields["http_scheme"] = scheme
	logFields["http_proto"] = r.Proto
	logFields["http_method"] = r.Method

	logFields["remote_addr"] = r.RemoteAddr
	logFields["user_agent"] = r.UserAgent()

	logFields["uri"] = fmt.Sprintf("%s://%s%s", scheme, r.Host, r.RequestURI)

	entry.Logger = entry.Logger.WithFields(logFields)

	entry.Logger.Infoln("request started")

	return entry
}

type StructuredLoggerEntry struct {
	Logger logrus.FieldLogger
}

func (l *StructuredLoggerEntry) Write(status, bytes int, elapsed time.Duration) {
	l.Logger = l.Logger.WithFields(logrus.Fields{
		"resp_status": status, "resp_bytes_length": bytes,
		"resp_elapsed_ms": float64(elapsed.Nanoseconds()) / 1000000.0,
	})

	l.Logger.Infoln("request complete")
}

func (l *StructuredLoggerEntry) Panic(v interface{}, stack []byte) {
	l.Logger = l.Logger.WithFields(logrus.Fields{
		"stack": string(stack),
		"panic": fmt.Sprintf("%+v", v),
	})
}

func GetLogger(ctx context.Context) *StructuredLoggerEntry {
	logger := ctx.Value(middleware.LogEntryCtxKey)
	if logger != nil {
		logEntry := logger.(*StructuredLoggerEntry)
		logEntry.Logger = logEntry.Logger.WithField("@timestamp", time.Now().Format(time.RFC3339Nano))
		return logEntry
	}
	return &StructuredLoggerEntry{Logger: Logger}
}

func InitializeLogger(config *config.GeneralConfig) {

	l, e := logrus.ParseLevel(config.LogLevel)
	if e != nil {
		l = logrus.InfoLevel
	}

	logpath := config.LogFilePath
	var logFormatter = logrus.Logger{}
	if config.MiscData["log_format_json"].(bool) {
		logFormatter.Formatter = new(logrus.JSONFormatter)
	} else {
		logFormatter.Formatter = new(logrus.TextFormatter)
	}
	if logpath != "" {
		logfile, err := os.OpenFile(logpath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0755)
		if err == nil {
			*Logger = logrus.Logger{
				Out:       logfile,
				Formatter: logFormatter.Formatter,
				Hooks:     make(logrus.LevelHooks),
				Level:     l,
			}
		}
		Logger.Info("initialized Logger successfully")
	}
}

func WatchLoggerChanges(watcher *fsnotify.Watcher, config *config.GeneralConfig, r *chi.Mux) {
	//done := make(chan bool)

	// Process events
	go func() {
		for {
			select {
			case ev := <-watcher.Events:
				if ev.Name == config.LogFilePath && (ev.Op.String() == "REMOVE" || ev.Op.String() == "RENAME") {
					logfile.Close()
					f, err := os.OpenFile(config.LogFilePath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0755)
					if err == nil {
						Logger.Out = f
						logfile = f
					} else {
						panic(err)
					}
					err = watcher.Add(logfile.Name())
					if err != nil {
						panic(err)
					}
				}
			case err := <-watcher.Errors:
				Logger.Error("error:", err)
			}
		}
	}()
	err := watcher.Add(config.LogFilePath)
	if err != nil {
		panic(err)
	}
	// Hang so program doesn't exit
	//<-done
}
