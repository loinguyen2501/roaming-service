package main

import (
	"flag"
	"net/http"

	"bitbucket.org/libertywireless/roaming-service/config"
	"bitbucket.org/libertywireless/roaming-service/controller"
	"bitbucket.org/libertywireless/roaming-service/gen_docs"
	_ "bitbucket.org/libertywireless/roaming-service/gen_docs"
	"bitbucket.org/libertywireless/roaming-service/logging"
	"github.com/fsnotify/fsnotify"
)

// @title User Service API
// @version 1.0
// @description User Service API documentation.

// @contact.name API Support
// @contact.email roaming-service-dev@circles.asia

// @host localhost:3000
// @BasePath /
func main() {

	var configFilePath string
	var serverPort string
	flag.StringVar(&configFilePath, "config", "config.yml", "absolute path to the configuration file")
	flag.StringVar(&serverPort, "server_port", "3000", "port on which server runs")
	flag.Parse()
	generalConfig := config.Loadconfig(configFilePath)
	initSwagger(generalConfig)
	logging.InitializeLogger(generalConfig)
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		panic(err)
	}
	defer watcher.Close()
	r := controller.Handler(generalConfig)
	if generalConfig.MiscData["log_rotation"] == "true" {
		go logging.WatchLoggerChanges(watcher, generalConfig, r)
	}
	logging.Logger.Infoln("############################## Server Started ##############################")
	http.ListenAndServe(":"+serverPort, r)
}

func initSwagger(generalConfig *config.GeneralConfig) {
	gen_docs.SwaggerInfo.Host = generalConfig.SwaggerHost
	gen_docs.SwaggerInfo.Version = generalConfig.SwaggerVersion
	gen_docs.SwaggerInfo.BasePath = generalConfig.SwaggerBasePath
}
