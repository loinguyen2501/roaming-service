package service

import (
	"bytes"
	"context"
	"encoding/json"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/libertywireless/roaming-service/config"
	"bitbucket.org/libertywireless/roaming-service/constant"
	"bitbucket.org/libertywireless/roaming-service/datastore"
	"bitbucket.org/libertywireless/roaming-service/logging"
	"bitbucket.org/libertywireless/roaming-service/model"
	"github.com/dgrijalva/jwt-go"
	"github.com/go-redis/redis"
	"github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"
	"gopkg.in/mgo.v2/bson"
)

const (
	// Status
	OTP_SENT      = "OTP_SENT"
	OTP_VALIDATED = "OTP_VALIDATED"
	FAILED        = "FAILED"

	// Medium of OTP authentication
	SMS_AUTH   = "SMS"
	EMAIL_AUTH = "EMAIL"

	// Channel
	MOBILE_CHANNEL = "mobile"
	EMAIL_CHANNEL  = "email"

	// OTP Request type
	LOGIN_MOBILE_OTP = "LOGIN_MOBILE_OTP"
	FORGOT_PASSWORD  = "FORGOT_PASSWORD"
	VERIFY_EMAIL     = "VERIFY_EMAIL"
	VERIFY_MOBILE    = "VERIFY_MOBILE"
	CHANGE_EMAIL     = "CHANGE_EMAIL"
	CHANGE_MOBILE    = "CHANGE_MOBILE"

	// Login Mode
	EMAIL_PASSWORD  = "EMAIL_PASSWORD"
	MOBILE_PASSWORD = "MOBILE_PASSWORD"
	USERID_PASSWORD = "USERID_PASSWORD"
	MOBILE_OTP      = "MOBILE_OTP"

	// User Audit Type
	OVERRIDE_MOBILE                     = "OVERRIDE_MOBILE"
	OVERRIDE_EMAIL                      = "OVERRIDE_EMAIL"
	GUEST_TO_USER                       = "GUEST_TO_USER"
	HARD_DELETE_USER model.AuditLogType = "HARD_DELETE_USER"
	SOFT_DELETE_USER model.AuditLogType = "SOFT_DELETE_USER"

	// OTP Seed
	MIN = 1000
	MAX = 9999

	//Custom Metric
	LinkUserTelcoAccountKirk   = "LINK_USER_TELCO_ACCOUT_KIRK"
	PasswordBcryptTime         = "PWD_BCRYPT_TIME"
	PasswordValidateBcryptTime = "PWD_VALIDATE_BCRYPT_TIME"

	// OTP Redis Prefix
	REDIS_OTP_PREFIX = "user_service_otpauth_"
)

func RandomINT(min, max int) int {
	rand.Seed(time.Now().Unix())
	return rand.Intn(max-min) + min
}


func JSONHTTPPost(ctx context.Context, payload interface{}, headers interface{}, url string, newrelicMetricName string) (*http.Response, error) {

	data, err := json.Marshal(payload)
	log := logging.GetLogger(ctx)

	if err != nil {
		return nil, err
	}
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(data))

	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")

	resp, err := SendHttpRequest(req, newrelicMetricName)
	if err != nil {
		log.Logger.WithError(err).Error("Failed to receive response in JSONHTTPPost")
		return nil, err
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)
	log.Logger.WithFields(map[string]interface{}{
		"Status":  resp.Status,
		"Headers": resp.Header,
		"Body":    string(body),
	}).Info("Received response in JSONHTTPPost")

	return resp, err
}

func RecordLogin(ctx context.Context, externalId, loginMode, loginProvider string, deviceModel model.UserDevice, loginStore datastore.LoginStore, authToken string) error {
	userLogin, err := loginStore.Find(ctx, bson.M{"user_id": externalId, "device_id": deviceModel.DeviceId})
	log := logging.GetLogger(ctx)

	if err == nil {
		currentTime := time.Now().Unix()
		userLogin.LoginMode = loginMode
		userLogin.LoginProvider = loginProvider
		userLogin.AuthToken = authToken
		userLogin.UpdatedAt = currentTime

		log.Logger.Info(externalId + " is logged in")
		err = loginStore.Update(ctx, userLogin)

	} else if err.Error() == "mongo: no documents in result" {
		currentTime := time.Now().Unix()
		userLogin = new(model.UserLogin)
		userLogin.UserId = externalId
		userLogin.LoginMode = loginMode
		userLogin.AuthToken = authToken
		userLogin.DeviceId = deviceModel.DeviceId
		userLogin.DeviceType = deviceModel.DeviceType
		userLogin.LoginProvider = loginProvider
		userLogin.CreatedAt = currentTime
		userLogin.UpdatedAt = currentTime

		log.Logger.Info(externalId + " is logged in for the first time")
		_, err = loginStore.Create(ctx, userLogin)
	}

	return err
}

func RecordDevice(ctx context.Context, externalId string, device *model.UserDevice, deviceStore datastore.DeviceStore) error {

	log := logging.GetLogger(ctx)

	if device.DeviceId == "" || externalId == "" || device.DeviceType == "Web" {
		return nil
	}

	_, err := deviceStore.Find(ctx, bson.M{"device_id": device.DeviceId, "user_id": externalId})

	if err != nil && err.Error() == "mongo: no documents in result" {

		device.UserId = externalId
		currentTime := time.Now().Unix()
		device.CreatedAt = currentTime
		device.UpdatedAt = currentTime
		log.Logger.WithField(constant.Identifier, device.DeviceId).Info("record new device")

		_, err = deviceStore.Create(ctx, device)
		if err != nil {
			log.Logger.WithError(err).Error("record new device fail")
		}
	}
	return err
}

// to record new or existing user in db and if telco account present for this user then create and link telco account of user

func RecordUser(ctx context.Context, userStore datastore.UserStore, userInfo model.MigrateUserModel) (*model.UserModel, error) {

	log := logging.GetLogger(ctx)
	currentTime := time.Now().Unix()
	user, _ := userStore.FindWithSoftDelete(ctx, false, datastore.M{"$or": []bson.M{
		{"email": userInfo.User.Email},
		{"phone_no": userInfo.User.PhoneNo, "isd_code": userInfo.User.ISDCode},
		{"external_id": userInfo.User.ExternalId}}})

	if user != nil {
		log.Logger.WithField(constant.Identifier, user.ExternalId).Info("update user with userId in migrate api")
		user.UpdateUserModel(&userInfo.User)
		user.UpdatedAt = currentTime
		userStore.Update2(ctx, user)
		return user, nil
	}

	userModel := MakeUserModel(userInfo.User.UserRegisterRequestModel)
	if userInfo.User.Email != "" {
		userModel.EmailConfirmed = userInfo.User.EmailConfirmed
	}
	if userInfo.User.ISDCode != "" && userInfo.User.PhoneNo != "" {
		userModel.PhoneConfirmed = userInfo.User.PhoneConfirmed
	}

	log.Logger.WithField(constant.Identifier, userModel.ExternalId).Info("create user with userId in migrate api")

	id, err := userStore.Create(ctx, userModel)
	if err != nil {
		return nil, err
	}

	if id != "" {
		userModel.Id = id
	}
	return userModel, nil
}

// MakeUserModel converts the UserRegisterRequestModel to UserModel and also hashes the password field.
func MakeUserModel(user model.UserRegisterRequestModel) *model.UserModel {

	if user.ISDCode == "" || user.PhoneNo == "" {
		user.ISDCode = ""
		user.PhoneNo = ""
	}

	currentTime := time.Now().Unix()
	userModel := &model.UserModel{ExternalId: user.ExternalId, FirstName: user.FirstName, LastName: user.LastName,
		Dob: user.Dob, ImageUrl: user.ImageUrl, Email: user.Email, Password: user.Password, PhoneNo: user.PhoneNo, Source: user.Source,
		ISDCode: user.ISDCode, AddressLine1: user.AddressLine1, AddressLine2: user.AddressLine2, AddressLine3: user.AddressLine3,
		CirclesAccounts: map[string]interface{}{}, City: user.City, State: user.State, Country: user.Country,
		PostalCode: user.PostalCode, SecurityQuestions: user.SecurityQuestions, CreatedAt: currentTime, UpdatedAt: currentTime}

	return userModel
}

func MakeUserModelFromGuest(user model.GuestRegisterRequestModel) *model.UserModel {

	if user.ISDCode == "" || user.PhoneNo == "" {
		user.ISDCode = ""
		user.PhoneNo = ""
	}

	currentTime := time.Now().Unix()
	userModel := &model.UserModel{ExternalId: user.ExternalId, FirstName: user.FirstName, LastName: user.LastName,
		Email:   user.Email,
		ISDCode: user.ISDCode, AddressLine1: user.AddressLine1, AddressLine2: user.AddressLine2, AddressLine3: user.AddressLine3,
		CirclesAccounts: map[string]interface{}{}, City: user.City, State: user.State, Country: user.Country,
		PostalCode: user.PostalCode, CreatedAt: currentTime, UpdatedAt: currentTime}

	return userModel
}

func RecordUserIfNew(ctx context.Context, userStore datastore.UserStore, user *model.CustomUser, config *config.GeneralConfig) (*model.UserModel, string) {

	var userType string
	currentTime := time.Now().Unix()
	dbUser, _ := userStore.FindWithSoftDelete(ctx, false, datastore.M{"email": user.Email})

	if dbUser != nil {
		dbUser.EmailConfirmed = true
		dbUser.UpdatedAt = currentTime
		userStore.Update2(ctx, dbUser)
		userType = model.SignIn
		return dbUser, userType
	}

	dbUser = &model.UserModel{ExternalId: GenExternalID(config.CountryCode), FirstName: user.FirstName,
		LastName: user.LastName, ImageUrl: user.Picture, Email: user.Email,
		Password: uuid.NewV4().String(), EmailConfirmed: true, Source: user.Source,
		CirclesAccounts: map[string]interface{}{}, Country: config.CountryName, CreatedAt: currentTime, UpdatedAt: currentTime}

	id, _ := userStore.Create(ctx, dbUser)
	userType = model.Register
	if id != "" {
		dbUser.Id = id
	}
	return dbUser, userType
}

func MakeLoginClaims(user *model.UserModel, loginTime int64, expiryTime int64, deviceType string) jwt.MapClaims {
	claims := make(jwt.MapClaims)

	if user.ExternalId != "" {
		claims["external_id"] = user.ExternalId
	}
	claims["login_time"] = loginTime
	claims["expiry_time"] = expiryTime
	claims["device_type"] = deviceType

	return claims
}

// genJWTToken returns a jwt token signed using RS256.
// It returns the token in string format.
func GenJWTToken(ctx context.Context, config config.GeneralConfig, claims jwt.MapClaims) (string, error) {

	log := logging.GetLogger(ctx)

	token := jwt.New(jwt.SigningMethodRS256)
	token.Claims = claims
	tokenString, err := token.SignedString(config.PrivateKey)
	if err != nil {
		log.Logger.WithError(err).Error(constant.TokenGenFail)
		return "", err
	}

	return tokenString, err
}

// parses the JWT String & generates the user Claims from the info

func ParseJWTString(ctx context.Context, jwtToken string, config config.GeneralConfig) jwt.MapClaims {

	log := logging.GetLogger(ctx)

	token, err := jwt.Parse(jwtToken, func(token *jwt.Token) (interface{}, error) {
		return config.PublicKey, nil
	})
	if err != nil {
		log.Logger.WithError(err).Error(constant.ParseTokenErr)
		return nil
	}
	return token.Claims.(jwt.MapClaims)
}

func InitRedisClient(config *config.GeneralConfig) *redis.Client {
	log := logging.GetLogger(context.Background())

	log.Logger.Info(constant.InRedis)
	client := redis.NewFailoverClient(&redis.FailoverOptions{
		MasterName:    config.RedisMaster,
		SentinelAddrs: config.RedisSentinelServers,
		Password:      config.RedisPassword,
		PoolSize:      config.RedisPoolSize,
		DB:            config.RedisDB,
	})
	_, err := client.Ping().Result()
	if err != nil {
		log.Logger.WithError(err).Error(constant.RedisInitFail)
		panic("Redis failed to init. Going to panic.")
	}
	log.Logger.Info(constant.OutRedis)
	return client
}

// genExternalID returns an ID string (uppercase).
// The ID is generated based on the system time (accuracy up to nanoseconds) and country code.
// The time will be formatted to base 36 with a prefix appended.
// e.g. SG-BMS9TSJIQIR6

func GenExternalID(prefix string) string {
	return prefix + "-" + strings.ToUpper(strconv.FormatInt(time.Now().UnixNano(), 36))
}

func ValidateSecurityQuestions(questions []model.SecurityQuestion) bool {

	if len(questions) <= 1 {
		return false
	}

	for i := 0; i < len(questions); i++ {
		if questions[i].Question == "" || questions[i].Answer == "" {
			return false
		}
	}
	return true
}

// ValidatePassword checks if hashed and pwd (after it has been hashed) is the same.

func ValidatePassword(hashed string, pwd string) bool {
	start := time.Now()
	err := bcrypt.CompareHashAndPassword([]byte(hashed), []byte(pwd))
	TimeTrack(start, PasswordValidateBcryptTime)
	return err == nil
}

func SpitStr(str string, op string, index int) string {
	if op == "" {
		return str
	}
	s := strings.Split(str, op)
	return s[index]
}

func TimeTrack(start time.Time, name string) {
	end := time.Since(start).Seconds() * 1000
	RecordCustomMetric(name, float64(end))
}

func SendHttpRequest(req *http.Request, clientName string) (*http.Response, error) {
	resp, err := RecordHttpTransaction(req, clientName)
	return resp, err
}

func IsOtpCheck(config *config.GeneralConfig, r *http.Request) bool {
	isCheck, ok := config.MiscData["otp_check"].(bool)
	if !ok {
		isCheck = true
	}

	if !isCheck {
		isHeaderCheck, err := strconv.ParseBool(r.Header.Get("otp-check"))
		if err == nil {
			isCheck = isHeaderCheck
		}
	}

	return isCheck
}

func IsSecurityQuestionCheck(config *config.GeneralConfig) bool {
	isCheck, ok := config.MiscData["security_qn_check"].(bool)
	if !ok {
		isCheck = true
	}
	return isCheck
}

func IsTokenExpired(claims map[string]interface{}) bool {
	if v, ok := claims["expiry_time"].(float64); ok {
		expTime := int64(v)
		return time.Now().Unix() > expTime
	}
	return false
}
