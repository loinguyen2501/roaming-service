package service

import (
	"context"
	"encoding/json"
	"net/http"
	"strings"
	"time"

	"bitbucket.org/libertywireless/roaming-service/constant"
	"bitbucket.org/libertywireless/roaming-service/constant/codes"
	"bitbucket.org/libertywireless/roaming-service/datastore"
	"bitbucket.org/libertywireless/roaming-service/internal/errors"
	"bitbucket.org/libertywireless/roaming-service/logging"
	"bitbucket.org/libertywireless/roaming-service/model"

	"golang.org/x/crypto/bcrypt"
	"gopkg.in/validator.v2"
)

var EMAIL_SECURITY_QUESTION = "email_security_question_text"

func WriteJSON(w http.ResponseWriter, v interface{}) error {
	data, err := json.Marshal(v)
	if err != nil {
		return err
	}
	_, err = w.Write(data)
	return err
}

func WriteJSONWithStatus(w http.ResponseWriter, statusCode int, v interface{}) error {
	w.WriteHeader(statusCode)
	return WriteJSON(w, v)
}

func WriteWithStatus(w http.ResponseWriter, statusCode int) {
	w.WriteHeader(statusCode)
}

// argument error object need to be CustomError type.
// if not, this function with return with 500 status code as default.
func WriteError(r *http.Request, w http.ResponseWriter, err error) error {
	code := getErrorCode(err)
	statusCode := getHttpStatus(code)
	w.WriteHeader(statusCode)
	return WriteJSON(w, &model.ErrorResponse{
		Object:  "error",
		Code:    code,
		Message: err.Error(),
	})
}

func WriteErrorWithMessage(r *http.Request, w http.ResponseWriter, err error, message string) error {
	code := getErrorCode(err)
	statusCode := getHttpStatus(code)

	w.WriteHeader(statusCode)
	return WriteJSON(w, &model.ErrorResponse{
		Object:  "error",
		Code:    code,
		Message: message,
	})
}

// error is not CustomError type return default error code (Internal Server Error)
func getErrorCode(err error) uint32 {
	clError, ok := err.(errors.CirclesLifeError)
	if !ok {
		return codes.InternalErrorCode
	}
	return clError.Code()
}

func getHttpStatus(code uint32) (status int) {
	firstThreeDigits := code / 100
	switch firstThreeDigits {
	case 400:
		status = http.StatusBadRequest
	case 401:
		status = http.StatusUnauthorized
	case 403:
		status = http.StatusForbidden
	case 404:
		status = http.StatusNotFound
	case 405:
		status = http.StatusMethodNotAllowed
	case 406:
		status = http.StatusNotAcceptable
	case 408:
		status = http.StatusRequestTimeout
	default:
		status = http.StatusInternalServerError
	}
	return
}

// record and link telco info for user
func LinkAccount(ctx context.Context, userModel *model.UserModel, telcoAccount string, userStore datastore.UserStore) {

	if userModel.CirclesAccounts == nil {
		userModel.CirclesAccounts = map[string]interface{}{}
	}

	if _, ok := userModel.CirclesAccounts[telcoAccount]; !ok {
		userModel.CirclesAccounts[telcoAccount] = map[string]interface{}{}
		userModel.TelcoUser = true
		userStore.Update2(ctx, userModel)
	}
}

// UnLinkAccount unlinks telco info from user
func UnLinkAccount(ctx context.Context, userModel *model.UserModel, telcoAccount string, userStore datastore.UserStore) {

	if userModel.CirclesAccounts == nil {
		return
	}

	log := logging.GetLogger(ctx)

	// Unlink telco account from user
	if _, ok := userModel.CirclesAccounts[telcoAccount]; ok {
		delete(userModel.CirclesAccounts, telcoAccount)
		log.Logger.Info("Unlinked telco account " + telcoAccount + " from " + userModel.ExternalId)
	}

	// Setting phone confirmed to false if the number is a primary number
	phone := strings.Split(telcoAccount, model.DELIMITER)
	if userModel.ISDCode == phone[0] && userModel.PhoneNo == phone[1] {
		userModel.PhoneConfirmed = false
		log.Logger.Info("Set phone confirmed to false for " + userModel.ExternalId)
	}

	userStore.Update2(ctx, userModel)
}

func HashAndSalt(pwd string) string {

	// MinCost is an integer constant provided by the bcrypt
	// package. Can also use any value that is greater than MinCost

	if pwd != "" {
		start := time.Now()
		hash, err := bcrypt.GenerateFromPassword([]byte(pwd), bcrypt.DefaultCost)
		TimeTrack(start, PasswordBcryptTime)
		if err == nil {
			return string(hash)
		}
	}
	return ""
}

// should be removed once migrate is deprecated
func ValidateFieldsOld(model interface{}, w http.ResponseWriter, r *http.Request) bool {
	err := validator.Validate(model)
	if err != nil {
		errs, ok := err.(validator.ErrorMap)
		if ok {
			for f, _ := range errs {
				err = errors.New(codes.ValidateFieldCode, constant.ValidateFieldErr+" - "+f)
				WriteError(r, w, err)
				return false
			}
		} else {
			err = errors.New(codes.ValidationUnknownCode, constant.ValidationUnknownErr)
			WriteError(r, w, err)
			return false
		}
	}

	return true
}
