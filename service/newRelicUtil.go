package service

import (
	"bitbucket.org/libertywireless/roaming-service/constant"
	"github.com/newrelic/go-agent"
	"github.com/sirupsen/logrus"
	"net/http"
	"time"
)

var app newrelic.Application = nil

func Init(nrObj newrelic.Application) {
	if app == nil {
		app = nrObj
	}
}

func getApp() newrelic.Application {
	return app
}

func RecordCustomMetric(metric string, value float64) {
	if getApp() == nil {
		logrus.Info(constant.NewRelicAgentNotFound)
		return
	}
	getApp().RecordCustomMetric(metric, value)
}

func RecordHttpTransaction(req *http.Request, name string) (*http.Response, error) {
	client := new(http.Client)
	client.Timeout = time.Duration(10 * time.Second)

	if getApp() == nil {
		return client.Do(req)
	}
	txn := getApp().StartTransaction(name, nil, req)
	defer txn.End()
	response, err := client.Do(req)
	return response, err
}
