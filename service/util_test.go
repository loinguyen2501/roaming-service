package service

import (
	"bytes"
	"context"
	"encoding/json"
	"testing"

	"bitbucket.org/libertywireless/roaming-service/config"
)

func TestGetTelcoUser(t *testing.T) {
	generalConfig := new(config.GeneralConfig)
	generalConfig.MiscData = make(map[string]interface{})
	generalConfig.MiscData["kirk_customer_info_url"] = "https://e96e7437-ed00-4025-b631-c4d3b204cbf9.mock.pstmn.io/%v/%v"
	generalConfig.MiscData["kirk_newrelic_metric_name"] = ""

	data, err := GetTelcoUser(context.Background(), "65", "87684565", generalConfig)
	if err != nil {
		t.Errorf("Expected error to be nil. Got '%v'", err)
	}

	var m map[string]interface{}
	json.NewDecoder(bytes.NewBuffer(data)).Decode(&m)
	billingAccountNumber := (m["cache"].(map[string]interface{}))["billingAccountNumber"].(string)

	if billingAccountNumber == "" {
		t.Errorf("Billing account number missing")
	}
}
