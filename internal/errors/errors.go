package errors

type CirclesLifeError interface {
	Error() string
	Code() uint32
}

type baseError struct {
	code    uint32
	message string
}

func (err baseError) Error() string {
	return err.message
}

func (err baseError) Code() uint32 {
	return err.code
}

func New(code uint32, message string) error {
	return new(code, message)
}

func new(code uint32, message string) error {
	return &baseError{
		code:    code,
		message: message,
	}
}
