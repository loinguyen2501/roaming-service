package errors

import (
	"testing"

	"bitbucket.org/libertywireless/roaming-service/internal/errors/codes"
	"github.com/stretchr/testify/assert"
)

func TestNewError(t *testing.T) {
	var eofCode codes.Code = 5001
	errorCodes := make(codes.ErrorMap)
	errorCodes[eofCode] = "EOF"

	err := codes.Add(errorCodes)
	assert.Nil(t, err)
	err = New(eofCode, "EOF")
	assert.NotNil(t, err)

	customError, ok := err.(CustomError)
	if ok {
		code := customError.Code()
		assert.Equal(t, code, eofCode)
		assert.Equal(t, "EOF", code.Message())
	}
}

func TestNewErrorWithCode(t *testing.T) {
	var eofCode codes.Code = 5002
	errorCodes := make(codes.ErrorMap)
	errorCodes[eofCode] = "EOF"

	err := codes.Add(errorCodes)
	assert.Nil(t, err)
	err = WithCode(eofCode)
	assert.NotNil(t, err)

	customError, ok := err.(CustomError)
	if ok {
		code := customError.Code()
		assert.Equal(t, eofCode, code)
		assert.Equal(t, "EOF", code.Message())
	}
}
