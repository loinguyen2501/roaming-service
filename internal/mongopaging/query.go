package mongopaging

import (
	"fmt"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

// PagingQuery is to construct mongo find command (https://docs.mongodb.com/manual/reference/command/find/#dbcmd.find).
// And, it will return cursor id (value of sorting field from last document) and result as []bson.Raw
type PagingQuery interface {
	// Find set the filter for query results.
	Find(criteria interface{}) PagingQuery

	// Sort used to do sorting for query results according to sort field.
	// The sort field may contain two parts,
	// prefix {{-}} or {{+}}, represents ascending or descending order and
	// fieldname {{document field name}} which need to be indexed.
	// Default: -_id
	Sort(field string) PagingQuery

	// Limit is to set the maximun number of documents to be retrieved.
	// There is not default limit.
	Limit(count int) PagingQuery

	// Select used to enable fields which should be retrieved.
	Select(selector interface{}) PagingQuery

	// Cursor is used to do pagination for document query.
	// Documents can be retrieved from that cursor value.
	Cursor(lastCursorValue string) PagingQuery

	// Result will unmarshal mongodb []byte result to specific type
	// return total number of docuemnts without limit for query
	// return cursor to skip this result next time
	Result(result interface{}) (totalCount int, cursor string, err error)

	// Run will run the command to database and return result as []bson.Raw
	Run() (res Result, err error)

	// Explain is to print out prepared query for command.
	Explain() string
}

// Result return type from mongodb Run function.
type Result struct {
	OK       int `bson:"ok"`
	WaitedMS int `bson:"waitedMS"`
	Cursor   struct {
		ID         interface{} `bson:"id"`
		NS         string      `bson:"ns"`
		FirstBatch []bson.Raw  `bson:"firstBatch"`
	} `bson:"cursor"`
}

type pagingQuery struct {

	// mgo database object
	db *mgo.Database

	// mongodb collection name
	collection string

	// query filter bson.M object
	criteria interface{}

	// select specific document fields from collection
	projection interface{}

	// ascending or descending order represent by 1 or -1
	sortKind int

	// document field which used to sort
	sortField string

	// max documents to be retrieved.
	limit int

	// previous cursor value to skip in result
	cursorValue string

	// document field to record as cursor to skip next time (Note: cursor field is also sort field)
	cursorField string

	// error which return from cursor parsing
	cursorError error

	// to create and parse hash value from actual cursor value.
	cursor Cursor

	// use previous cursorValue as reference to skip documents for ascending order
	max bson.D

	// use previous cursorValue as reference to skip documents for descending order.
	min bson.D
}

// New is to construct PagingQuery object with mongo.Database and collection name
func New(db *mgo.Database, collection string) PagingQuery {
	return &pagingQuery{
		db:         db,
		cursor:     cursor{},
		collection: collection,
	}
}

// set the filter to query
func (query *pagingQuery) Find(criteria interface{}) PagingQuery {
	query.criteria = criteria
	return query
}

// set the sorting field and type to query
func (query *pagingQuery) Sort(field string) PagingQuery {
	return query.sort(field)
}

// sort is to prepare mongo sorting statement from custom format (-fieldname)
func (query *pagingQuery) sort(field string) PagingQuery {
	n := 1
	if field == "" {
		field = "-_id"
	}
	if field[0] == '+' {
		field = field[1:]
	} else if field[0] == '-' {
		n, field = -1, field[1:]
	}
	query.sortField = field
	query.sortKind = n
	return query
}

// set number of documents to query
func (query *pagingQuery) Limit(limit int) PagingQuery {
	query.limit = limit
	return query
}

// set previous cursor value to query
func (query *pagingQuery) Cursor(lastCursorValue string) PagingQuery {
	query.cursorValue = lastCursorValue
	return query
}

// set document fields which need to be in result
func (query *pagingQuery) Select(selector interface{}) PagingQuery {
	query.projection = selector
	return query
}

// run mongodb with prepared command
func (query *pagingQuery) Run() (res Result, err error) {
	cmd, err := query.prepareCommand()
	if err != nil {
		return
	}
	err = query.db.Run(cmd, &res)
	if err != nil {
		return
	}
	return
}

// decode mongodb result to specific type
// and return total number of documents without limit for query
// return cursor value to skip this result
/* Result will call the following functions
   i) PrepareCommand()
   ii) Run()
   iii) UnMarshall the result & send back
*/
func (query *pagingQuery) Result(result interface{}) (totalCount int, cursor string, err error) {

	totalCount, err = query.getTotalCount()
	if err != nil {
		return
	}

	// run command in database
	res, err := query.Run()
	if err != nil {
		return
	}

	// set the cursor logic
	firstBatch := res.Cursor.FirstBatch
	if len(firstBatch) > 0 {
		// create cursor with the field and value of last document from results
		if query.cursorField != "" {
			var doc bson.M
			err = firstBatch[len(firstBatch)-1].Unmarshal(&doc)
			if err != nil {
				return
			}
			cursorData := bson.D{bson.DocElem{Name: query.cursorField, Value: doc[query.cursorField]}}
			cursor, err = query.cursor.Create(cursorData)
			if err != nil {
				return
			}
		}
	} else {
		cursor = query.cursorValue
	}

	// decode each value from result byte array
	err = query.db.C(query.collection).NewIter(nil, firstBatch, 0, nil).All(result)
	return
}

// setOffset is to choose mongo query operator ($min, $max) based on
// sorting type ascending and descanding
func (query *pagingQuery) setOffset() error {
	// set sort field as cursor field
	query.cursorField = query.sortField

	// choose $min or $max based on sorting type ascending or descending
	if query.cursorValue != "" {
		if query.sortKind == -1 {
			query.max, query.cursorError = query.cursor.Parse(query.cursorValue)
		} else {
			query.min, query.cursorError = query.cursor.Parse(query.cursorValue)
		}
	} else {
		query.min, query.cursorError = nil, nil
		query.max, query.cursorError = nil, nil
	}

	if query.cursorError != nil {
		return query.cursorError
	}
	return nil
}

// get totalCount of documents by filter
func (query *pagingQuery) getTotalCount() (int, error) {
	return query.db.C(query.collection).Find(query.criteria).Count()
}

// prepare mongodb command with parameters
func (query *pagingQuery) prepareCommand() (interface{}, error) {
	err := query.setOffset()
	if err != nil {
		return nil, err
	}

	cmd := bson.D{
		{Name: "find", Value: query.collection},
		{Name: "limit", Value: query.limit},
		{Name: "batchSize", Value: query.limit},
		{Name: "singleBatch", Value: true},
	}

	if query.criteria != nil {
		cmd = append(cmd, bson.DocElem{Name: "filter", Value: query.criteria})
	}

	if query.sortField != "" {
		cmd = append(cmd, bson.DocElem{Name: "sort", Value: bson.M{query.sortField: query.sortKind}})
	}

	if query.projection != nil {
		cmd = append(cmd, bson.DocElem{Name: "projection", Value: query.projection})
	}

	if query.min != nil {
		cmd = append(cmd,
			bson.DocElem{Name: "skip", Value: 1},
			bson.DocElem{Name: "min", Value: query.min},
		)
	}

	if query.max != nil {
		cmd = append(cmd, bson.DocElem{Name: "max", Value: query.max})
	}
	return cmd, nil
}

// to print out actual query which is used
func (query *pagingQuery) Explain() string {
	cmd, err := query.prepareCommand()
	if err != nil {
		return fmt.Sprintf("%s", err)
	}
	return fmt.Sprintf("%v \n", cmd)
}
