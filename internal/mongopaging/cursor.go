package mongopaging

import (
	"encoding/base64"

	"github.com/globalsign/mgo/bson"
)

// Cursor to convert bson.D object to hash value and vice versa
type Cursor interface {

	// Create bson.D object to hash value
	// for example: bson.D{{Name:"id", Value: "5be3ec8bc5f94a46749adfc6"}}
	// return base64 hash value as tring
	Create(cursorData bson.D) (string, error)

	// Parse hash value string to bson.D
	Parse(c string) (cursorData bson.D, err error)
}

type cursor struct{}

// Create hash value from bson.D cursor value
func (cursor) Create(cursorData bson.D) (string, error) {
	data, err := bson.Marshal(cursorData)
	return base64.RawURLEncoding.EncodeToString(data), err
}

// Parse hash value to bson.D cursor value
func (cursor) Parse(c string) (cursorData bson.D, err error) {
	var data []byte
	if data, err = base64.RawURLEncoding.DecodeString(c); err != nil {
		return
	}

	err = bson.Unmarshal(data, &cursorData)
	return
}
