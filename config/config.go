package config

import (
	"crypto/rsa"
	"fmt"
	"io/ioutil"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/spf13/viper"
)

type GeneralConfig struct {
	DatabaseName  string
	DatabaseHost  string
	DatabasePort  int
	MgoTimeOut    int
	MgoPoolSize   int
	MongoPoolSize int
	LogFilePath   string
	LogLevel      string
	CountryCode   string
	CountryName   string
	GuestCode     string

	NotificationSMSActivity     string
	ForgotPasswordActivity      string
	NotifcationBaseUrl          string
	SendSmsNewrelicMetricName   string
	SendEmailNewrelicMetricName string

	PrivateKeyPath string
	PublicKeyPath  string
	PrivateKey     *rsa.PrivateKey
	PublicKey      *rsa.PublicKey

	RedisMaster          string
	RedisSentinelServers []string
	RedisPoolSize        int
	RedisDB              int
	RedisPassword        string

	OtpDuration int // in minutes

	MiscData                map[string]interface{}
	OTPByPassData           map[string]string
	GoogleAuth              map[string]interface{}
	FbAuth                  map[string]interface{}
	NewRelic                map[string]interface{}
	ExtendedSchemaNamespace map[string]bool

	SwaggerHost     string
	SwaggerVersion  string
	SwaggerBasePath string
}

func Loadconfig(filepath string) (a *GeneralConfig) {
	viper.SetConfigFile(filepath)
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}

	var config = new(GeneralConfig)

	config.DatabaseName = viper.GetString("database_name")
	config.DatabaseHost = viper.GetString("database_host")
	config.MgoTimeOut = viper.GetInt("mgo_timeout")
	config.MgoPoolSize = viper.GetInt("mgo_pool_size")
	config.MongoPoolSize = viper.GetInt("mongo_pool_size")
	config.LogFilePath = viper.GetString("log_file")
	config.LogLevel = viper.GetString("log_level")
	config.DatabasePort = viper.GetInt("database_port")
	config.CountryCode = viper.GetString("country_code")
	config.CountryName = viper.GetString("country_name")
	config.GuestCode = viper.GetString("guest_code")

	config.NotifcationBaseUrl = viper.GetString("notification_url")
	config.NotificationSMSActivity = viper.GetString("notification_activity")
	config.ForgotPasswordActivity = viper.GetString("forgot_password_activity")
	config.SendSmsNewrelicMetricName = viper.GetString("send_sms_newrelic_metric_name")
	config.SendEmailNewrelicMetricName = viper.GetString("send_email_newrelic_metric_name")

	config.PrivateKeyPath = viper.GetString("private_key_path")
	config.PublicKeyPath = viper.GetString("public_key_path")
	// config.PrivateKey = LoadPrivateKey(config.PrivateKeyPath)
	// config.PublicKey = LoadPublicKey(config.PublicKeyPath)

	config.RedisMaster = viper.GetString("redis_master")
	config.RedisSentinelServers = strings.Split(viper.GetString("redis_sentinels"), ",")
	config.RedisDB = viper.GetInt("redis_db")
	config.RedisPoolSize = viper.GetInt("redis_pool_size")
	config.RedisPassword = viper.GetString("redis_password")

	config.OtpDuration = viper.GetInt("otp_duration")
	config.MiscData = viper.GetStringMap("misc_data")
	config.GoogleAuth = viper.GetStringMap("google_auth")
	config.FbAuth = viper.GetStringMap("fb_auth")
	config.NewRelic = viper.GetStringMap("new_relic")
	config.OTPByPassData = viper.GetStringMapString("otp_bypass_data")
	extendedSchemaNamespace := viper.GetStringMap("extended_schema_namespace")
	config.ExtendedSchemaNamespace = make(map[string]bool)
	for k, v := range extendedSchemaNamespace {
		config.ExtendedSchemaNamespace[k] = v.(bool)
	}

	config.SwaggerHost = viper.GetString("swagger_host")
	config.SwaggerVersion = viper.GetString("swagger_version")
	config.SwaggerBasePath = viper.GetString("swagger_base_path")

	return config
}

func LoadPrivateKey(keyPath string) *rsa.PrivateKey {

	signBytes, err := ioutil.ReadFile(keyPath)
	if err != nil {
		panic(fmt.Errorf("Fatal error reading private key: %s \n", err))
	}
	signKey, err := jwt.ParseRSAPrivateKeyFromPEM(signBytes)
	if err != nil {
		panic(fmt.Errorf("Fatal error parsing private key: %s \n", err))
	}

	return signKey
}

func LoadPublicKey(keyPath string) *rsa.PublicKey {

	signBytes, err := ioutil.ReadFile(keyPath)
	if err != nil {
		panic(fmt.Errorf("Fatal error reading private key: %s \n", err))
	}
	signKey, err := jwt.ParseRSAPublicKeyFromPEM(signBytes)
	if err != nil {
		panic(fmt.Errorf("Fatal error parsing private key: %s \n", err))
	}

	return signKey
}
